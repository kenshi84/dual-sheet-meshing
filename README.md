# Dual Sheet Meshing

This repository contains source code of an interactive hex meshing application proposed in the following publication:

> Kenshi Takayama.
> **Dual sheet meshing: An interactive approach to robust hexahedralization.**
> *Computer Graphics Forum (proceedings of Eurographics), 38(2), 2019*

The source code is provided under the BSD-3 license.

## Compiling

```
git clone --recursive https://bitbucket.org/kenshi84/dual-sheet-meshing.git
cd dual-sheet-meshing
mkdir build && cd build
cmake ..
make
```

## Usage

(TODO)
