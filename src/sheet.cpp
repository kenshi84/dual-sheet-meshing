#include "misc.h"
#include "helper.h"
using namespace std;
using namespace Eigen;
using namespace kt84;

int dsm::Sheet::id_counter = -1;

void dsm::Sheet::buildFunc() {
    if (type==Sheet::Type::Freeform) {
        freeform.rbf.constraints.clear();
        for (const PointNormal& pn : freeform.points) {
            freeform.rbf.constraints.push_back({pn.head(3), 0.0, pn.tail(3)});
        }
        for (const std::pair<TriMeshPoint, PointNormal>& p : freeform.surfacePoints) {
            freeform.rbf.constraints.push_back({p.second.head(3), 0.0, p.second.tail(3)});
        }
        freeform.rbf.factorize_and_solve();
    } else if (type==Sheet::Type::Sketch2D) {
        sketch2d.rbf.constraints.resize(sketch2d.points.size());
        for (size_t i = 0; i < sketch2d.points.size(); ++i) {
            sketch2d.rbf.constraints[i].point << sketch2d.points[i].head(2), 0;
            sketch2d.rbf.constraints[i].gradient << sketch2d.points[i].transpose().tail(2), 0;
            sketch2d.rbf.constraints[i].value = 0;
        }
        sketch2d.rbf.factorize_and_solve();
    }
}

double dsm::Sheet::func(const Eigen::Vector3d& p) const {
    if (type==Type::Freeform) {
        return freeform.rbf(p);
    }
    const Vector2d q = canvas.project(p);
    if (type==Type::Cylinder) {
        return q.norm() - cylinder.radius;
    }
    if (type==Type::Sketch2D) {
        return sketch2d.rbf(Vector3d(q[0], q[1], 0));
    }
    assert(false);
    return 0;    
}

double dsm::Sheet::funcLinear(const TetMesh& tetMesh, const TetMeshPoint& p) const {
    double result = 0;
    int i = 0;
    for (VHandle v : tetMesh.cell_vertices(p.cell))
        result += p.bc[i++] * isosurface.perVertexData[v].funcVal;
    return result;
}

Vector3d dsm::Sheet::gradient(const Vector3d& p) const {
    if (type==Type::Freeform) {
        return freeform.rbf.gradient(p);
    }
    const Vector2d q = canvas.project(p);
    Vector2d d;
    if (type==Type::Cylinder) {
        d = q.normalized();
    }
    if (type==Type::Sketch2D) {
        d = sketch2d.rbf.gradient(Vector3d(q[0], q[1], 0)).head(2);
    }
    return canvas.unproject(d) - canvas.translation;
}

void dsm::Sheet::initCanvas(const Polyline3d& stroke3d, const CameraFree& camera, double resampleInterval) {
    assert(type!=Sheet::Type::Freeform);
    assert(stroke3d.size() > 4);

    // determine canvas coordinate transformation
    Vector3d cog = Vector3d::Zero();
    for (auto& p : stroke3d)
        cog += p;
    cog /= stroke3d.size();
    const Vector3d e = camera.center_to_eye().normalized();
    canvas.rotation.col(1) = camera.up;
    canvas.rotation.col(2) = e;
    canvas.rotation.col(0) = canvas.rotation.col(1).cross(canvas.rotation.col(2));
    canvas.translation = cog + e * (e.dot(stroke3d.front() - cog) + 0.05);

    Polyline2d stroke2d(stroke3d.size(), false);
    for (size_t i = 0; i < stroke3d.size(); ++i)
        stroke2d[i] = canvas.project(stroke3d[i]);

    if (type == Sheet::Type::Cylinder) {
        // https://stackoverflow.com/q/44647239
        const size_t num_points = stroke2d.size();
        MatrixXd A(num_points, 3);
        VectorXd b(num_points);
        for (size_t i = 0; i < num_points; ++i) {
            A.row(i) << 2 * stroke2d[i].transpose(), -1;
            b[i] = stroke2d[i].squaredNorm();
        }
        Vector3d x = (A.transpose() * A).inverse() * (A.transpose() * b);
        Vector2d center = x.head(2);
        cylinder.radius = sqrt(center.squaredNorm() - x[2]);
        canvas.translation = canvas.unproject(center);
    } else if (type == Sheet::Type::Sketch2D) {
        stroke2d.resample(max<size_t>(stroke2d.length() / resampleInterval, 3));
        const size_t num_points = stroke2d.size();
        sketch2d.points.resize(num_points);
        for (size_t i = 0; i < num_points; ++i) {
            Vector2d normal = Vector2d::Zero();
            if (i > 0) {
                normal += eigen_util::rotate90<Vector2d>(stroke2d[i] - stroke2d[i - 1]);
            }
            if (i + 1 < num_points) {
                normal += eigen_util::rotate90<Vector2d>(stroke2d[i + 1] - stroke2d[i]);
            }
            normal.normalize();
            sketch2d.points[i] = pn2d_make(stroke2d[i], normal);
        }
    } else {
        assert(false);
    }
}

void dsm::Sheet::update(const TetMesh& mesh) {
    buildFunc();
    isosurface = misc::marchingTetra(mesh, [&](VHandle v) { return func(vector_cast<3,Vector3d>(mesh.vertex(v))); }, 0);
    for (int tet : ignoredTets) {
        if (tet>=mesh.n_cells()) {
            cout << "[Sheet::update] warning: the recorded ignored tet index is inconsistent with the actual tetMesh\n";
            continue;
        }
        isosurface.perTetData[tet].ignored = true;
    }
    dispList.invalidate();
}
