#include "typedefs.h"
using namespace std;
using namespace Eigen;

namespace {

void compute_edges(const int nV, const vector<VectorXi>& F, VectorXi& E2E, VectorXi& V2E, vector<pair<int,int>>& edges) {
    const int nF = F.size();
    unordered_map<array<int,2>,int> pair_to_halfedgeIdx;
    for (int f = 0; f < nF; ++f) {
        const int deg = F[f].size();
        for (int i = 0; i < deg; ++i) {
            const array<int,2> p = { F[f][i], F[f][(i+1)%deg] };
            assert(pair_to_halfedgeIdx.find(p) == pair_to_halfedgeIdx.end());
            pair_to_halfedgeIdx[p] = 5*f + i;
        }
    }
    E2E = VectorXi::Constant(5*nF,-1);
    V2E = VectorXi::Constant(nV,-1);
    edges = {};
    for (int f = 0; f < nF; ++f) {
        const int deg = F[f].size();
        for (int i = 0; i < deg; ++i) {
            const int v0 = F[f][i];
            const int v1 = F[f][(i+1)%deg];
            if (v0 < v1) {
                edges.push_back({v0,v1});
            }
            const int halfedgeIdx = 5*f + i;
            E2E[halfedgeIdx] = pair_to_halfedgeIdx.find({v1,v0})->second;
            V2E[v0] = halfedgeIdx;
        }
    }
}

dsm::ReferencePolyhedron get_4_0_0() {
    MatrixXd V(4,3);
    V << 
        -0.117851,  0.589256, -0.117851,
        -0.117851, -0.117851,  0.589256,
         0.589256, -0.117851, -0.117851,
        -0.353553, -0.353553, -0.353553;
    vector<VectorXi> F(4);
    F[0].resize(3); F[0] << 2, 0, 1;
    F[1].resize(3); F[1] << 3, 1, 0;
    F[2].resize(3); F[2] << 3, 2, 1;
    F[3].resize(3); F[3] << 3, 0, 2;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

dsm::ReferencePolyhedron get_2_3_0() {
    MatrixXd V(6,3);
    V << 
         0.5, -0.28868,  0.5,
           0,  0.57735,  0.5,
        -0.5, -0.28868,  0.5,
         0.5, -0.28868, -0.5,
           0,  0.57735, -0.5,
        -0.5, -0.28868, -0.5;
    vector<VectorXi> F(5);
    F[0].resize(3); F[0] << 2, 0, 1;
    F[1].resize(3); F[1] << 5, 4, 3;
    F[2].resize(4); F[2] << 4, 1, 0, 3;
    F[3].resize(4); F[3] << 5, 2, 1, 4;
    F[4].resize(4); F[4] << 5, 3, 0, 2;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

dsm::ReferencePolyhedron get_2_2_2() {
    MatrixXd V(8,3);
    V << 
         0.5     , -0.735442,  0  ,
         0.836547,  0.062516,  0.5,
         0       ,  0.610411,  0.5,
        -0.836547,  0.062516,  0.5,
        -0.5     , -0.735442,  0  ,
        -0.836547,  0.062516, -0.5,
         0       ,  0.610411, -0.5,
         0.836547,  0.062516, -0.5;
    vector<VectorXi> F(6);
    F[0].resize(3); F[0] << 5, 4, 3;
    F[1].resize(3); F[1] << 7, 1, 0;
    F[2].resize(4); F[2] << 6, 5, 3, 2;
    F[3].resize(4); F[3] << 7, 6, 2, 1;
    F[4].resize(5); F[4] << 7, 0, 4, 5, 6;
    F[5].resize(5); F[5] << 4, 0, 1, 2, 3;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

dsm::ReferencePolyhedron get_0_6_0() {
    MatrixXd V(8,3);
    V << 
         0.396362,  0.5,  0.585574,
        -0.585574,  0.5,  0.396362,
        -0.585574, -0.5,  0.396362,
         0.396362, -0.5,  0.585574,
         0.585574,  0.5, -0.396362,
        -0.396362,  0.5, -0.585574,
        -0.396362, -0.5, -0.585574,
         0.585574, -0.5, -0.396362;
    vector<VectorXi> F(6);
    F[0].resize(4); F[0] << 3, 0, 1, 2;
    F[1].resize(4); F[1] << 7, 6, 5, 4;
    F[2].resize(4); F[2] << 5, 1, 0, 4;
    F[3].resize(4); F[3] << 6, 2, 1, 5;
    F[4].resize(4); F[4] << 7, 3, 2, 6;
    F[5].resize(4); F[5] << 7, 4, 0, 3;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

dsm::ReferencePolyhedron get_0_5_2() {
    MatrixXd V(10,3);
    V << 
         0       ,  0.850651,  0.5,
        -0.809017,  0.262866,  0.5,
        -0.5     , -0.688191,  0.5,
         0.5     , -0.688191,  0.5,
         0.809017,  0.262866,  0.5,
         0       ,  0.850651, -0.5,
        -0.809017,  0.262866, -0.5,
        -0.5     , -0.688191, -0.5,
         0.5     , -0.688191, -0.5,
         0.809017,  0.262866, -0.5;
    vector<VectorXi> F(7);
    F[0].resize(5); F[0] << 4, 0, 1, 2, 3;
    F[1].resize(5); F[1] << 9, 8, 7, 6, 5;
    F[2].resize(4); F[2] << 6, 1, 0, 5;
    F[3].resize(4); F[3] << 7, 2, 1, 6;
    F[4].resize(4); F[4] << 8, 3, 2, 7;
    F[5].resize(4); F[5] << 9, 4, 3, 8;
    F[6].resize(4); F[6] << 9, 5, 0, 4;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

dsm::ReferencePolyhedron get_1_3_3() {
    MatrixXd V(10,3);
    V << 
        -0.5     ,  0.882212,  0.45415,
         0       ,  0.278963,  1.07551,
         0.5     ,  0.882212,  0.45415,
        -0.587501,  0.203586, -0.27510,
         0       , -0.505232,  0.455  ,
         0.587501,  0.203586, -0.27510,
        -0.811697, -0.750237, -0.07521,
         0.811697, -0.750237, -0.07521,
         0       ,  0.229074, -1.08392,
         0       , -0.673925, -0.65428;
    vector<VectorXi> F(7);
    F[0].resize(3); F[0] << 2, 0, 1;
    F[1].resize(4); F[1] << 9, 7, 4, 6;
    F[2].resize(4); F[2] << 9, 8, 5, 7;
    F[3].resize(4); F[3] << 9, 6, 3, 8;
    F[4].resize(5); F[4] << 8, 3, 0, 2, 5;
    F[5].resize(5); F[5] << 6, 4, 1, 0, 3;
    F[6].resize(5); F[6] << 7, 5, 2, 1, 4;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

dsm::ReferencePolyhedron get_0_4_4() {
    MatrixXd V(12,3);
    V << 
         0.792758, -0.45513,  0.5     ,
         0.792758, -0.45513, -0.5     ,
         0.5     ,  0.45513, -0.792758,
         0.5     ,  1.06467,  0       ,
         0.5     ,  0.45513,  0.792758,
        -0.5     ,  0.45513,  0.792758,
        -0.792758, -0.45513,  0.5     ,
         0       , -1.06467,  0.5     ,
         0       , -1.06467, -0.5     ,
        -0.5     ,  1.06467,  0       ,
        -0.792758, -0.45513, -0.5     ,
        -0.5     ,  0.45513, -0.792758;
    vector<VectorXi> F(8);
    F[0].resize(4); F[0] <<  8,  1, 0, 7;
    F[1].resize(4); F[1] << 10,  8, 7, 6;
    F[2].resize(4); F[2] <<  9,  5, 4, 3;
    F[3].resize(4); F[3] << 11,  9, 3, 2;
    F[4].resize(5); F[4] <<  4,  0, 1, 2,  3;
    F[5].resize(5); F[5] <<  7,  0, 4, 5,  6;
    F[6].resize(5); F[6] << 11, 10, 6, 5,  9;
    F[7].resize(5); F[7] << 11,  2, 1, 8, 10;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

dsm::ReferencePolyhedron get_2_0_6() {
    MatrixXd V(12,3);
    V << 
         0       , -1.19288 ,  0.435848,
        -0.5     , -1.08781 , -0.423781,
         0.5     , -1.08781 , -0.423781,
         0       , -0.303248,  0.892532,
        -0.799107, -0.135327, -0.481338,
         0.799107, -0.135327, -0.481338,
        -0.799107,  0.135327,  0.481338,
         0       ,  0.303248, -0.892532,
         0.799107,  0.135327,  0.481338,
        -0.5     ,  1.08781 ,  0.423781,
         0       ,  1.19288 , -0.435848,
         0.5     ,  1.08781 ,  0.423781;
    vector<VectorXi> F(8);
    F[0].resize(3); F[0] <<  2,  0, 1;
    F[1].resize(3); F[1] << 11, 10, 9;
    F[2].resize(5); F[2] <<  7,  5, 2, 1,  4;
    F[3].resize(5); F[3] <<  8,  3, 0, 2,  5;
    F[4].resize(5); F[4] <<  6,  4, 1, 0,  3;
    F[5].resize(5); F[5] << 11,  9, 6, 3,  8;
    F[6].resize(5); F[6] << 10,  7, 4, 6,  9;
    F[7].resize(5); F[7] << 11,  8, 5, 7, 10;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

dsm::ReferencePolyhedron get_0_3_6() {
    MatrixXd V(14,3);
    V << 
        -0.90627  ,  0       , -0.569705,
        -0.972779 ,  0.819153,  0       ,
         0        ,  0.987421,  0       ,
         0.48639  ,  0.819153, -0.842452,
        -0.0402443,  0       , -1.06971 ,
         0.48639  , -0.819153, -0.842452,
         0        , -0.987421,  0       ,
        -0.972779 , -0.819153,  0       ,
        -0.90627  ,  0       ,  0.569705,
         0.946514 ,  0       , -0.5     ,
        -0.0402443,  0       ,  1.06971 ,
         0.48639  ,  0.819153,  0.842452,
         0.946514 ,  0       ,  0.5     ,
         0.48639  , -0.819153,  0.842452;
    vector<VectorXi> F(9);
    F[0].resize(4); F[0] <<  8,  1,  0,  7;
    F[1].resize(4); F[1] <<  9,  5,  4,  3;
    F[2].resize(4); F[2] << 13, 12, 11, 10;
    F[3].resize(5); F[3] <<  4,  0,  1,  2,  3;
    F[4].resize(5); F[4] <<  7,  0,  4,  5,  6;
    F[5].resize(5); F[5] << 13, 10,  8,  7,  6;
    F[6].resize(5); F[6] << 11,  2,  1,  8, 10;
    F[7].resize(5); F[7] << 12,  9,  3,  2, 11;
    F[8].resize(5); F[8] << 13,  6,  5,  9, 12;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

dsm::ReferencePolyhedron get_0_2_8() {
    MatrixXd V(16,3);
    V << 
         0.5     ,  -1.14301,  0.5     ,
        -0.5     ,  -1.14301,  0.5     ,
        -0.5     ,  -1.14301, -0.5     ,
         0.5     ,  -1.14301, -0.5     ,
         0.807753, -0.242688,  0.807753,
        -0.807753, -0.242688,  0.807753,
        -0.807753, -0.242688, -0.807753,
         0.807753, -0.242688, -0.807753,
         0       ,  0.242688,  1.14234 ,
        -1.14234 ,  0.242688,  0       ,
         0       ,  0.242688, -1.14234 ,
         1.14234 ,  0.242688,  0       ,
         0       ,  1.14301 ,  0.707107,
        -0.707107,  1.14301 ,  0       ,
         0       ,  1.14301 , -0.707107,
         0.707107,  1.14301 ,  0       ;
    vector<VectorXi> F(10);
    F[0].resize(4); F[0] <<  3,  0,  1,  2;
    F[1].resize(4); F[1] << 15, 14, 13, 12;
    F[2].resize(5); F[2] <<  8,  5,  1,  0,  4;
    F[3].resize(5); F[3] <<  9,  6,  2,  1,  5;
    F[4].resize(5); F[4] << 10,  7,  3,  2,  6;
    F[5].resize(5); F[5] << 11,  4,  0,  3,  7;
    F[6].resize(5); F[6] << 15, 12,  8,  4, 11;
    F[7].resize(5); F[7] << 13,  9,  5,  8, 12;
    F[8].resize(5); F[8] << 14, 10,  6,  9, 13;
    F[9].resize(5); F[9] << 15, 11,  7, 10, 14;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

dsm::ReferencePolyhedron get_0_0_12() {
    MatrixXd V(20,3);
    V << 
         0       ,  0.910076,  1.0655  ,
        -0.809017,  0.323164,  1.09753 ,
        -0.5     , -0.626478,  1.14937 ,
         0.5     , -0.626478,  1.14937 ,
         0.809017,  0.323164,  1.09753 ,
         0       ,  1.38866 ,  0.187458,
        -1.30902 ,  0.43902 ,  0.239293,
        -0.809017, -1.09753 ,  0.323164,
         0.809017, -1.09753 ,  0.323164,
         1.30902 ,  0.43902 ,  0.239293,
        -0.809017,  1.09753 , -0.323164,
        -1.30902 , -0.43902 , -0.239293,
         0       , -1.38866 , -0.187458,
         1.30902 , -0.43902 , -0.239293,
         0.809017,  1.09753 , -0.323164,
        -0.5     ,  0.626478, -1.14937 ,
        -0.809017, -0.323164, -1.09753 ,
         0       , -0.910076, -1.0655  ,
         0.809017, -0.323164, -1.09753 ,
         0.5     ,  0.626478, -1.14937 ;
    vector<VectorXi> F(12);
    F[ 0].resize(5); F[ 0] <<  4,  0,  1,  2,  3;
    F[ 1].resize(5); F[ 1] << 19, 18, 17, 16, 15;
    F[ 2].resize(5); F[ 2] << 10,  6,  1,  0,  5;
    F[ 3].resize(5); F[ 3] << 11,  7,  2,  1,  6;
    F[ 4].resize(5); F[ 4] << 12,  8,  3,  2,  7;
    F[ 5].resize(5); F[ 5] << 13,  9,  4,  3,  8;
    F[ 6].resize(5); F[ 6] << 14,  5,  0,  4,  9;
    F[ 7].resize(5); F[ 7] << 16, 11,  6, 10, 15;
    F[ 8].resize(5); F[ 8] << 17, 12,  7, 11, 16;
    F[ 9].resize(5); F[ 9] << 18, 13,  8, 12, 17;
    F[10].resize(5); F[10] << 19, 14,  9, 13, 18;
    F[11].resize(5); F[11] << 19, 15, 10,  5, 14;
    VectorXi E2E;
    VectorXi V2E;
    vector<pair<int,int>> edges;
    compute_edges(V.rows(), F, E2E, V2E, edges);
    return { V, F, E2E, V2E, edges };
}

}

const dsm::ReferencePolyhedron& dsm::ReferencePolyhedron::get(const array<int,3>& signature) {
    static const ReferencePolyhedron referencepolyhedron_4_0_0 = get_4_0_0();
    static const ReferencePolyhedron referencepolyhedron_2_3_0 = get_2_3_0();
    static const ReferencePolyhedron referencepolyhedron_2_2_2 = get_2_2_2();
    static const ReferencePolyhedron referencepolyhedron_0_6_0 = get_0_6_0();
    static const ReferencePolyhedron referencepolyhedron_0_5_2 = get_0_5_2();
    static const ReferencePolyhedron referencepolyhedron_1_3_3 = get_1_3_3();
    static const ReferencePolyhedron referencepolyhedron_0_4_4 = get_0_4_4();
    static const ReferencePolyhedron referencepolyhedron_2_0_6 = get_2_0_6();
    static const ReferencePolyhedron referencepolyhedron_0_3_6 = get_0_3_6();
    static const ReferencePolyhedron referencepolyhedron_0_2_8 = get_0_2_8();
    static const ReferencePolyhedron referencepolyhedron_0_0_12 = get_0_0_12();
    if (signature[0] == 4 && signature[1] == 0 && signature[2] == 0) return referencepolyhedron_4_0_0;
    if (signature[0] == 2 && signature[1] == 3 && signature[2] == 0) return referencepolyhedron_2_3_0;
    if (signature[0] == 2 && signature[1] == 2 && signature[2] == 2) return referencepolyhedron_2_2_2;
    if (signature[0] == 0 && signature[1] == 6 && signature[2] == 0) return referencepolyhedron_0_6_0;
    if (signature[0] == 0 && signature[1] == 5 && signature[2] == 2) return referencepolyhedron_0_5_2;
    if (signature[0] == 1 && signature[1] == 3 && signature[2] == 3) return referencepolyhedron_1_3_3;
    if (signature[0] == 0 && signature[1] == 4 && signature[2] == 4) return referencepolyhedron_0_4_4;
    if (signature[0] == 2 && signature[1] == 0 && signature[2] == 6) return referencepolyhedron_2_0_6;
    if (signature[0] == 0 && signature[1] == 3 && signature[2] == 6) return referencepolyhedron_0_3_6;
    if (signature[0] == 0 && signature[1] == 2 && signature[2] == 8) return referencepolyhedron_0_2_8;
    if (signature[0] == 0 && signature[1] == 0 && signature[2] == 12) return referencepolyhedron_0_0_12;
    assert(false);
    static ReferencePolyhedron dummy;
    return dummy;
}

bool dsm::ReferencePolyhedron::is_face_flipped(vector<int> face_vertices) const {
    vector<int> face_vertices_reversed = face_vertices;
    boost::reverse(face_vertices_reversed);
    for (int f = 0; f < F.size(); ++f) {
        const int deg = F[f].size();
        if (deg != face_vertices.size())
            continue;
        kt84::container_util::bring_front(face_vertices, F[f][0]);
        kt84::container_util::bring_front(face_vertices_reversed, F[f][0]);
        if (Map<VectorXi>(face_vertices.data(),deg) == F[f])
            return false;
        if (Map<VectorXi>(face_vertices_reversed.data(),deg) == F[f])
            return true;
    }
    assert(false);      // wrong query!
    return false;
}

