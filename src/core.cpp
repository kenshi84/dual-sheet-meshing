#include "core.h"
#include "helper.h"
#include "misc.h"
#include <kt84/bgl/isomorphism.hh>
#include <igl/cotmatrix.h>
#include <igl/min_quad_with_fixed.h>
using namespace std;
using namespace Eigen;
using namespace kt84;

void dsm::core::computeHexLayoutTopology_step1_dualize(HexLayout& layout, const TetMesh& tetMesh, const vector<Sheet>& sheets, int rand_seed) {
    srand(rand_seed < 0 ? (unsigned int)time(0) : (unsigned int)rand_seed);
    layout = {};
    // check if any pair of sheets intersect tangentially
    for (int i=0; i<sheets.size(); ++i) {
        for (int j=i+1; j<sheets.size(); ++j) {
            for (auto tet : tetMesh.cells()) {
                if (sheets[i].isosurface.perTetData[tet].intersections.empty() ||
                    sheets[j].isosurface.perTetData[tet].intersections.empty() ||
                    sheets[i].isosurface.perTetData[tet].ignored ||
                    sheets[j].isosurface.perTetData[tet].ignored)
                    continue;
                Vector4d func0 = helper::funcValAtTet(tetMesh, sheets[i], tet),
                         func1 = helper::funcValAtTet(tetMesh, sheets[j], tet);
                auto t = helper::compute2SheetsIntersectionWithinTet(func0, func1);
                if (!t)
                    continue;
                // sheets i & j intersect within this tet; now checking the angle
                Vector3d n0 = helper::funcGradAtTet(tetMesh, sheets[i], tet).normalized(),
                         n1 = helper::funcGradAtTet(tetMesh, sheets[j], tet).normalized();
                double angle = helper::acos(abs(n0.dot(n1))) * 180 / util::pi();
                if (angle < 10) {
                    throw error::DualizationFailed{
                        error::DualizationFailed_Type::TangentialIntersection,
                        {i, j},
                        {
                            TetMeshPoint{tet,(*t)[0]}.getPosition(tetMesh),
                            TetMeshPoint{tet,(*t)[1]}.getPosition(tetMesh),
                        }
                    };
                }
            }
        }
    }
    // before generating cells & vertices, detect all 3-sheet intersections where 1 or more among the 3 sheets are tagged with 'ignored'
    // generate vertices with code that has '*' in the indices corresponding to the ignored sheets
    for (auto tet : tetMesh.cells()) {
        vector<int> intersectingSheets;
        for (int i=0; i<sheets.size(); ++i)
            if (!sheets[i].isosurface.perTetData[tet].intersections.empty())
                intersectingSheets.push_back(i);
        if (intersectingSheets.size()<3)
            continue;
        for (auto i=intersectingSheets.begin(); i!=intersectingSheets.end(); ++i)
        for (auto j=i+1;                        j!=intersectingSheets.end(); ++j)
        for (auto k=j+1;                        k!=intersectingSheets.end(); ++k)
        {
            vector<int> ignoredSheets;
            if (sheets[*i].isosurface.perTetData[tet].ignored) ignoredSheets.push_back(*i);
            if (sheets[*j].isosurface.perTetData[tet].ignored) ignoredSheets.push_back(*j);
            if (sheets[*k].isosurface.perTetData[tet].ignored) ignoredSheets.push_back(*k);
            if (ignoredSheets.empty())
                continue;
            Vector4d func0 = helper::funcValAtTet(tetMesh, sheets[*i], tet),
                     func1 = helper::funcValAtTet(tetMesh, sheets[*j], tet),
                     func2 = helper::funcValAtTet(tetMesh, sheets[*k], tet);
            auto t = helper::compute3SheetsIntersectionWithinTet(func0, func1, func2);
            if (!t)
                continue;
            // three sheets intersect within this tet
            ElemCode cell_code;
            for (int m=0; m<sheets.size(); ++m)
                cell_code.str.push_back(
                    (m==*i || m==*j || m==*k) ? '0' :
                    helper::funcValAtTet(tetMesh, sheets[m], tet).dot(*t) > 0 ? '+' : '-');
            // generate layout vertex if not existent
            for (int m=0; m<8; ++m) {
                auto vertex_code = cell_code;
                vertex_code.str[*i] = (m&1) ? '+' : '-';
                vertex_code.str[*j] = (m&2) ? '+' : '-';
                vertex_code.str[*k] = (m&4) ? '+' : '-';
                // replace '+' or '-' with '*' at indices corresponding to ignored sheets
                for (auto m : ignoredSheets)
                    vertex_code.str[m] = '*';
                auto found = layout.vertexByCode.find(vertex_code);
                if (found==layout.vertexByCode.end()) {
                    auto v = layout.vertexByCode[vertex_code] = layout.add_vertex(Vec3d{});
                    layout.data(v).code = vertex_code;
                }
            }
        }
    }
    // visit all tets, create new hex when 3 sheets intersect within the tet, creating new hexLayout vertices if non-existent
    for (auto tet : tetMesh.cells()) {
        vector<int> intersectingSheets;
        for (int i=0; i<sheets.size(); ++i)
            if (!sheets[i].isosurface.perTetData[tet].intersections.empty() && !sheets[i].isosurface.perTetData[tet].ignored)
                intersectingSheets.push_back(i);
        if (intersectingSheets.size()<3)
            continue;
        for (auto i=intersectingSheets.begin(); i!=intersectingSheets.end(); ++i)
        for (auto j=i+1;                        j!=intersectingSheets.end(); ++j)
        for (auto k=j+1;                        k!=intersectingSheets.end(); ++k)
        {
            Vector4d func0 = helper::funcValAtTet(tetMesh, sheets[*i], tet),
                     func1 = helper::funcValAtTet(tetMesh, sheets[*j], tet),
                     func2 = helper::funcValAtTet(tetMesh, sheets[*k], tet);
            auto t = helper::compute3SheetsIntersectionWithinTet(func0, func1, func2);
            if (!t)
                continue;
            // three sheets intersect within this tet -> create new layout cell
            ElemCode cell_code;
            for (int m=0; m<sheets.size(); ++m)
                cell_code.str.push_back(
                    (m==*i || m==*j || m==*k) ? '0' :
                    helper::funcValAtTet(tetMesh, sheets[m], tet).dot(*t) > 0 ? '+' : '-');
            if (layout.cellByCode.find(cell_code)!=layout.cellByCode.end()) {
                cout << "BadConfig: distinct hex cells with identical code: " << cell_code.str << endl;
                throw error::DualizationFailed{
                    error::DualizationFailed_Type::MultiplePrimalCellsWithSameCode,
                    {*i, *j, *k},
                    {
                        layout.data(layout.cellByCode[cell_code]).sheetCrossing.getPosition(tetMesh)
                    }
                };
            }
            vector<VHandle> cellVertices(8);
            // generate layout vertex if not existent
            for (int m=0; m<8; ++m) {
                auto vertex_code = cell_code;
                vertex_code.str[*i] = (m&1) ? '+' : '-';
                vertex_code.str[*j] = (m&2) ? '+' : '-';
                vertex_code.str[*k] = (m&4) ? '+' : '-';
                auto found = layout.vertexByCode.find(vertex_code);
                VHandle v;
                if (found==layout.vertexByCode.end()) {
                    v = layout.vertexByCode[vertex_code] = layout.add_vertex(Vec3d{});
                    layout.data(v).code = vertex_code;
                } else {
                    v = found->second;
                }
                cellVertices[m] = v;
            }
            // reorder cellVertices conforming to OpenVolumeMesh API: <OpenVolumeMesh/Mesh/HexahedralMeshTopologyKernel.hh>
            vector<VHandle> cellVertices_reordered;
            Vector3d grad_i = sheets[*i].isosurface.perTetData[tet].gradient,
                     grad_j = sheets[*j].isosurface.perTetData[tet].gradient,
                     grad_k = sheets[*k].isosurface.perTetData[tet].gradient;
            bool flipped = grad_i.cross(grad_j).dot(grad_k) < 0;
            if (flipped) {
                cellVertices_reordered = {
                    cellVertices[1], cellVertices[0], cellVertices[4], cellVertices[5],
                    cellVertices[3], cellVertices[7], cellVertices[6], cellVertices[2]
                };
            } else {
                cellVertices_reordered = {
                    cellVertices[0], cellVertices[1], cellVertices[5], cellVertices[4],
                    cellVertices[2], cellVertices[6], cellVertices[7], cellVertices[3]
                };
            }
            auto c = layout.cellByCode[cell_code] = layout.add_cell(cellVertices_reordered);
            // set per-cell info
            auto& cdata = layout.data(c);
            cdata.code = cell_code;
            cdata.sheetCrossing = {tet, *t};
            cdata.sheetId = {*i,*j,*k};
            cdata.flipped = flipped;
            cdata.color = Vector3f::Random()*0.25f + Vector3f::Constant(0.75f);
        }
    }
    // check for DualizationFailed_Type::MoreThanThreeSheetsIntersect
    for (auto layout_c0=layout.cells_begin(); layout_c0!=layout.cells_end(); ++layout_c0) {
        for (auto layout_c1=layout_c0+1; layout_c1!=layout.cells_end(); ++layout_c1) {
            Vector3d p0 = layout.data(*layout_c0).sheetCrossing.getPosition(tetMesh),
                     p1 = layout.data(*layout_c1).sheetCrossing.getPosition(tetMesh);
            if ((p1-p0).norm() < 0.001) {
                cout << "BadConfig: two intersections corresponding to layout cells " << *layout_c0 << " & " << *layout_c1
                     << " are too close to each other!\n";
                auto& c0data = layout.data(*layout_c0);
                auto& c1data = layout.data(*layout_c1);
                throw error::DualizationFailed{
                    error::DualizationFailed_Type::MoreThanThreeSheetsIntersect,
                    container_util::set_union(c0data.sheetId, c1data.sheetId),
                    {
                        c0data.sheetCrossing.getPosition(tetMesh),
                        c1data.sheetCrossing.getPosition(tetMesh)
                    }
                };
            }
        }
    }
    // check for DualizationFailed_Type::IsolatedDuallCell
    for (auto tet_v : tetMesh.vertices()) {
        auto v_code = helper::getVertexCode(sheets, tet_v);
        if (layout.vertexByCode.count(v_code)==0) {
            cout << "BadConfig: tetMesh vertex " << tet_v << " belongs to a dual cell isolated from the hex layout!\n";
            vector<int> sheetId;
            for (auto& p : layout.vertexByCode) {
                auto e_code = helper::getEdgeCode({v_code, p.first});
                if (!e_code.str.empty())
                    sheetId.push_back(e_code.str.find('0'));
            }
            throw error::DualizationFailed{
                error::DualizationFailed_Type::IsolatedDuallCell,
                sheetId,
                {
                    vector_cast<3,Vector3d>(tetMesh.vertex(tet_v))
                }
            };
        }
    }
    cout << "Hex Layout statistics:\n" 
        << "vertices: " << layout.n_vertices() << '\n'
        << "cells   : " << layout.n_cells() << '\n'
        << "faces   : " << layout.n_faces() << '\n'
        << "edges   : " << layout.n_edges() << '\n';
    layout.utility_storeIncidence();
    // check for unsupported singularity types (edge valence below 3 or above 5)
    for (EHandle layout_e : layout.edges()) {
        const int ngon = layout.edge_cells(layout_e).size() + (layout.is_boundary(layout_e) ? 2 : 0);
        if (ngon < 3 || 5 < ngon) {
            set<int> sheetId;
            vector<Vector3d> locations;
            for (CHandle layout_c : layout.edge_cells(layout_e)) {
                for (int i = 0; i < 3; ++i)
                    sheetId.insert(layout.data(layout_c).sheetId[i]);
                locations.push_back(layout.data(layout_c).sheetCrossing.getPosition(tetMesh));
            }
            throw error::DualizationFailed{
                error::DualizationFailed_Type::UnsupportedSingularity,
                container_util::cast<vector<int>>(sheetId),
                locations
            };
        }
    }
    // the same check for the boundary vertices
    for (VHandle layout_v : layout.boundary_vertices()) {
        const int ngon = layout.boundary_vertex_edges(layout_v).size();
        if (ngon < 3 || 5 < ngon) {
            set<int> sheetId;
            vector<Vector3d> locations;
            for (CHandle layout_c : layout.vertex_cells(layout_v)) {
                for (int i = 0; i < 3; ++i)
                    sheetId.insert(layout.data(layout_c).sheetId[i]);
                locations.push_back(layout.data(layout_c).sheetCrossing.getPosition(tetMesh));
            }
            throw error::DualizationFailed{
                error::DualizationFailed_Type::UnsupportedSingularity,
                container_util::cast<vector<int>>(sheetId),
                locations
            };
        }
    }
    // compute code & sheetId for edges and faces
    layout.edgeByCode.clear();
    for (auto e : layout.edges()) {
        auto v = layout.edge_vertices(e);
        array<ElemCode,2> v_code;
        for (int i=0; i<2; ++i)
            v_code[i] = layout.data(v[i]).code;
        auto& e_code = layout.data(e).code;
        e_code = helper::getEdgeCode(v_code);
        layout.data(e).sheetId = helper::parseIntersectingSheets(e_code)[0];
        layout.edgeByCode[e_code] = e;
    }
    layout.faceByCode.clear();
    for (auto f : layout.faces()) {
        auto v = layout.face_vertices(f);
        array<ElemCode,4> v_code;
        for (int i=0; i<4; ++i)
            v_code[i] = layout.data(v[i]).code;
        auto& f_code = layout.data(f).code;
        f_code = helper::getFaceCode(v_code);
        layout.data(f).sheetId = vector_cast<2,array<int,2>>(helper::parseIntersectingSheets(f_code));
        layout.faceByCode[f_code] = f;
    }
    // assign handles in the canonical orientation
    for (auto c : layout.cells()) {
        auto& cdata = layout.data(c);
        vector<VHandle> cellVertices(8);
        for (int m=0; m<8; ++m) {
            auto vertex_code = cdata.code;
            vertex_code.str[cdata.sheetId[0]] = (m&1) ? '+' : '-';
            vertex_code.str[cdata.sheetId[1]] = (m&2) ? '+' : '-';
            vertex_code.str[cdata.sheetId[2]] = (m&4) ? '+' : '-';
            cellVertices[m] = helper::queryMap(layout.vertexByCode, vertex_code);
        }
        for (int m=0; m<8; ++m)
            cdata.corner[m] = cellVertices[m];
        for (auto e : layout.cell_edges(c)) {
            for (int m=0; m<2; ++m) {
                auto he = layout.halfedge_handle(e,m);
                auto v = layout.halfedge_vertices(he);
                if (v[0]==cellVertices[0] && v[1]==cellVertices[1]) cdata.edge[0] = he;
                if (v[0]==cellVertices[2] && v[1]==cellVertices[3]) cdata.edge[1] = he;
                if (v[0]==cellVertices[4] && v[1]==cellVertices[5]) cdata.edge[2] = he;
                if (v[0]==cellVertices[6] && v[1]==cellVertices[7]) cdata.edge[3] = he;
                if (v[0]==cellVertices[0] && v[1]==cellVertices[2]) cdata.edge[4] = he;
                if (v[0]==cellVertices[4] && v[1]==cellVertices[6]) cdata.edge[5] = he;
                if (v[0]==cellVertices[1] && v[1]==cellVertices[3]) cdata.edge[6] = he;
                if (v[0]==cellVertices[5] && v[1]==cellVertices[7]) cdata.edge[7] = he;
                if (v[0]==cellVertices[0] && v[1]==cellVertices[4]) cdata.edge[8] = he;
                if (v[0]==cellVertices[1] && v[1]==cellVertices[5]) cdata.edge[9] = he;
                if (v[0]==cellVertices[2] && v[1]==cellVertices[6]) cdata.edge[10] = he;
                if (v[0]==cellVertices[3] && v[1]==cellVertices[7]) cdata.edge[11] = he;
            }
        }
        VHandle fv[6][4] = {
            { cellVertices[0], cellVertices[2], cellVertices[4], cellVertices[6] },
            { cellVertices[1], cellVertices[3], cellVertices[5], cellVertices[7] },
            { cellVertices[0], cellVertices[1], cellVertices[4], cellVertices[5] },
            { cellVertices[2], cellVertices[3], cellVertices[6], cellVertices[7] },
            { cellVertices[0], cellVertices[1], cellVertices[2], cellVertices[3] },
            { cellVertices[4], cellVertices[5], cellVertices[6], cellVertices[7] },
        };
        for (int m=0; m<6; ++m)
            boost::sort(fv[m]);
        for (auto hf : layout.cell_halffaces(c)) {
            auto v = layout.halfface_vertices(hf);
            boost::sort(v);
            for (int m=0; m<6; ++m) {
                if (fv[m][0]==v[0] && fv[m][1]==v[1] && fv[m][2]==v[2] && fv[m][3]==v[3]) {
                    cdata.face[m] = hf;
                    break;
                }
            }
        }
    }
    // visit all triangles and check if 2 sheets intersect within the triangle
    for (auto tri : tetMesh.faces()) {
        vector<int> intersectingSheets;
        for (int i=0; i<sheets.size(); ++i) {
            bool ignored = false;
            for (int m=0; m<2; ++m) {
                auto tet = tetMesh.cell_handle(tetMesh.halfface_handle(tri,m));
                if (tet.is_valid() && sheets[i].isosurface.perTetData[tet].ignored) {
                    ignored = true;
                    break;
                }
            }
            if (sheets[i].isosurface.perFaceData[tri].intersections[0].is_valid() && !ignored)
                intersectingSheets.push_back(i);
        }
        if (intersectingSheets.size()<2)
            continue;
        for (auto i=intersectingSheets.begin(); i!=intersectingSheets.end(); ++i)
        for (auto j=i+1;                        j!=intersectingSheets.end(); ++j)
        {
            Vector3d func0 = helper::funcValAtTriangle(tetMesh, sheets[*i], tri),
                     func1 = helper::funcValAtTriangle(tetMesh, sheets[*j], tri);
            auto t = helper::compute2SheetsIntersectionWithinTriangle(func0, func1);
            if (!t)
                continue;
            // three sheets intersect within this tri -> create new layout cell
            ElemCode face_code;
            for (int m=0; m<sheets.size(); ++m)
                face_code.str.push_back(
                    (m==*i || m==*j) ? '0' :
                    helper::funcValAtTriangle(tetMesh, sheets[m], tri).dot(*t) > 0 ? '+' : '-');
            const FHandle layout_f = layout.faceByCode.find(face_code)->second;
            layout.data(layout_f).sheetCrossing.push_back({tri, *t});
        }
    }
}
void dsm::core::computeHexLayoutTopology_step1a_init_numSubdiv(const HexLayout& layout, const TetMesh& tetMesh, std::vector<Sheet>& sheets, double targetEdgeLength) {
    assert(targetEdgeLength > 0);
    vector<MaxMinAverage> thickness(sheets.size()*2);
    for (FHandle layout_f : layout.faces()) {
        const array<CHandle,2>& layout_c = layout.face_cells(layout_f);
        double d;
        if (layout.is_boundary(layout_f)) {
            bool found = false;
            for (const TetMeshPoint& p : layout.data(layout_f).sheetCrossing) {
                if (tetMesh.is_boundary(p.face)) {
                    d = (layout.data(layout_c[0]).sheetCrossing.getPosition(tetMesh) - p.getPosition(tetMesh)).norm();
                    found = true;
                    break;
                }
            }
            assert(found);
        } else {
            d = 0.5 * (layout.data(layout_c[0]).sheetCrossing.getPosition(tetMesh) - layout.data(layout_c[1]).sheetCrossing.getPosition(tetMesh)).norm();
        }
        for (int i = 0; i < 2; ++i) {
            if (layout_c[i].is_valid()) {
                int sheetId = -1;
                bool is_negative;
                for (int m = 0; m < 6; ++m) {
                    if (layout.face_handle(layout.data(layout_c[i]).face[m]) == layout_f) {
                        sheetId = layout.data(layout_c[i]).sheetId[m/2];
                        is_negative = m%2 == 0;
                        break;
                    }
                }
                assert(sheetId != -1);
                thickness[2*sheetId + is_negative].update(d);
            }
        }
    }
    for (int sheetId = 0; sheetId < sheets.size(); ++sheetId) {
        for (int i = 0; i < 2; ++i) {
            sheets[sheetId].numSubdiv[i] = max<uint32_t>(ceil(thickness[2*sheetId + i].avg() / targetEdgeLength), 1) - 1;
        }
    }
}
void dsm::core::computeHexLayoutTopology_step2_check_featureGraph(HexLayout& layout, TriMesh& triMesh, const vector<Sheet>& sheets) {
    // check if a boundary dual cell contains more than one featuregraph nodes
    for (uint32_t trimesh_v = 0; trimesh_v < triMesh.V.cols(); ++trimesh_v) {
        if (triMesh.vertex_constraint[trimesh_v]) {
            const VHandle layout_v = layout.vertexByCode.find(helper::getVertexCode(sheets, triMesh.V.col(trimesh_v).cast<double>()))->second;
            if (layout.data(layout_v).featuregraph_node != -1) {
                throw error::FeatureGraphInconsistent{
                    error::FeatureGraphInconsistent_Type::MultipleNodesInDualCell,
                    layout_v
                };
            }
            layout.data(layout_v).featuregraph_node = trimesh_v;
        }
    }
    // visit all constrained edges and group together sequence of edges between nodes
    for (uint32_t trimesh_e = 0; trimesh_e < triMesh.edges.size(); ++trimesh_e) {
        // initialize constraint edges with flag -1
        if (triMesh.edge_constraint[trimesh_e])
            triMesh.edge_constraint[trimesh_e] = -1;
    }
    set<uint32_t> visited_edges;
    int featuregraph_arc_idx = 1;
    for (uint32_t trimesh_e = 0; trimesh_e < triMesh.edges.size(); ++trimesh_e) {
        if (visited_edges.count(trimesh_e) == 0 && triMesh.edge_constraint[trimesh_e]) {
            const array<uint32_t,2> trimesh_he_start = {
                triMesh.edge_to_halfedge[trimesh_e],
                triMesh.E2E[triMesh.edge_to_halfedge[trimesh_e]]
            };
            for (int i = 0; i < 2; ++i) {
                uint32_t trimesh_he = trimesh_he_start[i];
                while (true) {
                    const uint32_t trimesh_e = triMesh.halfedge_to_edge[trimesh_he];
                    visited_edges.insert(trimesh_e);
                    triMesh.edge_constraint[trimesh_e] = featuregraph_arc_idx;
                    const uint32_t trimesh_v = triMesh.to_vertex(trimesh_he);
                    if (triMesh.vertex_constraint[trimesh_v])
                        break;
                    const vector<uint32_t> outgoing_halfedges = triMesh.outgoing_halfedges(trimesh_v);
                    bool found = false;
                    for (uint32_t trimesh_he2 : outgoing_halfedges) {
                        if (trimesh_he2 != triMesh.E2E[trimesh_he] && triMesh.edge_constraint[triMesh.halfedge_to_edge[trimesh_he2]]) {
                            found = true;
                            trimesh_he = trimesh_he2;
                            break;
                        }
                    }
                    assert(found);
                    if (trimesh_he == trimesh_he_start[i])
                        break;
                }
            }
            ++featuregraph_arc_idx;
        }
    }
    // check if a boundary dual edge gets intersected by more than one featuregraph arcs
    for (uint32_t trimesh_e = 0; trimesh_e < triMesh.edges.size(); ++trimesh_e) {
        if (triMesh.edge_constraint[trimesh_e]) {
            // check if this edge is intersected by each of the sheets
            for (int i = 0; i < sheets.size(); ++i) {
                array<Vector3d,2> p;
                array<double,2> val;
                for (int j = 0; j <2; ++j) {
                    p[j] = triMesh.V.col(triMesh.edges[trimesh_e][j]).cast<double>();
                    val[j] = sheets[i].func(p[j]);
                }
                const double t = -val[0] / (val[1]-val[0]);         // (1-t)*val[0] + t*val[1] = 0
                if (0 < t && t < 1) {
                    const EHandle layout_e = layout.edgeByCode.find(helper::getEdgeCode(sheets, (1-t)*p[0] + t*p[1], i))->second;
                    assert(layout.is_boundary(layout_e));
                    if (layout.data(layout_e).featuregraph_arc != -1) {
                        throw error::FeatureGraphInconsistent{
                            error::FeatureGraphInconsistent_Type::MultipleArcsOnDualFace,
                            VHandle{},
                            layout_e
                        };
                    }
                    layout.data(layout_e).featuregraph_arc = triMesh.edge_constraint[trimesh_e];
                }
            }
        }
    }
    // check if all dual cells have "star-like" configuration
    for (VHandle layout_v : layout.boundary_vertices()) {
        vector<int> crossing_arcs;
        for (EHandle layout_e : layout.boundary_vertex_edges(layout_v)) {
            if (layout.data(layout_e).featuregraph_arc != -1)
                crossing_arcs.push_back(layout.data(layout_e).featuregraph_arc);
        }
        if (layout.data(layout_v).featuregraph_node == -1) {
            if (!crossing_arcs.empty()) {
                if (crossing_arcs.size() != 2 || crossing_arcs[0] != crossing_arcs[1]) {
                    throw error::FeatureGraphInconsistent{
                        error::FeatureGraphInconsistent_Type::NonStarLikeConfigInDuallCell,
                        layout_v
                    };
                }
            }
        } else {
            if (container_util::cast<set<int>>(crossing_arcs).size() != crossing_arcs.size()) {
                throw error::FeatureGraphInconsistent{
                    error::FeatureGraphInconsistent_Type::NonStarLikeConfigInDuallCell,
                    layout_v
                };
            }
        }
    }
    // check if all featuregraph arcs are crossed by at least one sheet
    for (int i = 1; i < featuregraph_arc_idx; ++i) {
        bool found = false;
        for (EHandle layout_e : layout.boundary_edges()) {
            if (layout.data(layout_e).featuregraph_arc == i) {
                found = true;
                break;
            }
        }
        if (!found) {
            throw error::FeatureGraphInconsistent{
                error::FeatureGraphInconsistent_Type::ArcWithNoIntersection,
                VHandle{},
                EHandle{},
                i
            };
        }
    }
}
void dsm::core::computeHexLayoutGeometry_step1_tetrahedralize(HexLayout& layout, RefinedTetMesh& refTetMesh, const TriMesh& triMesh, const TetMesh& tetMesh, const vector<Sheet>& sheets, const tetgen_util::Switches& tetgen_switches) {
    auto timer = make_ScopedTimerPrinter("Preparing input PLC for TetGen");
    tetgen_util::Format_poly in_poly;
    // to construct an input PLC for TetGen, all sheet intersecting points are uniquely mapped to indices in PLC pointlist
    //   - each intersecting point between a tetmesh edge and a sheet is uniquely identified by (edgeID, sheetId)
    //   - each intersecting point between two sheets on a tetmesh face is uniquely identified by (faceID, {sheetId_0, sheetId_1})
    //   - each intersecting point among three sheets in a tetmesh cell is uniquely identified by (cellID, , {sheetId_0, sheetId_1, sheetId_2})
    unordered_map<pair<EHandle,int>, int> edgePoint;
    unordered_map<pair<FHandle,set<int>>, int> facePoint;     // use set<int> instead of array<int,2> to get automatic sorting
    unordered_map<pair<CHandle,set<int>>, int> cellPoint;
    int numPoints = 0;
    for (int sheetId = 0; sheetId < sheets.size(); ++sheetId) {
        const Sheet& sheet = sheets[sheetId];
        for (EHandle tet_e : tetMesh.edges()) {
            const Isosurface::PerEdgeData& edata = sheet.isosurface.perEdgeData[tet_e];
            if (!edata.intersection.is_valid())
                continue;
            const pair<EHandle,int> key{ tet_e, sheetId };
            assert(edgePoint.find(key)==edgePoint.end());
            edgePoint[key] = numPoints++;
            const Vector3d pos = edata.intersection.getPosition(tetMesh);
            for (int d=0; d<3; ++d)
                in_poly.node.pointlist.push_back(pos[d]);
        }
    }
    for (FHandle layout_f : layout.faces()) {
        const auto sheetId = container_util::cast<set<int>>(layout.data(layout_f).sheetId);
        for (const TetMeshPoint& p : layout.data(layout_f).sheetCrossing) {
            assert(p.face.is_valid());
            const pair<FHandle,set<int>> key{ p.face, sheetId };
            assert(facePoint.find(key)==facePoint.end());
            facePoint[key] = numPoints++;
            const Vector3d pos = p.getPosition(tetMesh);
            for (int d=0; d<3; ++d)
                in_poly.node.pointlist.push_back(pos[d]);
        }
    }
    for (CHandle layout_c : layout.cells()) {
        const TetMeshPoint& p = layout.data(layout_c).sheetCrossing;
        assert(p.cell.is_valid());
        const auto sheetId = container_util::cast<set<int>>(layout.data(layout_c).sheetId);
        const pair<CHandle,set<int>> key{ p.cell, sheetId };
        assert(cellPoint.find(key)==cellPoint.end());
        cellPoint[key] = numPoints++;
        const Vector3d pos = p.getPosition(tetMesh);
        for (int d=0; d<3; ++d)
            in_poly.node.pointlist.push_back(pos[d]);
    }
    // process each sheet to generate facets
    struct Polygon {
        deque<pair<int,VectorXd>> corners;          // pair.first-->"point ID into the final node list in in_poly", pair.second-->"interpolated sheets func values"
        deque<pair<FHandle,int>> segments;          // if the segment is on the current tet's face, the face ID is set to the first; otherwise, the intersecting sheet ID is set to the second
        ElemCode e_code;                            // needed for setting regional attribute
        void rotate(size_t num) {
            boost::rotate(corners, corners.begin() + num);
            boost::rotate(segments, segments.begin() + num);
        }
        void resize(size_t sz) {
            assert(sz <= corners.size());
            corners.resize(sz);
            segments.resize(sz);
        }
    };
    for (int currentSheetId = 0; currentSheetId < sheets.size(); ++currentSheetId) {
        const Sheet& currentSheet = sheets[currentSheetId];
        for (CHandle tetmesh_c : tetMesh.cells()) {
            // function to add a new polygon to PLC
            const Vector3d tinyOffset = 1.0e-3 * currentSheet.isosurface.perTetData[tetmesh_c].gradient.normalized();
            auto addPolygon = [&](const Polygon& polygon) {
                in_poly.facetlist.push_back({});
                in_poly.facetlist.back().polygonlist.push_back({});
                for (auto& i : polygon.corners) {
                    in_poly.facetlist.back().polygonlist.back().push_back(i.first);
                }
            };
            // function to split a polygon if needed before adding
            function<void(const Polygon&, int)> processPolygon;
            processPolygon = [&](const Polygon& polygon, int splitSheetId) {
                if (splitSheetId == sheets.size()) {
                    addPolygon(polygon);
                    return;
                }
                if (splitSheetId == currentSheetId) {
                    processPolygon(polygon, splitSheetId+1);
                    return;
                }
                // test if the polygon gets split by the sheet
                vector<pair<int,pair<int,VectorXd>>> intersections;
                for (int i = 0; i < polygon.corners.size(); ++i) {
                    const VectorXd& x0 = polygon.corners[i].second;
                    const VectorXd& x1 = polygon.corners[(i+1)%polygon.corners.size()].second;
                    // (1-t) * x0[splitSheetId] + t * x1[splitSheetId] = 0
                    const double t = x0[splitSheetId] / (x0[splitSheetId] - x1[splitSheetId]);
                    if (0 < t && t < 1) {
                        const FHandle tetmesh_f = polygon.segments[i].first;
                        const int thirdSheetId = polygon.segments[i].second;
                        int cornerId;
                        if (tetmesh_f.is_valid()) {
                            assert(thirdSheetId == -1);
                            cornerId = facePoint.find({tetmesh_f, {currentSheetId, splitSheetId}})->second;
                        } else {
                            assert(thirdSheetId != -1);
                            cornerId = cellPoint.find({tetmesh_c, {currentSheetId, splitSheetId, thirdSheetId}})->second;
                        }
                        intersections.push_back({i, {cornerId, (1-t)*x0 + t*x1}});
                    }
                }
                if (intersections.empty()) {
                    Polygon next_polygon = polygon;
                    processPolygon(next_polygon, splitSheetId+1);
                    return;
                }
                assert(intersections.size() == 2);
                // split found
                array<Polygon,2> split_polygon = { polygon, polygon };
                split_polygon[0].rotate(intersections[0].first);
                split_polygon[0].resize(intersections[1].first - intersections[0].first + 1);
                split_polygon[0].corners.pop_front();
                split_polygon[0].corners.push_front(intersections[0].second);
                split_polygon[0].corners.push_back(intersections[1].second);
                split_polygon[0].segments.push_back({FHandle{}, splitSheetId});
                split_polygon[1].rotate(intersections[1].first);
                split_polygon[1].resize(polygon.corners.size() - (intersections[1].first - intersections[0].first) + 1);
                split_polygon[1].corners.pop_front();
                split_polygon[1].corners.push_front(intersections[1].second);
                split_polygon[1].corners.push_back(intersections[0].second);
                split_polygon[1].segments.push_back({FHandle{}, splitSheetId});
                processPolygon(split_polygon[0], splitSheetId+1);
                processPolygon(split_polygon[1], splitSheetId+1);
            };
            // prepare the initial polygon (cross-section of the tet by the sheet)
            const size_t numCorners = currentSheet.isosurface.perTetData[tetmesh_c].intersections.size();
            if (numCorners == 0)
                continue;
            Polygon polygon;
            polygon.corners.resize(numCorners);
            polygon.segments.resize(numCorners);
            for (size_t i = 0; i < numCorners; ++i) {
                const TetMeshPoint& p = currentSheet.isosurface.perTetData[tetmesh_c].intersections[i];
                const array<VHandle,2> tetmesh_v = tetMesh.edge_vertices(p.edge);
                const TetMeshPoint& p_next = currentSheet.isosurface.perTetData[tetmesh_c].intersections[(i+1)%numCorners];
                FHandle tetmesh_f;
                for (FHandle tmp : tetMesh.cell_faces(tetmesh_c)) {
                    if (tetMesh.is_incident(tmp, p.edge) && tetMesh.is_incident(tmp, p_next.edge)) {
                        tetmesh_f = tmp;
                        break;
                    }
                }
                assert(tetmesh_f.is_valid());
                VectorXd sheetFuncValues = VectorXd::Zero(sheets.size());
                for (int sheetId = 0; sheetId < sheets.size(); ++sheetId) {
                    if (sheetId == currentSheetId) {
                        // we already know the current sheet's func value is zero
                        continue;
                    }
                    for (int m=0; m<2; ++m) {
                        sheetFuncValues[sheetId] += p.bc[m] * sheets[sheetId].isosurface.perVertexData[tetmesh_v[m]].funcVal;
                    }
                }
                polygon.corners[i] = { edgePoint.find({p.edge, currentSheetId})->second, sheetFuncValues };
                polygon.segments[i] = { tetmesh_f, -1 };
            }
            processPolygon(polygon, 0);
        }
    }
    // all the boundary faces need to be added to the PLC as well
    unordered_map<VHandle, int> boundaryVertexPoint;
    for (VHandle tetmesh_v : tetMesh.boundary_vertices()) {
        boundaryVertexPoint[tetmesh_v] = numPoints++;
        const Vector3d pos = vector_cast<3,Vector3d>(tetMesh.vertex(tetmesh_v));
        for (int d=0; d<3; ++d)
            in_poly.node.pointlist.push_back(pos[d]);
    }
    struct BoundaryPolygon {
        deque<pair<int,VectorXd>> corners;          // pair.first-->"point ID into the final node list in in_poly", pair.second-->"interpolated sheets func values"
        deque<pair<EHandle,int>> segments;          // if the segment is on the boundary triangle's edge, the edge ID is set to the first; otherwise, the intersecting sheet ID is set to the second
        void rotate(size_t num) {
            boost::rotate(corners, corners.begin() + num);
            boost::rotate(segments, segments.begin() + num);
        }
        void resize(size_t sz) {
            assert(sz <= corners.size());
            corners.resize(sz);
            segments.resize(sz);
        }
    };
    for (HFHandle tetmesh_hf : tetMesh.boundary_halffaces()) {
        const FHandle tetmesh_f = tetMesh.face_handle(tetmesh_hf);
        // function to add a new boundary polygon to PLC
        auto addBoundaryPolygon = [&](const BoundaryPolygon& boundaryPolygon) {
            in_poly.facetlist.push_back({});
            in_poly.facetlist.back().polygonlist.push_back({});
            for (auto& i : boundaryPolygon.corners) {
                in_poly.facetlist.back().polygonlist.back().push_back(i.first);
            }
        };
        // function to split a boundary polygon if needed before adding
        function<void(const BoundaryPolygon&, int)> processBoundaryPolygon;
        processBoundaryPolygon = [&](const BoundaryPolygon& boundaryPolygon, int splitSheetId) {
            if (splitSheetId == sheets.size()) {
                addBoundaryPolygon(boundaryPolygon);
                return;
            }
            // test if the boundary polygon gets split by the sheet
            vector<pair<int,pair<int,VectorXd>>> intersections;
            for (int i = 0; i < boundaryPolygon.corners.size(); ++i) {
                const VectorXd& x0 = boundaryPolygon.corners[i].second;
                const VectorXd& x1 = boundaryPolygon.corners[(i+1)%boundaryPolygon.corners.size()].second;
                // (1-t) * x0[splitSheetId] + t * x1[splitSheetId] = 0
                const double t = x0[splitSheetId] / (x0[splitSheetId] - x1[splitSheetId]);
                if (0 < t && t < 1) {
                    const EHandle tetmesh_e = boundaryPolygon.segments[i].first;
                    const int secondSheetId = boundaryPolygon.segments[i].second;
                    int cornerId;
                    if (tetmesh_e.is_valid()) {
                        assert(secondSheetId == -1);
                        cornerId = edgePoint.find({tetmesh_e, splitSheetId})->second;
                    } else {
                        assert(secondSheetId != -1);
                        cornerId = facePoint.find({tetmesh_f, {splitSheetId, secondSheetId}})->second;
                    }
                    intersections.push_back({i, {cornerId, (1-t)*x0 + t*x1}});
                }
            }
            if (intersections.empty()) {
                processBoundaryPolygon(boundaryPolygon, splitSheetId+1);
                return;
            }
            assert(intersections.size() == 2);
            // split found
            array<BoundaryPolygon,2> split_boundaryPolygon = { boundaryPolygon, boundaryPolygon };
            split_boundaryPolygon[0].rotate(intersections[0].first);
            split_boundaryPolygon[0].resize(intersections[1].first - intersections[0].first + 1);
            split_boundaryPolygon[0].corners.pop_front();
            split_boundaryPolygon[0].corners.push_front(intersections[0].second);
            split_boundaryPolygon[0].corners.push_back(intersections[1].second);
            split_boundaryPolygon[0].segments.push_back({EHandle{}, splitSheetId});
            split_boundaryPolygon[1].rotate(intersections[1].first);
            split_boundaryPolygon[1].resize(boundaryPolygon.corners.size() - (intersections[1].first - intersections[0].first) + 1);
            split_boundaryPolygon[1].corners.pop_front();
            split_boundaryPolygon[1].corners.push_front(intersections[1].second);
            split_boundaryPolygon[1].corners.push_back(intersections[0].second);
            split_boundaryPolygon[1].segments.push_back({EHandle{}, splitSheetId});
            processBoundaryPolygon(split_boundaryPolygon[0], splitSheetId+1);
            processBoundaryPolygon(split_boundaryPolygon[1], splitSheetId+1);
        };
        // prepare the initial boundary polygon (cross-section of the boundary face by the sheet)
        BoundaryPolygon boundaryPolygon;
        boundaryPolygon.corners.resize(3);
        boundaryPolygon.segments.resize(3);
        size_t i = 0;
        for (HEHandle tetmesh_he : tetMesh.halfface_halfedges(tetmesh_hf)) {
            const VHandle tetmesh_v = tetMesh.from_vertex(tetmesh_he);
            VectorXd sheetFuncValues = VectorXd::Zero(sheets.size());
            for (int sheetId = 0; sheetId < sheets.size(); ++sheetId) {
                sheetFuncValues[sheetId] = sheets[sheetId].isosurface.perVertexData[tetmesh_v].funcVal;
            }
            boundaryPolygon.corners[i] = { boundaryVertexPoint.find(tetmesh_v)->second, sheetFuncValues };
            boundaryPolygon.segments[i] = { tetMesh.edge_handle(tetmesh_he), -1 };
            ++i;
        }
        processBoundaryPolygon(boundaryPolygon, 0);
    }
    // set region attributes
    for (CHandle layout_c : layout.cells()) {
        MaxMinAverage offset_magnitude;
        const TetMeshPoint& sheetCrossing = layout.data(layout_c).sheetCrossing;
        const array<int, 3>& sheetId = layout.data(layout_c).sheetId;
        const Vector3d center = sheetCrossing.getPosition(tetMesh);
        for (CHandle layout_c2 : layout.cell_cells(layout_c)) {
            offset_magnitude.update((center - layout.data(layout_c2).sheetCrossing.getPosition(tetMesh)).norm());
        }
        for (FHandle layout_f : layout.cell_faces(layout_c)) {
            if (layout.is_boundary(layout_f)) {
                bool found = false;
                for (const TetMeshPoint& p : layout.data(layout_f).sheetCrossing) {
                    if (tetMesh.is_boundary(p.face)) {
                        offset_magnitude.update((center - p.getPosition(tetMesh)).norm());
                        found = true;
                        break;
                    }
                }
                assert(found);
            }
        }
        array<Vector3d,3> d;
        for (int i = 0; i < 3; ++i) {
            d[i] = sheets[sheetId[i]].isosurface.perTetData[sheetCrossing.cell].gradient.normalized();
        }
        for (int m=0; m<8; ++m) {
            ElemCode v_code = layout.data(layout_c).code;
            v_code.str[sheetId[0]] = (m&1) ? '+' : '-';
            v_code.str[sheetId[1]] = (m&2) ? '+' : '-';
            v_code.str[sheetId[2]] = (m&4) ? '+' : '-';
            const Vector3d offset =
                ((m&1) ? 1 : -1) * d[0] +
                ((m&2) ? 1 : -1) * d[1] +
                ((m&4) ? 1 : -1) * d[2];
            const Vector3d pos = center + offset_magnitude.min() * 0.0001 * offset;
            for (int i=0; i<3; ++i)
                in_poly.regionlist.push_back(pos[i]);
            in_poly.regionlist.push_back(layout.vertexByCode.find(v_code)->second.idx());
            in_poly.regionlist.push_back(0);        // unused volume constraint
        }
    }
    timer.reset();

    // call TetGen
    in_poly.write("debug_in");
    cout << "TetGen switches: " << tetgen_switches.to_string() << endl;
    const tetgen_util::Out out = tetgen_util::tetrahedralize(in_poly, tetgen_switches);
    // out.write("debug_out");

    // convert to OpenVolumeMesh format
    refTetMesh = {};
    MatrixXd tetV;
    tetV.resize(3, out.node.pointlist.size()/3);
    int v = 0;
    for (auto p=out.node.pointlist.begin(); p!=out.node.pointlist.end(); p+=3, ++v)
        tetV.col(v) << *p, *(p+1), *(p+2);
    MatrixXi tetC;
    tetC.resize(4, out.ele.tetrahedronlist.size()/4);
    int c = 0;
    for (auto p=out.ele.tetrahedronlist.begin(); p!=out.ele.tetrahedronlist.end(); p+=4, ++c)
        tetC.col(c) << *p, *(p+1), *(p+2), *(p+3);
    misc::initTetMesh(refTetMesh, tetV, tetC);

    timer = make_ScopedTimerPrinter("Analyzing refined tetmesh");
    // assign layout vertex to each reftet_cell
    for (CHandle reftet_c : refTetMesh.cells()) {
        const VHandle layout_v{ (int)out.ele.tetrahedronattributelist[reftet_c] };
        refTetMesh.data(reftet_c).layout_vertex = layout_v;
        layout.data(layout_v).reftet_cells.insert(reftet_c);
        TetMeshPoint& testPoint = layout.data(layout_v).testPoint;
        if (!testPoint.is_valid())
            testPoint = misc::searchBVH(tetMesh, vector_cast<3,Vector3d>(refTetMesh.barycenter(reftet_c)));
    }
    // for each refTetMesh face, if its two incident tets belong to distinct dual cells, register it to the corresponding dual face (layout edge) with proper orientation
    for (FHandle reftet_f : refTetMesh.faces()) {
        if (refTetMesh.is_boundary(reftet_f)) {
            HFHandle reftet_hf = refTetMesh.boundary_halfface(reftet_f);
            layout.data(refTetMesh.data(refTetMesh.cell_handle(refTetMesh.opposite_halfface_handle(reftet_hf))).layout_vertex).reftet_boundary_halffaces.insert(reftet_hf);
            continue;
        }
        const array<CHandle,2> reftet_c = refTetMesh.face_cells(reftet_f);
        const array<VHandle,2> layout_v = {
            refTetMesh.data(reftet_c[0]).layout_vertex,
            refTetMesh.data(reftet_c[1]).layout_vertex
        };
        if (layout_v[0] == layout_v[1])
            continue;
        bool found = false;
        for (HEHandle layout_he : layout.vertex_outgoing_halfedges(layout_v[0])) {
            if (layout.to_vertex(layout_he) == layout_v[1]) {
                layout.data(layout_he).reftet_halffaces.insert(refTetMesh.halfface_handle(reftet_f, 0));
                layout.data(layout.opposite_halfedge_handle(layout_he)).reftet_halffaces.insert(refTetMesh.halfface_handle(reftet_f, 1));
                found = true;
                TetMeshPoint& testPoint = layout.data(layout.edge_handle(layout_he)).testPoint;
                if (!testPoint.is_valid())
                    testPoint = misc::searchBVH(tetMesh, vector_cast<3,Vector3d>(refTetMesh.barycenter(reftet_f)));
                break;
            }
        }
        assert(found);
    }
    // for each refTetMesh edge, if its incident tets belong to 4 distinct dual cells, register it to the corresponding dual edge (layout face) with proper orientation
    for (EHandle reftet_e : refTetMesh.edges()) {
        if (refTetMesh.is_boundary(reftet_e)) {
            unordered_set<VHandle> layout_v;
            for (CHandle reftet_c : refTetMesh.edge_cells(reftet_e)) {
                layout_v.insert(refTetMesh.data(reftet_c).layout_vertex);
            }
            assert(layout_v.size() == 1 || layout_v.size() == 2);
            if (layout_v.size() == 2) {
                auto layout_v1 = layout_v.begin();
                auto layout_v0 = layout_v1++;
                const array<ElemCode,2> vcode = {
                    layout.data(*layout_v0).code,
                    layout.data(*layout_v1).code
                };
                refTetMesh.data(reftet_e).layout_boundary_edge = layout.edgeByCode.find(helper::getEdgeCode(vcode))->second;
            }
            continue;
        }
        vector<VHandle> unique_layout_v;
        for (CHandle reftet_c : refTetMesh.edge_cells(reftet_e)) {
            const VHandle layout_v = refTetMesh.data(reftet_c).layout_vertex;
            if (unique_layout_v.empty() || unique_layout_v.back() != layout_v)
                unique_layout_v.push_back(layout_v);
        }
        if (unique_layout_v.size() >= 3 && unique_layout_v.front() == unique_layout_v.back())
            unique_layout_v.pop_back();
        assert(unique_layout_v.size() == 1 || unique_layout_v.size() == 2 || unique_layout_v.size() == 4);
        if (unique_layout_v.size() != 4)
            continue;
        bool found = false;
        for (FHandle layout_f : layout.vertex_faces(unique_layout_v[0])) {
            vector<VHandle> layout_v = layout.face_vertices(layout_f);
            vector<VHandle> layout_v_reversed = layout_v;
            boost::reverse(layout_v_reversed);
            container_util::bring_front(layout_v, unique_layout_v[0]);
            container_util::bring_front(layout_v_reversed, unique_layout_v[0]);
            const bool found_forward = boost::equal(unique_layout_v, layout_v);
            const bool found_backward = boost::equal(unique_layout_v, layout_v_reversed);
            if (!found_forward && !found_backward)
                continue;
            layout.data(layout.halfface_handle(layout_f, found_forward ? 1 : 0)).reftet_halfedges.push_back(refTetMesh.halfedge_handle(reftet_e,0));
            layout.data(layout.halfface_handle(layout_f, found_forward ? 0 : 1)).reftet_halfedges.push_back(refTetMesh.halfedge_handle(reftet_e,1));
            found = true;
            break;
        }
        assert(found);
    }
    // for each refTetMesh vertex, if its incident tets belong to 8 distinct cells, register it to the corresponding dual vertex (layout cell)
    for (VHandle reftet_v : refTetMesh.vertices()) {
        set<VHandle> unique_layout_v;
        for (CHandle reftet_c : refTetMesh.vertex_cells(reftet_v)) {
            unique_layout_v.insert(refTetMesh.data(reftet_c).layout_vertex);
        }
        assert(unique_layout_v.size() == 1 || unique_layout_v.size() == 2 || unique_layout_v.size() == 4 || unique_layout_v.size() == 8);
        if (refTetMesh.is_boundary(reftet_v)) {
            if (unique_layout_v.size() == 4) {
                bool found = false;
                for (FHandle layout_f : layout.vertex_faces(*unique_layout_v.begin())) {
                    const auto layout_v = container_util::cast<set<VHandle>>(layout.face_vertices(layout_f));
                    if (boost::equal(unique_layout_v, layout_v)) {
                        assert(layout.is_boundary(layout_f));
                        refTetMesh.data(reftet_v).layout_boundary_face = layout_f;
                        found = true;
                        break;
                    }
                }
                assert(found);
            }
            continue;
        }
        if (unique_layout_v.size() != 8)
            continue;
        bool found = false;
        for (CHandle layout_c : layout.vertex_cells(*unique_layout_v.begin())) {
            const auto layout_v = container_util::cast<set<VHandle>>(layout.cell_vertices(layout_c));
            if (boost::equal(unique_layout_v, layout_v)) {
                layout.data(layout_c).reftet_vertex = reftet_v;
                refTetMesh.data(reftet_v).layout_cell = layout_c;
                found = true;
                break;
            }
        }
        assert(found);
    }
    // for each dual edge (layout face), sort the set of refTetMesh halfedges so that the sequence becomes a continuous path between dual vertices
    for (FHandle layout_f : layout.faces()) {
        array<HFHandle,2> layout_hf;
        for (int m = 0; m < 2; ++m) {
            layout_hf[m] = layout.halfface_handle(layout_f,m);
        }
        if (layout.is_boundary(layout_hf[0])) {
            swap(layout_hf[0], layout_hf[1]);
        }
        vector<HEHandle>& reftet_halfedges = layout.data(layout_hf[0]).reftet_halfedges;
        auto reftet_halfedges_unsorted = container_util::cast<unordered_set<HEHandle>>(reftet_halfedges);
        reftet_halfedges.clear();
        VHandle reftet_v_from = layout.data(layout.cell_handle(layout_hf[0])).reftet_vertex;
        while (!reftet_halfedges_unsorted.empty()) {
            bool found = false;
            for (HEHandle reftet_he : reftet_halfedges_unsorted) {
                if (refTetMesh.from_vertex(reftet_he) == reftet_v_from) {
                    reftet_halfedges_unsorted.erase(reftet_he);
                    reftet_halfedges.push_back(reftet_he);
                    reftet_v_from = refTetMesh.to_vertex(reftet_he);
                    found = true;
                    break;
                }
            }
            assert(found);
        }
        if (layout.is_boundary(layout_f))
            assert(refTetMesh.is_boundary(refTetMesh.to_vertex(reftet_halfedges.back())));
        else
            assert(refTetMesh.to_vertex(reftet_halfedges.back()) == layout.data(layout.cell_handle(layout_hf[1])).reftet_vertex);
        // copy to the opposite
        vector<HEHandle>& reftet_halfedges_opposite = layout.data(layout_hf[1]).reftet_halfedges;
        reftet_halfedges_opposite = reftet_halfedges;
        boost::reverse(reftet_halfedges_opposite);
        for (HEHandle& reftet_he : reftet_halfedges_opposite)
            reftet_he = refTetMesh.opposite_halfedge_handle(reftet_he);
    }
    // for each layout boundary edge, collect an array of refTetMesh boundary edges with proper orientation and ordering
    for (EHandle layout_e : layout.boundary_edges()) {
        const auto layout_v = container_util::cast<set<VHandle>>(layout.edge_vertices(layout_e));
        const HEHandle layout_he0 = layout.halfedge_handle(layout_e,0);
        const vector<HFHandle> layout_hf = layout.halfedge_halffaces(layout_he0);
        VHandle reftet_v_from = refTetMesh.from_vertex(layout.data(layout_hf.back()).reftet_halfedges.front());
        const VHandle reftet_v_to = refTetMesh.to_vertex(layout.data(layout_hf.front()).reftet_halfedges.back());
        assert(refTetMesh.is_boundary(reftet_v_from) && refTetMesh.is_boundary(reftet_v_to));
        vector<HEHandle>& reftet_halfedges = layout.data(layout_he0).reftet_boundary_halfedges;
        while (true) {
            bool found = false;
            for (HEHandle reftet_he : refTetMesh.boundary_vertex_outgoing_halfedges(reftet_v_from)) {
                if (!reftet_halfedges.empty() && reftet_he == refTetMesh.opposite_halfedge_handle(reftet_halfedges.back()))
                    continue;
                set<VHandle> layout_v2;
                for (CHandle reftet_c : refTetMesh.halfedge_cells(reftet_he)) {
                    layout_v2.insert(refTetMesh.data(reftet_c).layout_vertex);
                }
                if (boost::equal(layout_v, layout_v2)) {
                    reftet_halfedges.push_back(reftet_he);
                    reftet_v_from = refTetMesh.to_vertex(reftet_he);
                    found = true;
                    break;
                }
            }
            assert(found);
            if (reftet_v_from == reftet_v_to)
                break;
        }
        // copy to the opposite
        vector<HEHandle>& reftet_halfedges_opposite = layout.data(layout.halfedge_handle(layout_e,1)).reftet_boundary_halfedges;
        reftet_halfedges_opposite = reftet_halfedges;
        boost::reverse(reftet_halfedges_opposite);
        for (HEHandle& reftet_he : reftet_halfedges_opposite)
            reftet_he = refTetMesh.opposite_halfedge_handle(reftet_he);
    }
    // compute vertex signature and establish mapping from this dual cell's corners to the corresponding reference polyhedron's corners
    for (VHandle layout_v : layout.vertices()) {
        array<int,3>& signature = layout.data(layout_v).signature;
        signature = {0,0,0};
        for (EHandle layout_e : layout.vertex_edges(layout_v)) {
            const int ngon = layout.edge_cells(layout_e).size() + (layout.is_boundary(layout_e) ? 2 : 0);
            assert(3 <= ngon && ngon <= 5);
            signature[ngon-3]++;
        }
        if (layout.is_boundary(layout_v)) {
            const int ngon = layout.boundary_vertex_edges(layout_v).size();
            assert(3 <= ngon && ngon <= 5);
            signature[ngon-3]++;
        }
        cout << "signature for layout vertex " << layout_v << ": " << Map<RowVector3i>(signature.data()) << endl;
        // prepare input to isomorphism
        vector<pair<int,int>> dualCell_edges;
        unordered_set<FHandle> processed_layout_faces;
        for (EHandle layout_e : layout.vertex_edges(layout_v)) {
            for (FHandle layout_f : layout.edge_faces(layout_e)) {
                if (processed_layout_faces.find(layout_f) == processed_layout_faces.end()) {
                    processed_layout_faces.insert(layout_f);
                    const vector<HEHandle>& reftet_halfedges = layout.data(layout.halfface_handle(layout_f,0)).reftet_halfedges;
                    dualCell_edges.push_back({
                        refTetMesh.from_vertex(reftet_halfedges.front()).idx(),
                        refTetMesh.to_vertex  (reftet_halfedges.back ()).idx()
                    });
                }
            }
        }
        if (layout.is_boundary(layout_v)) {
            for (HEHandle layout_he : layout.boundary_vertex_outgoing_halfedges(layout_v)) {
                const vector<HEHandle>& reftet_boundary_halfedges = layout.data(layout_he).reftet_boundary_halfedges;
                dualCell_edges.push_back({
                    refTetMesh.from_vertex(reftet_boundary_halfedges.front()).idx(),
                    refTetMesh.to_vertex  (reftet_boundary_halfedges.back ()).idx()
                });
            }
        }
        const map<int, int> m = kt84::isomorphism(dualCell_edges, ReferencePolyhedron::get(signature).edges);
        assert(!m.empty());
        for (const pair<int,int>& p : m)
            layout.data(layout_v).dualCellCorner_to_referencePolyhedronCorner[VHandle{p.first}] = p.second;
        // check orientation
        vector<int> referencePolyhedron_face_vertices;
        const HEHandle layout_he = layout.vertex_outgoing_halfedges(layout_v).front();
        bool first = true;
        for (HFHandle layout_hf : layout.halfedge_halffaces(layout_he)) {
            if (first && layout.is_boundary(layout_he)) {
                const VHandle reftet_v = refTetMesh.to_vertex(layout.data(layout_hf).reftet_halfedges.back());
                referencePolyhedron_face_vertices.push_back(layout.data(layout_v).dualCellCorner_to_referencePolyhedronCorner.find(reftet_v)->second);
                first = false;
            }
            const VHandle reftet_v = refTetMesh.from_vertex(layout.data(layout_hf).reftet_halfedges.front());
            referencePolyhedron_face_vertices.push_back(layout.data(layout_v).dualCellCorner_to_referencePolyhedronCorner.find(reftet_v)->second);
        }
        layout.data(layout_v).flipped = ReferencePolyhedron::get(signature).is_face_flipped(referencePolyhedron_face_vertices);
    }
    // register the feature graph information to the refined tet mesh simply by comparing geometry
    for (int v = 0; v < triMesh.V.cols(); ++v) {
        if (triMesh.vertex_constraint[v]) {
            bool found = false;
            for (VHandle reftet_v : refTetMesh.boundary_vertices()) {
                const double d = (vector_cast<3,Vector3d>(refTetMesh.vertex(reftet_v)) - vector_cast<3,Vector3d>(triMesh.V.col(v))).norm();
                if (d == 0) {
                    assert(!refTetMesh.data(reftet_v).constrained);
                    refTetMesh.data(reftet_v).constrained = true;
                    found = true;
                    break;
                }
            }
            assert(found);
        }
    }
    for (int e = 0; e < triMesh.edges.size(); ++e) {
        if (triMesh.edge_constraint[e]) {
            const auto p0 = vector_cast<3,Vector3d>(triMesh.V.col(triMesh.edges[e][0]));
            const auto p1 = vector_cast<3,Vector3d>(triMesh.V.col(triMesh.edges[e][1]));
            bool found = false;
            for (EHandle reftet_e : refTetMesh.boundary_edges()) {
                const auto reftet_v = refTetMesh.edge_vertices(reftet_e);
                array<bool,2> check;
                for (int i = 0; i < 2; ++i) {
                    const auto q = vector_cast<3,Vector3d>(refTetMesh.vertex(reftet_v[i]));
                    Vector2d t;
                    const double d = *eigen_util::distance_to_line(p0, p1, q, false, &t);
                    static const double epsilon = 1.0e-14;              // actual typical range of error is 1.0e-16 ~ 1.0e-17
                    check[i] = (d < epsilon && t.minCoeff() >= 0 && t.maxCoeff() <= 1);
                }
                if (check[0] && check[1]) {
                    assert(!refTetMesh.data(reftet_e).constrained);
                    refTetMesh.data(reftet_e).constrained = true;
                    found = true;
                }
            }
            assert(found);
        }
    }
    // transfer feature graph info stored on ref tet mesh to layout
    // step 1: constrained vertex info
    for (VHandle layout_v : layout.boundary_vertices()) {
        for (HFHandle reftet_hf : layout.data(layout_v).reftet_boundary_halffaces) {
            bool found = false;
            for (VHandle reftet_v : refTetMesh.halfface_vertices(reftet_hf)) {
                if (refTetMesh.data(reftet_v).constrained) {
                    layout.data(layout_v).reftet_constrained_vertex = reftet_v;
                    found = true;
                    break;
                }
            }
            if (found) break;
        }
    }
    // step 2: collect all constrained reftet edges for each layout boundary vertex
    unordered_map<VHandle, unordered_set<EHandle>> reftet_constrained_edges_per_layout_vertex;
    for (EHandle reftet_e : refTetMesh.boundary_edges()) {
        if (refTetMesh.data(reftet_e).constrained) {
            set<VHandle> layout_v;
            for (CHandle reftet_c : refTetMesh.edge_cells(reftet_e))
                layout_v.insert(refTetMesh.data(reftet_c).layout_vertex);
            assert(layout_v.size()==1);
            assert(layout.is_boundary(*layout_v.begin()));
            reftet_constrained_edges_per_layout_vertex[*layout_v.begin()].insert(reftet_e);
        }
    }
    // step 3: visit all layout boundary vertices that contain constrained reftet vertex or constrained reftet edges (or both), then sort edges
    for (const pair<VHandle, unordered_set<EHandle>>& p : reftet_constrained_edges_per_layout_vertex) {
        const VHandle layout_v = p.first;
        const unordered_set<EHandle>& reftet_constrained_edges = p.second;
        if (reftet_constrained_edges.size() == 1) {
            // super edge case where only one reftet constrained edge exists in the boundary dual face --> don't bother with reftet_constrained_halfedges_per_boundary_outgoing_halfedge
            for (VHandle reftet_v : refTetMesh.edge_vertices(*reftet_constrained_edges.begin())) {
                vector<EHandle> layout_e;
                for (EHandle reftet_e : refTetMesh.boundary_vertex_edges(reftet_v)) {
                    if (refTetMesh.data(reftet_e).layout_boundary_edge.is_valid())
                        layout_e.push_back(refTetMesh.data(reftet_e).layout_boundary_edge);
                }
                assert(layout_e.size()==2 && layout_e[0]==layout_e[1]);
                layout.data(layout_e[0]).reftet_constrained_vertex = reftet_v;
            }
            continue;
        }
        unordered_set<EHandle> added_reftet_edges;
        VHandle& reftet_constrained_vertex = layout.data(layout_v).reftet_constrained_vertex;
        vector<deque<HEHandle>> reftet_constrained_halfedges;      // continuous chains of halfedges starting from reftet_constrained_vertex
        if (reftet_constrained_vertex.is_valid()) {
            // all relevant constrained reftet edges must form a set of distinct continous sequence of edges
            while (added_reftet_edges.size() < reftet_constrained_edges.size()) {
                HEHandle reftet_he;
                for (HEHandle reftet_he2 : refTetMesh.boundary_vertex_outgoing_halfedges(reftet_constrained_vertex)) {
                    const EHandle reftet_e2 = refTetMesh.edge_handle(reftet_he2);
                    if (reftet_constrained_edges.count(reftet_e2)==1 && added_reftet_edges.count(reftet_e2)==0) {
                        reftet_he = reftet_he2;
                        break;
                    }
                }
                assert(reftet_he.is_valid());
                reftet_constrained_halfedges.push_back({});
                while (true) {
                    added_reftet_edges.insert(refTetMesh.edge_handle(reftet_he));
                    reftet_constrained_halfedges.back().push_back(reftet_he);
                    const VHandle reftet_v = refTetMesh.to_vertex(reftet_he);
                    HEHandle reftet_he_next;
                    for (EHandle reftet_e : reftet_constrained_edges) {
                        if (added_reftet_edges.count(reftet_e)==0 && refTetMesh.is_incident(reftet_e, reftet_v)) {
                            reftet_he_next = refTetMesh.halfedge_handle(reftet_e,0);
                            if (refTetMesh.to_vertex(reftet_he_next) == reftet_v)
                                reftet_he_next = refTetMesh.halfedge_handle(reftet_e,1);
                            assert(refTetMesh.from_vertex(reftet_he_next) == reftet_v);
                            break;
                        }
                    }
                    if (reftet_he_next.is_valid()) {
                        reftet_he = reftet_he_next;
                    } else {
                        break;
                    }
                }
            }
        } else {
            layout.data(layout_v).pseudo_constrained = true;
            // this dual cell is passed by a single continous sequence of constrained reftet edges
            reftet_constrained_halfedges.resize(2);
            const HEHandle reftet_he_start = refTetMesh.halfedge_handle(*reftet_constrained_edges.begin(),0);
            // move forward
            HEHandle reftet_he = reftet_he_start;
            while (true) {
                added_reftet_edges.insert(refTetMesh.edge_handle(reftet_he));
                reftet_constrained_halfedges[0].push_back(reftet_he);
                const VHandle reftet_v = refTetMesh.to_vertex(reftet_he);
                HEHandle reftet_he_next;
                for (EHandle reftet_e : reftet_constrained_edges) {
                    if (added_reftet_edges.count(reftet_e)==0 && refTetMesh.is_incident(reftet_e, reftet_v)) {
                        reftet_he_next = refTetMesh.halfedge_handle(reftet_e,0);
                        if (refTetMesh.to_vertex(reftet_he_next) == reftet_v)
                            reftet_he_next = refTetMesh.halfedge_handle(reftet_e,1);
                        assert(refTetMesh.from_vertex(reftet_he_next) == reftet_v);
                        break;
                    }
                }
                if (reftet_he_next.is_valid()) {
                    reftet_he = reftet_he_next;
                } else {
                    break;
                }
            }
            // move backward
            reftet_he = reftet_he_start;
            while (true) {
                if (reftet_he != reftet_he_start) {
                    added_reftet_edges.insert(refTetMesh.edge_handle(reftet_he));
                    reftet_constrained_halfedges[0].push_front(reftet_he);
                }
                const VHandle reftet_v = refTetMesh.from_vertex(reftet_he);
                HEHandle reftet_he_next;
                for (EHandle reftet_e : reftet_constrained_edges) {
                    if (added_reftet_edges.count(reftet_e)==0 && refTetMesh.is_incident(reftet_e, reftet_v)) {
                        reftet_he_next = refTetMesh.halfedge_handle(reftet_e,0);
                        if (refTetMesh.from_vertex(reftet_he_next) == reftet_v)
                            reftet_he_next = refTetMesh.halfedge_handle(reftet_e,1);
                        assert(refTetMesh.to_vertex(reftet_he_next) == reftet_v);
                        break;
                    }
                }
                if (reftet_he_next.is_valid()) {
                    reftet_he = reftet_he_next;
                } else {
                    break;
                }
            }
            // identify the middle vertex and split the sequence into two
            double total_length = 0;
            for (HEHandle reftet_he : reftet_constrained_halfedges[0])
                total_length += refTetMesh.length(reftet_he);
            double t = 0;
            for (auto reftet_he = reftet_constrained_halfedges[0].begin(); t < 0.5 && reftet_he != reftet_constrained_halfedges[0].end()-1; ) {
                t += refTetMesh.length(*reftet_he) / total_length;
                reftet_constrained_halfedges[1].push_front(refTetMesh.opposite_halfedge_handle(*reftet_he));
                reftet_he = reftet_constrained_halfedges[0].erase(reftet_he);
            }
            reftet_constrained_vertex = refTetMesh.from_vertex(reftet_constrained_halfedges[0].front());
            assert(reftet_constrained_vertex == refTetMesh.from_vertex(reftet_constrained_halfedges[1].front()));
            assert(added_reftet_edges.size() == reftet_constrained_halfedges[0].size() + reftet_constrained_halfedges[1].size());
        }
        assert(added_reftet_edges.size() == reftet_constrained_edges.size());
        unordered_map<EHandle, vector<HEHandle>> reftet_constrained_halfedges_per_layout_boundary_edge;
        for (auto& i : reftet_constrained_halfedges) {
            assert(!i.empty() && refTetMesh.from_vertex(i.front()) == reftet_constrained_vertex);
            // identify which layout edge this sequence belongs to by looking at the last halfedge
            const VHandle reftet_v = refTetMesh.to_vertex(i.back());
            vector<EHandle> layout_e;
            for (EHandle reftet_e : refTetMesh.boundary_vertex_edges(reftet_v)) {
                if (refTetMesh.data(reftet_e).layout_boundary_edge.is_valid())
                    layout_e.push_back(refTetMesh.data(reftet_e).layout_boundary_edge);
            }
            assert(layout_e.size()==2 && layout_e[0]==layout_e[1]);
            reftet_constrained_halfedges_per_layout_boundary_edge[layout_e[0]] = container_util::cast<vector<HEHandle>>(i);
            layout.data(layout_e[0]).reftet_constrained_vertex = reftet_v;
        }
        const int ngon = layout.boundary_vertex_edges(layout_v).size();
        layout.data(layout_v).reftet_constrained_halfedges_per_boundary_outgoing_halfedge.resize(ngon);
        int k = 0;
        for (EHandle layout_e : layout.boundary_vertex_edges(layout_v)) {
            layout.data(layout_v).reftet_constrained_halfedges_per_boundary_outgoing_halfedge[k++] = reftet_constrained_halfedges_per_layout_boundary_edge[layout_e];
        }
    }
}
void dsm::core::computeHexLayoutGeometry_step2_parameterize_dualFace(HexLayout& layout, const RefinedTetMesh& refTetMesh) {
    for (EHandle layout_e : layout.edges()) {
        ScopedTimerPrinter timer{"Parameterizing dual face " + to_string(layout_e.idx()+1) + "/" + to_string(layout.n_edges())};
        const bool is_boundary = layout.is_boundary(layout_e);
        const HEHandle layout_he = layout.halfedge_handle(layout_e,0);
        // extract triangle mesh corresponding to this dual face
        MatrixXd dualFace_V(0,3);
        MatrixXi dualFace_F(0,3);
        map<VHandle,int> vidxMap_global2local;
        map<int,VHandle> vidxMap_local2global;
        for (HFHandle ref_hf : layout.data(layout_he).reftet_halffaces) {
            dualFace_F.conservativeResize(dualFace_F.rows()+1, 3);
            int i = 0;
            for (VHandle ref_v : refTetMesh.halfface_vertices(ref_hf)) {
                if (vidxMap_global2local.find(ref_v) == vidxMap_global2local.end()) {
                    vidxMap_global2local[ref_v] = dualFace_V.rows();
                    vidxMap_local2global[dualFace_V.rows()] = ref_v;
                    dualFace_V.conservativeResize(dualFace_V.rows()+1, 3);
                    dualFace_V.row(dualFace_V.rows()-1) = vector_cast<3,Vector3d>(refTetMesh.vertex(ref_v));
                }
                dualFace_F(dualFace_F.rows()-1, i++) = vidxMap_global2local[ref_v];
            }
        }
        const int nV = dualFace_V.rows();
        // build Laplacian matrix
        SparseMatrixd L;
        igl::cotmatrix(dualFace_V, dualFace_F, L);
        const SparseMatrixd Q = -L;                 // Harmonic parameterization (Dirichlet energy, see igl::harmonic)
        // set constraints along the boundary
        VectorXi b;             // indices of constrained vertices
        Matrix<double,-1,2> bc; // fixed values for constrained vertices
        const int ngon = layout.edge_cells(layout_e).size() + (is_boundary ? 2 : 0);
        const double theta = 2*util::pi()/ngon;
        const Rotation2D<double> R(theta);
        Vector2d uv(tan(util::pi()/2 - theta/2), -1);
        int k = 0;
        if (is_boundary) {
            const VHandle reftet_constrained_vertex = layout.data(layout_e).reftet_constrained_vertex;
            bool second_half = false;
            double total_length = 0;
            array<double,2> total_length_split = {0,0};
            for (HEHandle reftet_he : layout.data(layout_he).reftet_boundary_halfedges) {
                if (reftet_constrained_vertex.is_valid()) {
                    if (refTetMesh.from_vertex(reftet_he) == reftet_constrained_vertex) {
                        second_half = true;
                    }
                    total_length_split[second_half] += refTetMesh.length(reftet_he);
                } else {
                    total_length += refTetMesh.length(reftet_he);
                }
            }
            if (reftet_constrained_vertex.is_valid()) {
                assert(second_half);
            }
            const Vector2d uv_next = R*uv;
            const Vector2d uv_prev = R.inverse()*uv;
            const Vector2d uv_next2 = R*uv_next;
            double t = 0;
            second_half = false;
            for (HEHandle reftet_he : layout.data(layout_he).reftet_boundary_halfedges) {
                b.conservativeResize(b.size()+1);
                bc.conservativeResize(bc.rows()+1, 2);
                b[b.size()-1] = vidxMap_global2local.find(refTetMesh.from_vertex(reftet_he))->second;
                double t2;
                array<Vector2d,2> endpoints;
                if (t < 0.5) {
                    t2 = 2*t;
                    endpoints = { 0.5*(uv_prev + uv), Vector2d::Zero() };
                } else {
                    t2 = 2*(t-0.5);
                    endpoints = { Vector2d::Zero(), 0.5*(uv_next + uv_next2) };
                }
                bc.row(bc.rows()-1) = (1-t2)*endpoints[0] + t2*endpoints[1];
                if (reftet_constrained_vertex.is_valid()) {
                    if (refTetMesh.from_vertex(reftet_he) == reftet_constrained_vertex) {
                        second_half = true;
                    }
                    t += 0.5 * refTetMesh.length(reftet_he) / total_length_split[second_half];
                } else {
                    t += refTetMesh.length(reftet_he) / total_length;
                }
            }
            uv = uv_next;
            k = 1;
        }
        for (HFHandle layout_hf : layout.halfedge_halffaces(layout_he)) {
            double total_length = 0;
            for (HEHandle reftet_he : layout.data(layout.opposite_halfface_handle(layout_hf)).reftet_halfedges) {
                total_length += refTetMesh.length(reftet_he);
            }
            const Vector2d uv_next = R*uv;
            double t = 0;
            for (HEHandle reftet_he : layout.data(layout.opposite_halfface_handle(layout_hf)).reftet_halfedges) {
                b.conservativeResize(b.size()+1);
                bc.conservativeResize(bc.rows()+1, 2);
                b[b.size()-1] = vidxMap_global2local.find(refTetMesh.from_vertex(reftet_he))->second;
                const double t2 = is_boundary && k == 1 ? 0.5 + 0.5*t : is_boundary && k==ngon-1 ? 0.5*t : t;
                bc.row(bc.rows()-1) = (1-t2)*uv + t2*uv_next;
                t += refTetMesh.length(reftet_he) / total_length;
            }
            uv = uv_next;
            ++k;
        }
        assert(k == ngon);
        MatrixXd dualFace_P(nV, 2);
        if (b.size() == nV) {
            // degenerate case with no unknown --> simply copy fixed values
            for (int i = 0; i < nV; ++i) {
                dualFace_P.row(b[i]) = bc.row(i);
            }
        } else {
            igl::min_quad_with_fixed_data<double> mqwf;
            igl::min_quad_with_fixed_precompute(Q, b, SparseMatrixd(), true, mqwf);
            for (int i = 0; i < 2; ++i) {
                VectorXd tmp;
                igl::min_quad_with_fixed_solve(mqwf, VectorXd::Zero(nV), bc.col(i), VectorXd(), tmp);
                dualFace_P.col(i) = tmp;
            }
        }
        unordered_map<VHandle, Vector2d>& param2d = layout.data(layout_e).param2d;
        param2d.clear();
        for (int v = 0; v < nV; ++v)
            param2d[vidxMap_local2global[v]] = dualFace_P.row(v);
    }
    // for each boundary layout vertex, parameterize the corresponding boundary dual face (intersection of dual cell and boundary surface)
    for (VHandle layout_v : layout.boundary_vertices()) {
        ScopedTimerPrinter timer{"Parameterizing boundary dual face " + to_string(layout.boundary_vertex_idx(layout_v)+1) + "/" + to_string(layout.n_boundary_vertices())};
        // extract triangle mesh corresponding to this boundary dual face
        MatrixXd dualFace_V(0,3);
        MatrixXi dualFace_F(0,3);
        map<VHandle,int> vidxMap_global2local;
        map<int,VHandle> vidxMap_local2global;
        for (HFHandle reftet_hf : layout.data(layout_v).reftet_boundary_halffaces) {
            dualFace_F.conservativeResize(dualFace_F.rows()+1, 3);
            int i = 0;
            for (VHandle reftet_v : refTetMesh.halfface_vertices(reftet_hf)) {
                if (vidxMap_global2local.find(reftet_v) == vidxMap_global2local.end()) {
                    vidxMap_global2local[reftet_v] = dualFace_V.rows();
                    vidxMap_local2global[dualFace_V.rows()] = reftet_v;
                    dualFace_V.conservativeResize(dualFace_V.rows()+1, 3);
                    dualFace_V.row(dualFace_V.rows()-1) = vector_cast<3,Vector3d>(refTetMesh.vertex(reftet_v));
                }
                dualFace_F(dualFace_F.rows()-1, i++) = vidxMap_global2local[reftet_v];
            }
        }
        const int nV = dualFace_V.rows();
        // build Laplacian matrix
        SparseMatrixd L;
        igl::cotmatrix(dualFace_V, dualFace_F, L);
        const SparseMatrixd Q = -L;                 // Harmonic parameterization (Dirichlet energy, see igl::harmonic)
        // set constraints along the boundary
        VectorXi b;             // indices of constrained vertices
        Matrix<double,-1,2> bc; // fixed values for constrained vertices
        const int ngon = layout.boundary_vertex_edges(layout_v).size();
        const double theta = 2*util::pi()/ngon;
        const Rotation2D<double> R(theta);
        Vector2d uv(tan(util::pi()/2 - theta/2), -1);
        int k = 0;
        for (HEHandle layout_he : layout.boundary_vertex_outgoing_halfedges(layout_v)) {
            const VHandle reftet_constrained_vertex = layout.data(layout.edge_handle(layout_he)).reftet_constrained_vertex;
            bool second_half = false;
            double total_length = 0;
            array<double,2> total_length_split = {0,0};
            for (HEHandle reftet_he : layout.data(layout.opposite_halfedge_handle(layout_he)).reftet_boundary_halfedges) {
                if (reftet_constrained_vertex.is_valid()) {
                    if (refTetMesh.from_vertex(reftet_he) == reftet_constrained_vertex) {
                        second_half = true;
                    }
                    total_length_split[second_half] += refTetMesh.length(reftet_he);
                } else {
                    total_length += refTetMesh.length(reftet_he);
                }
            }
            if (reftet_constrained_vertex.is_valid()) {
                assert(second_half);
            }
            const Vector2d uv_next = R*uv;
            double t = 0;
            second_half = false;
            for (HEHandle reftet_he : layout.data(layout.opposite_halfedge_handle(layout_he)).reftet_boundary_halfedges) {
                b.conservativeResize(b.size()+1);
                bc.conservativeResize(bc.rows()+1, 2);
                b[b.size()-1] = vidxMap_global2local.find(refTetMesh.from_vertex(reftet_he))->second;
                bc.row(bc.rows()-1) = (1-t)*uv + t*uv_next;
                if (reftet_constrained_vertex.is_valid()) {
                    if (refTetMesh.from_vertex(reftet_he) == reftet_constrained_vertex) {
                        second_half = true;
                    }
                    t += 0.5 * refTetMesh.length(reftet_he) / total_length_split[second_half];
                } else {
                    t += refTetMesh.length(reftet_he) / total_length;
                }
            }
            uv = uv_next;
            ++k;
        }
        assert(k == ngon);
        const bool pseudo_constrained = layout.data(layout_v).pseudo_constrained;
        if (layout.data(layout_v).reftet_constrained_vertex.is_valid() && !pseudo_constrained) {
            b.conservativeResize(b.size()+1);
            bc.conservativeResize(bc.rows()+1, 2);
            b[b.size()-1] = vidxMap_global2local.find(layout.data(layout_v).reftet_constrained_vertex)->second;
            bc.row(bc.rows()-1) << 0, 0;
        }
        uv << tan(util::pi()/2 - theta/2), 0;
        if (!layout.data(layout_v).reftet_constrained_halfedges_per_boundary_outgoing_halfedge.empty()) {
            vector<double> subtotal_length(ngon,0);
            vector<int> relevant_k;
            double length_offset = 0;
            double total_length2 = 0;
            if (pseudo_constrained) {
                k = 0;
                for (HEHandle layout_he : layout.boundary_vertex_outgoing_halfedges(layout_v)) {
                    const std::vector<HEHandle>& reftet_constrained_halfedges = layout.data(layout_v).reftet_constrained_halfedges_per_boundary_outgoing_halfedge[k];
                    if (!reftet_constrained_halfedges.empty()) {
                        relevant_k.push_back(k);
                        for (HEHandle reftet_he : reftet_constrained_halfedges)
                            subtotal_length[k] += refTetMesh.length(reftet_he);
                    }
                    ++k;
                }
                assert(relevant_k.size() == 2);
                if (subtotal_length[relevant_k[0]] > subtotal_length[relevant_k[1]])
                    swap(relevant_k[0], relevant_k[1]);
                total_length2 = (subtotal_length[relevant_k[0]]+subtotal_length[relevant_k[1]])/2;
                length_offset = total_length2 - subtotal_length[relevant_k[0]];
                assert(length_offset > 0);
            }
            k = 0;
            for (HEHandle layout_he : layout.boundary_vertex_outgoing_halfedges(layout_v)) {
                const std::vector<HEHandle>& reftet_constrained_halfedges = layout.data(layout_v).reftet_constrained_halfedges_per_boundary_outgoing_halfedge[k];
                if (!reftet_constrained_halfedges.empty()) {
                    assert(refTetMesh.from_vertex(reftet_constrained_halfedges.front()) == layout.data(layout_v).reftet_constrained_vertex);
                    double total_length = 0;
                    if (pseudo_constrained) {
                        total_length = total_length2;
                    } else {
                        for (HEHandle reftet_he : reftet_constrained_halfedges)
                            total_length += refTetMesh.length(reftet_he);
                    }
                    bool first = true;
                    double t = 0;
                    if (pseudo_constrained) {
                        if (k == relevant_k[0]) {
                            t = length_offset / total_length;
                        } else {
                            assert(k == relevant_k[1]);
                            t = -length_offset / total_length;
                        }
                    }
                    for (HEHandle reftet_he : reftet_constrained_halfedges) {
                        if (!first || (pseudo_constrained && k==relevant_k[0])) {
                            b.conservativeResize(b.size()+1);
                            bc.conservativeResize(bc.rows()+1, 2);
                            b[b.size()-1] = vidxMap_global2local.find(refTetMesh.from_vertex(reftet_he))->second;
                            bc.row(bc.rows()-1) = t*uv;
                        }
                        first = false;
                        t += refTetMesh.length(reftet_he) / total_length;
                    }
                }
                uv = R * uv;
                ++k;
            }
        }
        // sanity check that there's no duplicate constraints for the same vertex
        set<int> duplicate_check;
        for (int i = 0; i < b.size(); ++i) {
            duplicate_check.insert(b[i]);
        }
        assert(duplicate_check.size() == b.size());
        MatrixXd dualFace_P(nV, 2);
        if (b.size() == nV) {
            // degenerate case with no unknown --> simply copy fixed values
            for (int i = 0; i < nV; ++i) {
                dualFace_P.row(b[i]) = bc.row(i);
            }
        } else {
            igl::min_quad_with_fixed_data<double> mqwf;
            igl::min_quad_with_fixed_precompute(Q, b, SparseMatrixd(), true, mqwf);
            for (int i = 0; i < 2; ++i) {
                VectorXd tmp;
                igl::min_quad_with_fixed_solve(mqwf, VectorXd::Zero(nV), bc.col(i), VectorXd(), tmp);
                dualFace_P.col(i) = tmp;
            }
        }
        unordered_map<VHandle, Vector2d>& boundary_param2d = layout.data(layout_v).boundary_param2d;
        boundary_param2d.clear();
        for (int v = 0; v < nV; ++v)
            boundary_param2d[vidxMap_local2global[v]] = dualFace_P.row(v);

    }
}
template <class T>
struct BilinearCorners {
    T bottomLeft, bottomRight, topLeft, topRight;
};
void dsm::core::computeHexLayoutGeometry_step3_parameterize_dualCell(HexLayout& layout, const RefinedTetMesh& refTetMesh) {
    for (VHandle layout_v : layout.vertices()) {
        ScopedTimerPrinter timer{"Parameterizing dual cell " + to_string(layout_v.idx()+1) + "/" + to_string(layout.n_vertices())};
        const bool is_boundary_v = layout.is_boundary(layout_v);
        // extract tet mesh corresponding to this dual cell
        MatrixXd dualCell_V(0,3);
        MatrixXi dualCell_C(0,4);
        map<VHandle,int> vidxMap_global2local;
        map<int,VHandle> vidxMap_local2global;
        for (CHandle reftet_c : layout.data(layout_v).reftet_cells) {
            dualCell_C.conservativeResize(dualCell_C.rows()+1, 4);
            int i = 0;
            for (VHandle reftet_v : refTetMesh.cell_vertices(reftet_c)) {
                if (vidxMap_global2local.find(reftet_v) == vidxMap_global2local.end()) {
                    vidxMap_global2local[reftet_v] = dualCell_V.rows();
                    vidxMap_local2global[dualCell_V.rows()] = reftet_v;
                    dualCell_V.conservativeResize(dualCell_V.rows()+1, 3);
                    dualCell_V.row(dualCell_V.rows()-1) = vector_cast<3,Vector3d>(refTetMesh.vertex(reftet_v));
                }
                dualCell_C(dualCell_C.rows()-1, i++) = vidxMap_global2local[reftet_v];
            }
        }
        const int nV = dualCell_V.rows();
        // build Laplacian matrix
        SparseMatrixd L;
        igl::cotmatrix(dualCell_V, dualCell_C, L);
        const SparseMatrixd Q = -L;                 // Harmonic parameterization (Dirichlet energy, see igl::harmonic)
        const unordered_map<VHandle, int>& dualCellCorner_to_referencePolyhedronCorner = layout.data(layout_v).dualCellCorner_to_referencePolyhedronCorner;
        const auto& referencePolyhedron = ReferencePolyhedron::get(layout.data(layout_v).signature);
        // set constraints along the boundary
        VectorXi b;             // indices of constrained vertices
        Matrix<double,-1,3> bc; // fixed values for constrained vertices
        unordered_map<VHandle, Vector3d> alreadyConstrained;            // wedge (corner) vetex will be visited twice (thrice), so make sure the constraint is given only once (and check for consistency)
        auto getCanonicalCorners = [](int ngon) -> BilinearCorners<Vector2d> {
            const double theta = 2*util::pi()/ngon;
            const Rotation2D<double> R(theta);
            BilinearCorners<Vector2d> c2d;
            c2d.bottomLeft  = { tan(util::pi()/2 - theta/2), -1 };
            c2d.bottomRight = { c2d.bottomLeft.x(), 0 };
            c2d.topLeft     = R.inverse() * c2d.bottomRight;
            c2d.topRight    = Vector2d::Zero();
            return c2d;
        };
        if (is_boundary_v) {
            const int ngon_top = layout.boundary_vertex_edges(layout_v).size();
            vector<Vector3d> boundaryPolygon_midpoints;
            vector<Vector3d> boundaryPolygon_centroids;
            // looking from this boundary polygon corresponding to the boundary dual face, we need to collect:
            //   1) midpoint of edges emanating from each of the boundary polygon's corners to another corner not belonging to the boundary polygon 
            //   2) centroid of polygons incident to the boundary polygon
            for (HFHandle layout_hf : layout.boundary_vertex_halffaces(layout_v)) {
                const vector<HEHandle>& reftet_halfedges = layout.data(layout_hf).reftet_halfedges;
                const Vector3d p0 = referencePolyhedron.V.row(dualCellCorner_to_referencePolyhedronCorner.find(refTetMesh.from_vertex(reftet_halfedges.front()))->second);
                const Vector3d p1 = referencePolyhedron.V.row(dualCellCorner_to_referencePolyhedronCorner.find(refTetMesh.to_vertex(reftet_halfedges.back()))->second);
                boundaryPolygon_midpoints.push_back(0.5*(p0+p1));
            }
            for (HEHandle layout_he : layout.boundary_vertex_outgoing_halfedges(layout_v)) {
                const int ngon_side = layout.edge_cells(layout.edge_handle(layout_he)).size() + 2;
                vector<Vector3d> polygonCorners;
                bool first = true;
                for (HFHandle layout_hf : layout.halfedge_halffaces(layout_he)) {
                    const vector<HEHandle>& reftet_halfedges = layout.data(layout.opposite_halfface_handle(layout_hf)).reftet_halfedges;
                    if (first) {
                        assert(layout.is_boundary(layout.opposite_halfface_handle(layout_hf)));
                        polygonCorners.push_back(referencePolyhedron.V.row(dualCellCorner_to_referencePolyhedronCorner.find(refTetMesh.from_vertex(reftet_halfedges.front()))->second));
                    }
                    first = false;
                    polygonCorners.push_back(referencePolyhedron.V.row(dualCellCorner_to_referencePolyhedronCorner.find(refTetMesh.to_vertex(reftet_halfedges.back()))->second));
                }
                assert(polygonCorners.size() == ngon_side);
                boundaryPolygon_centroids.push_back(boost::accumulate(polygonCorners, Vector3d{0,0,0}) / ngon_side);
            }
            const double theta = 2*util::pi()/ngon_top;
            const Rotation2D<double> R(theta);
            const auto c2d = getCanonicalCorners(ngon_top);
            for (const pair<VHandle,Vector2d>& p : layout.data(layout_v).boundary_param2d) {
                const VHandle& reftet_v = p.first;
                Vector2d uv = p.second;
                double theta2 = util::mod_double(atan2(uv[1], uv[0]), 2*util::pi());
                int section_idx = 0;
                while (theta2 < theta * (ngon_top-1)) {
                    theta2 = util::mod_double(theta2 + theta*(ngon_top-1), 2*util::pi());
                    uv = R.inverse() * uv;
                    section_idx++;
                }
                const Vector2d t = eigen_util::inverse_bilinear(c2d.bottomLeft, c2d.bottomRight, c2d.topLeft, c2d.topRight, uv);
                BilinearCorners<Vector3d> c3d;
                c3d.bottomLeft  = boundaryPolygon_midpoints[section_idx];
                c3d.bottomRight = boundaryPolygon_centroids[section_idx];
                c3d.topLeft     = boundaryPolygon_centroids[(section_idx+ngon_top-1) % ngon_top];
                c3d.topRight    = Vector3d::Zero();
                const Vector3d uvw = eigen_util::interpolate_bilinear(c3d.bottomLeft, c3d.bottomRight, c3d.topLeft, c3d.topRight, t);
                assert(alreadyConstrained.find(reftet_v) == alreadyConstrained.end());
                alreadyConstrained[reftet_v] = uvw;
                b.conservativeResize(b.size()+1);
                bc.conservativeResize(bc.rows()+1, 3);
                b[b.size()-1] = vidxMap_global2local.find(reftet_v)->second;
                bc.row(bc.rows()-1) = uvw;
            }
        }
        for (EHandle layout_e : layout.vertex_edges(layout_v)) {
            const bool is_boundary_e = layout.is_boundary(layout_e);
            const HEHandle layout_he = layout.halfedge_handle(layout_e,0);
            const int ngon = layout.edge_cells(layout_e).size() + (is_boundary_e ? 2 : 0);
            vector<Vector3d> polygonCorners;
            if (is_boundary_e) {
                polygonCorners.push_back(referencePolyhedron.V.row(dualCellCorner_to_referencePolyhedronCorner.find(refTetMesh.from_vertex(layout.data(layout_he).reftet_boundary_halfedges.front()))->second));
            }
            for (HFHandle layout_hf : layout.halfedge_halffaces(layout_he)) {
                polygonCorners.push_back(referencePolyhedron.V.row(dualCellCorner_to_referencePolyhedronCorner.find(refTetMesh.from_vertex(layout.data(layout.opposite_halfface_handle(layout_hf)).reftet_halfedges.front()))->second));
            }
            assert(polygonCorners.size() == ngon);
            const Vector3d polygonCentroid = boost::accumulate(polygonCorners, Vector3d{0,0,0}) / ngon;
            const double theta = 2*util::pi()/ngon;
            const Rotation2D<double> R(theta);
            const auto c2d = getCanonicalCorners(ngon);
            for (const pair<VHandle,Vector2d>& p : layout.data(layout_e).param2d) {
                const VHandle& reftet_v = p.first;
                Vector2d uv = p.second;
                double theta2 = util::mod_double(atan2(uv[1], uv[0]), 2*util::pi());
                int section_idx = 0;
                while (theta2 < theta * (ngon-1)) {
                    theta2 = util::mod_double(theta2 + theta*(ngon-1), 2*util::pi());
                    uv = R.inverse() * uv;
                    section_idx++;
                }
                const Vector2d t = eigen_util::inverse_bilinear(c2d.bottomLeft, c2d.bottomRight, c2d.topLeft, c2d.topRight, uv);
                BilinearCorners<Vector3d> c3d;
                c3d.bottomLeft  = polygonCorners[section_idx];
                c3d.bottomRight = 0.5 * (c3d.bottomLeft + polygonCorners[(section_idx+1) % ngon]);
                c3d.topLeft     = 0.5 * (c3d.bottomLeft + polygonCorners[(section_idx+ngon-1) % ngon]);
                c3d.topRight    = polygonCentroid;
                const Vector3d uvw = eigen_util::interpolate_bilinear(c3d.bottomLeft, c3d.bottomRight, c3d.topLeft, c3d.topRight, t);
                auto found = alreadyConstrained.find(reftet_v);
                if (found != alreadyConstrained.end()) {
                    const Vector3d uvw_prev = found->second;
                    const double err = (uvw - uvw_prev).norm();
                    if (err > 0.2) {
                        cout << "WARNING: Detected large discrepancy in 3D constraint values for a refined tetmesh vertex shared by multiple dual faces (error = " << err << ")! Consider using tighter quality setting for tetrahedralization." << endl;
                    }
                } else {
                    alreadyConstrained[reftet_v] = uvw;
                    b.conservativeResize(b.size()+1);
                    bc.conservativeResize(bc.rows()+1, 3);
                    b[b.size()-1] = vidxMap_global2local.find(reftet_v)->second;
                    bc.row(bc.rows()-1) = uvw;
                }
            }
        }
        MatrixXd dualCell_P(nV, 3);
        if (b.size() == nV) {
            // degenerate case with no unknown --> simply copy fixed values
            for (int i = 0; i < nV; ++i) {
                dualCell_P.row(b[i]) = bc.row(i);
            }
        } else {
            igl::min_quad_with_fixed_data<double> mqwf;
            igl::min_quad_with_fixed_precompute(Q, b, SparseMatrixd(), true, mqwf);
            for (int i = 0; i < 3; ++i) {
                VectorXd tmp;
                igl::min_quad_with_fixed_solve(mqwf, VectorXd::Zero(nV), bc.col(i), VectorXd(), tmp);
                dualCell_P.col(i) = tmp;
            }
        }
        unordered_map<VHandle, Vector3d>& param3d = layout.data(layout_v).param3d;
        param3d.clear();
        for (int v = 0; v < nV; ++v)
            param3d[vidxMap_local2global[v]] = dualCell_P.row(v);
    }
}

namespace dsm {

template <class Base>
struct WeightedHandle : public Base {
    boost::rational<int> weight;
    WeightedHandle(const Base& base = Base{}) : Base(base) {}
    bool operator<(const WeightedHandle<Base>& rhs) const {
        return static_cast<const Base&>(*this) < static_cast<const Base&>(rhs);
    }
    bool operator==(const WeightedHandle<Base>& rhs) const {
        return static_cast<const Base&>(*this) == static_cast<const Base&>(rhs) &&
               weight == rhs.weight;
    }
};
using WVHandle = WeightedHandle<VHandle>;
using WEHandle = WeightedHandle<EHandle>;
using WFHandle = WeightedHandle<FHandle>;
using WCHandle = WeightedHandle<CHandle>;

struct VertexKey {
    WVHandle vertex;
    std::array<WEHandle,3> edge;
    std::array<WFHandle,3> face;
    WCHandle cell;
    bool operator==(const VertexKey& rhs) const {
        return vertex == rhs.vertex &&
               edge == rhs.edge &&
               face == rhs.face &&
               cell == rhs.cell;
    }
    void make_unique() {
        if (vertex.weight == 0) vertex = {};
        if (cell.weight == 0) cell = {};
        for (int i = 0; i < 3; ++i) {
            if (edge[i].weight == 0) edge[i] = {};
            if (face[i].weight == 0) face[i] = {};
        }
        sort(edge.begin(), edge.end());
        sort(face.begin(), face.end());
    }
};

}   // namespace dsm

namespace std {
template <class Base>
struct hash<dsm::WeightedHandle<Base>> {
    size_t operator()(const dsm::WeightedHandle<Base>& x) const {
        size_t h = 0;
        kt84::hash_util::hash_combine(h, static_cast<const Base&>(x));
        kt84::hash_util::hash_combine(h, x.weight.numerator());
        kt84::hash_util::hash_combine(h, x.weight.denominator());
        return h;
    }
};
template <>
struct hash<dsm::VertexKey> {
    size_t operator()(const dsm::VertexKey& x) const {
        size_t h = 0;
        kt84::hash_util::hash_combine(h, x.vertex);
        kt84::hash_util::hash_combine(h, x.edge);
        kt84::hash_util::hash_combine(h, x.face);
        kt84::hash_util::hash_combine(h, x.cell);
        return h;
    }
};
}   // namespace std

void dsm::core::computeHexMesh(HexMesh& hexMesh, const HexLayout& layout, const RefinedTetMesh& refTetMesh, const TetMesh& tetMesh, const vector<Sheet>& sheets) {
    hexMesh = {};
    unordered_map<VertexKey, vector<Vector3d>> vertexPosition_buffer;
    unordered_map<VertexKey, VHandle> key_to_handle;
    VertexKey vertex_key;
    for (VHandle layout_v : layout.vertices()) {
        ScopedTimerPrinter timer{"Generating grid for dual cell " + to_string(layout_v + 1) + "/" + to_string(layout.n_vertices())};
        const ReferencePolyhedron& rp = ReferencePolyhedron::get(layout.data(layout_v).signature);
        const unordered_map<VHandle, int>& dualCellCorner_to_referencePolyhedronCorner = layout.data(layout_v).dualCellCorner_to_referencePolyhedronCorner;
        const TetMeshPoint testPoint = layout.data(layout_v).testPoint;
        auto get_kth_reftet_vertex = [&dualCellCorner_to_referencePolyhedronCorner](int k) -> VHandle {
            for (const pair<VHandle,int>& p : dualCellCorner_to_referencePolyhedronCorner) {
                if (p.second == k)
                    return p.first;
            }
            assert(false);
            return VHandle{};
        };
        vertex_key.vertex = layout_v;
        for (int k = 0; k < rp.V.rows(); ++k) {
            // get corresponding layout cell
            const VHandle reftet_v = get_kth_reftet_vertex(k);
            if (refTetMesh.is_boundary(reftet_v)) {
                // k-th corner corresponds to the void space beyond the boundary, so skip
                continue;
            }
            const CHandle layout_c = refTetMesh.data(reftet_v).layout_cell;
            assert(layout_c.is_valid());
            vertex_key.cell = layout_c;
            // determine relevant layout edges/faces and their positions in reference polyhedron
            array<Vector3d,3> midpoint, centroid;
            int e = rp.V2E[k];
            for (int i = 0; i < 3; ++i) {
                // look at the dual edge along e
                const int k2 = rp.to_vertex(e);
                const VHandle reftet_v2 = get_kth_reftet_vertex(k2);
                if (refTetMesh.is_boundary(reftet_v2)) {
                    vertex_key.face[i] = refTetMesh.data(reftet_v2).layout_boundary_face;
                } else {
                    const CHandle layout_c2 = refTetMesh.data(reftet_v2).layout_cell;
                    assert(layout_c2.is_valid());
                    vertex_key.face[i] = layout.common_face(layout_c, layout_c2);
                }
                assert(vertex_key.face[i].is_valid());
                midpoint[i] = 0.5 * (rp.V.row(k) + rp.V.row(k2));
                // look at the dual face surrounded by a cycle of halfedges including e
                const int f = e/5;
                const int deg = rp.F[f].size();
                centroid[i].setZero();
                set<VHandle> sought_reftet_vertices;
                for (int j = 0; j < deg; ++j) {
                    const int k3 = rp.F[f][j];
                    centroid[i] += rp.V.row(k3) / deg;
                    sought_reftet_vertices.insert(get_kth_reftet_vertex(k3));
                }
                // determine which primal edge corresponds to this dual face
                for (HEHandle layout_he : layout.vertex_outgoing_halfedges(layout_v)) {
                    set<VHandle> found_reftet_vertices;
                    for (HFHandle layout_hf : layout.halfedge_halffaces(layout_he)) {
                        const vector<HEHandle>& reftet_halfedges = layout.data(layout_hf).reftet_halfedges;
                        found_reftet_vertices.insert(refTetMesh.from_vertex(reftet_halfedges.front()));
                        found_reftet_vertices.insert(refTetMesh.to_vertex(reftet_halfedges.back()));
                    }
                    if (boost::equal(sought_reftet_vertices, found_reftet_vertices)) {
                        vertex_key.edge[i] = layout.edge_handle(layout_he);
                        break;
                    }
                }
                assert(vertex_key.edge[i].is_valid());
                e = rp.E2E[rp.dedge_prev(e)];
            }
            assert(e == rp.V2E[k]);
            // Conventions for trilinear interpolation:
            //  - t[0] represents blend between left-right
            //  - t[1] represents blend between bottom-top
            //  - t[2] represents blend between front-back
            const Vector3d bottomLeftFront  = rp.V.row(k);
            const Vector3d bottomRightFront = midpoint[0];
            const Vector3d topLeftFront     = midpoint[2];                      // so confusing to get right...
            const Vector3d topRightFront    = centroid[2];
            const Vector3d bottomLeftBack   = midpoint[1];
            const Vector3d bottomRightBack  = centroid[0];
            const Vector3d topLeftBack      = centroid[1];
            const Vector3d topRightBack     = {0,0,0};
            // How to relate mesh items to coordinate axes:
            // The rectangular subregion is defined by 8 entities: vertex, cell, face[0,1,2], and edge[0,1,2].
            // We define a 3D coordinate system where cell represents the origin (0,0,0) and face[0,1,2] represent 3 axes:
            //  - face[0] represents (1,0,0)
            //  - face[2] represents (0,1,0)
            //  - face[1] represents (0,0,1)
            // Furthur, we have edges[0,1,2] such that
            //  - edge[0] represents (1,0,1)
            //  - edge[1] represents (0,1,1)
            //  - edge[2] represents (1,1,0)
            // And finally, vertex represents (1,1,1).
            Vector3i numCells;
            for (int i = 0; i < 3; ++i) {
                const Sheet& sheet = sheets[layout.data(vertex_key.edge[i]).sheetId];
                const bool backSide = sheet.funcLinear(tetMesh, testPoint) < 0;
                numCells[i==0 ? 1 : i==1 ? 0 : 2] = sheet.numSubdiv[backSide] + 1;          // so confusing to get right...
            }
            Grid<VertexKey> grid;
            grid.resize(numCells);
            Vector3i index;
            // fill a grid of vertex keys which we refer later for generating hex cells
            for (index[0]=0; index[0]<=numCells[0]; ++index[0])
            for (index[1]=0; index[1]<=numCells[1]; ++index[1])
            for (index[2]=0; index[2]<=numCells[2]; ++index[2])
            {
                auto t = vector_cast<3,array<boost::rational<int>,3>>(index);
                t[0] /= numCells[0];
                t[1] /= numCells[1];
                t[2] /= numCells[2];
                vertex_key.cell.weight = (1-t[0])*(1-t[1])*(1-t[2]);                    // so confusing to get right...
                vertex_key.vertex.weight = t[0]*t[1]*t[2];
                vertex_key.face[0].weight = t[0]*(1-t[1])*(1-t[2]);
                vertex_key.face[1].weight = (1-t[0])*(1-t[1])*t[2];
                vertex_key.face[2].weight = (1-t[0])*t[1]*(1-t[2]);
                vertex_key.edge[0].weight = t[0]*(1-t[1])*t[2];
                vertex_key.edge[1].weight = (1-t[0])*t[1]*t[2];
                vertex_key.edge[2].weight = t[0]*t[1]*(1-t[2]);
                grid(index) = vertex_key;
                grid(index).make_unique();
                // determine UVW corresponding to t
                const Vector3d t2 = {
                    boost::rational_cast<double>(t[0]),
                    boost::rational_cast<double>(t[1]),
                    boost::rational_cast<double>(t[2])
                };
                const Vector3d uvw = eigen_util::interpolate_trilinear(bottomLeftFront, bottomRightFront, topLeftFront, topRightFront, bottomLeftBack, bottomRightBack, topLeftBack, topRightBack, t2);
                // and obtain the final XYZ by searching for a tet in the dual cell that contains this UVW
                Vector3d xyz = Vector3d::Zero();
                MaxMinSelector<TetMeshPoint> found;
                for (CHandle reftet_c : layout.data(layout_v).reftet_cells) {
                    array<Vector3d,4> uvw_tet;
                    int i = 0;
                    for (VHandle reftet_v : refTetMesh.cell_vertices(reftet_c)) {
                        uvw_tet[i] = layout.data(layout_v).param3d.find(reftet_v)->second;
                        ++i;
                    }
                    Vector4d bc;
                    if (eigen_util::project_to_tetrahedron(uvw_tet[0], uvw_tet[1], uvw_tet[2], uvw_tet[3], uvw, bc)) {
                        if (bc.minCoeff() >= 0) {
                            found.update(0, {reftet_c, bc});
                            break;
                        }
                        bc = eigen_util::clamp_barycentric_coordinate(bc);
                        Vector3d uvw_projected = Vector3d::Zero();
                        for (i = 0; i < 4; ++i)
                            uvw_projected += bc[i] * uvw_tet[i];
                        found.update((uvw - uvw_projected).norm(), {reftet_c, bc});
                    }
                }
                xyz = found.min_value.getPosition(refTetMesh);
                if (found.min_score > 0.001) {
                    cout << "3D param point " << uvw.transpose() << " was not located within any tets! closest = " << found.min_score << ", index = " << index.transpose() << endl;
                }
                // add vertex if new
                if (key_to_handle.find(grid(index)) == key_to_handle.end()) {
                    key_to_handle[grid(index)] = hexMesh.add_vertex(vector_cast<3,Vec3d>(xyz));     // there can be other candidates, but initialize with this value for later use in flipping check
                }
                vertexPosition_buffer[grid(index)].push_back(xyz);
            }
            // actually add individual hex cells
            for (index[0]=0; index[0]<numCells[0]; ++index[0])
            for (index[1]=0; index[1]<numCells[1]; ++index[1])
            for (index[2]=0; index[2]<numCells[2]; ++index[2])
            {
                array<VHandle,8> cellVertices;
                for (int m=0; m<8; ++m) {
                    const Vector3i offset = {
                        (m&1) ? 1 : 0,
                        (m&2) ? 1 : 0,
                        (m&4) ? 1 : 0
                    };
                    cellVertices[m] = key_to_handle.find(grid(index+offset))->second;
                }
                // reorder cellVertices conforming to OpenVolumeMesh API: <OpenVolumeMesh/Mesh/HexahedralMeshTopologyKernel.hh>
                vector<VHandle> cellVertices_reordered;
                if (layout.data(layout_v).flipped) {
                    cellVertices_reordered = {
                        cellVertices[1], cellVertices[0], cellVertices[4], cellVertices[5],
                        cellVertices[3], cellVertices[7], cellVertices[6], cellVertices[2]
                    };
                } else {
                    cellVertices_reordered = {
                        cellVertices[0], cellVertices[1], cellVertices[5], cellVertices[4],
                        cellVertices[2], cellVertices[6], cellVertices[7], cellVertices[3]
                    };
                }
                const CHandle hexmesh_c = hexMesh.add_cell(cellVertices_reordered);
                hexMesh.data(hexmesh_c).layout_cell = layout_c;
            }
        }
    }
    // finally, average over possibly inconsistent candidates of vertex positions
    assert(hexMesh.n_vertices() == vertexPosition_buffer.size());
    for (const pair<VertexKey, vector<Vector3d>>& i : vertexPosition_buffer) {
        Vector3d p;
        if (i.second.size() == 1)
            p = i.second.front();
        else
            p = boost::accumulate(i.second, Vector3d{0,0,0}) / i.second.size();
        hexMesh.set_vertex(key_to_handle.find(i.first)->second, vector_cast<3,Vec3d>(p));
    }
    cout << "Hex Mesh statistics:\n" 
        << "vertices: " << hexMesh.n_vertices() << '\n'
        << "cells   : " << hexMesh.n_cells() << '\n'
        << "faces   : " << hexMesh.n_faces() << '\n'
        << "edges   : " << hexMesh.n_edges() << '\n';
    hexMesh.utility_storeIncidence();
}
