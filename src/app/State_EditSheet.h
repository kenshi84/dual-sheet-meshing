#pragma once
#include "../app.h"

namespace dsm { namespace app {

struct State_EditSheet : public State {
    void init();

    const char* name() const override { return "EditSheet"; }
    void enter() override;
    void render_ssao() override;
    void render() override;
    void mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) override;
    void mouseUp  (int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) override;
    void mouseMove(int mouse_x, int mouse_y) override;
    void keyboard(int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) override;
    
    struct Selected {
        Sheet* sheet = nullptr;
        kt84::PointNormal* freeformPoint = nullptr;
        std::pair<TriMeshPoint, kt84::PointNormal>* freeformSurfacePoint = nullptr;
        kt84::PointNormal2d* sketch2dPoint = nullptr;
        double* cylinderRadius = nullptr;
        void init(Sheet* p = nullptr) { *this = { p, nullptr, nullptr, nullptr, nullptr }; }
        int sheet_id() const { return sheet ? sheet->id : -1; }
        Sheet::Type sheet_type() const { return sheet ? sheet->type : Sheet::Type::UNDEFINED; }
    } m_selected;

    enum struct StrokeMode {
        NONE = 0,
        Cylinder,
        Sketch2D
    } m_strokeMode = StrokeMode::NONE;
    kt84::Polyline3d m_stroke;
    boost::optional<Eigen::Vector3d> m_canvasDragStart;
    bool m_canvasRotating = false;

    struct MementoData {
        std::vector<Sheet> sheets;
        int selected_sheet_id;
    };
    kt84::Memento<MementoData> m_memento;
};

extern State_EditSheet g_state_EditSheet;

} }
