#pragma once
#include "../app.h"

namespace dsm { namespace app {

struct State_EditHexMesh : public State {
    void init();
    const char* name() const override { return "EditHexMesh"; }
    void enter() override;
    void render_ssao() override;
    void render() override;
    void mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) override;
    void mouseUp  (int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) override;
    void mouseMove(int mouse_x, int mouse_y) override;
    void keyboard  (int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) override;

    struct MementoData {
        std::vector<Eigen::Vector3d> vertices;
        void set(const HexMesh& hexMesh) {
            vertices.resize(hexMesh.n_vertices());
            for (VHandle v : hexMesh.vertices())
                vertices[v] = kt84::vector_cast<3,Eigen::Vector3d>(hexMesh.vertex(v));
        }
        void get(HexMesh& hexMesh) const {
            for (VHandle v : hexMesh.vertices())
                hexMesh.set_vertex(v, kt84::vector_cast<3,Vec3d>(vertices[v]));
        }
    };
    kt84::Memento<MementoData> m_memento;

    kt84::DisplayList m_dispList;
};

extern State_EditHexMesh g_state_EditHexMesh;

} }
