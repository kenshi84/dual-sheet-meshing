#include <cstdlib>
#include "../app.h"
#include "../misc.h"
using namespace std;
using namespace Eigen;
using namespace kt84;
using namespace kt84::graphics_util;

void autoSave() {
    string window_title = APPNAME;
    if (!dsm::app::g_filename_dat.empty()) {
        window_title += " - " + dsm::app::g_filename_dat + (dsm::app::g_config.autoSave.unsaved ? "*" : "");

        if (dsm::app::g_config.autoSave.enabled) {
            const time_t now = time(nullptr);
            if (dsm::app::g_config.autoSave.unsaved) {
                if (now - dsm::app::g_config.autoSave.last_time > dsm::app::g_config.autoSave.interval) {
                    dsm::app::g_config.autoSave.last_time = now;
                    dsm::app::saveBinary();
                }
            } else {
                dsm::app::g_config.autoSave.last_time = now;
            }
        }
    }
    glfwSetWindowTitle(dsm::app::g_loopControl.window, window_title.c_str());
}

void display() {
    glPushAttrib(GL_ALL_ATTRIB_BITS);

    if (!dsm::app::g_triMesh.is_empty()) {
        // set projection matrix
        double zNear = dsm::app::g_camera.center_to_eye().norm() * 0.1;
        double zFar  = zNear * 10 + dsm::app::g_triMesh.stats.mAABB.diagonal().norm() * 10;
        double aspect_ratio = dsm::app::g_camera.width / static_cast<double>(dsm::app::g_camera.height);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        if (dsm::app::g_renderOption.get_bool("use_ortho")) {
            double ortho_y = zNear * 2.5;
            double ortho_x  = ortho_y * aspect_ratio;
            glOrtho(-ortho_x, ortho_x, -ortho_y, ortho_y, zNear, zFar);
        } else {
            gluPerspective(40, aspect_ratio, zNear, zFar);
        }
    }

    // set modelview matrix
    auto eye    = dsm::app::g_camera.get_eye();
    auto center = dsm::app::g_camera.center;
    auto up     = dsm::app::g_camera.get_up();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eye, center, up);

    dsm::app::g_ssao.render_begin();
    dsm::app::g_state->render_ssao();
    dsm::app::g_ssao.render_end();

    dsm::app::g_state->render();

    if (!dsm::app::g_triMesh.is_empty() && dsm::app::g_renderOption.get_bool("show_aabb")) {
        AABB aabb = dsm::app::g_triMesh.stats.mAABB;
        aabb.min -= Vector3f::Constant(0.1f);
        aabb.max += Vector3f::Constant(0.1f);
        glPushMatrix();
        glTranslated(aabb.center());
        glScaled(aabb.diagonal() * 0.5);
        glLineWidth(1);
        glColor3d(0.8, 0.8, 0.8);
        glutWireCube(2);
        glLineWidth(5);
        glBegin(GL_LINES);
        glColor3d(0.2, 0.2, 0.2);   glVertex3d(-1, -1, -1);    glColor3d(1, 0, 0);    glVertex3d(1, -1, -1);
        glColor3d(0.2, 0.2, 0.2);   glVertex3d(-1, -1, -1);    glColor3d(0, 1, 0);    glVertex3d(-1, 1, -1);
        glColor3d(0.2, 0.2, 0.2);   glVertex3d(-1, -1, -1);    glColor3d(0, 0, 1);    glVertex3d(-1, -1, 1);
        glEnd();
        glPopMatrix();
    }

    glPopAttrib();
    TwDraw();
    glCheckError();
}

int getDevicePixelRatio(GLFWwindow* window) {
  int wWidth, wHeight;
  glfwGetWindowSize(window, &wWidth, &wHeight);
  int fbWidth, fbHeight;
  glfwGetFramebufferSize(window, &fbWidth, &fbHeight);
  return fbWidth/wWidth;
}
void mapCursorPos(GLFWwindow* window, int &x, int &y) {
    int wWidth, wHeight;
    glfwGetWindowSize(window, &wWidth, &wHeight);
    int fbWidth, fbHeight;
    glfwGetFramebufferSize(window, &fbWidth, &fbHeight);
    x = x * fbWidth  / wWidth;
    y = y * fbHeight / wHeight;
}

namespace glfw_callback {
    void framebuffersize(GLFWwindow* window, int width, int height) {
        dsm::app::g_camera.reshape(width, height);
        glViewport(0, 0, width, height);
        TwWindowSize(width, height);
        dsm::app::g_ssao.allocate(width, height);
    }
    void key(GLFWwindow* window, int key, int scancode, int action, int mods) {
        if (TwEventKeyGLFW3(window, key, scancode, action, mods))
            return;
        auto actionFlag = glfw_util::parseAction(action);
        auto modFlag = glfw_util::parseMods(mods);
        if (actionFlag.press || actionFlag.repeat) {
            if (!dsm::app::common_keyboard(key, modFlag.shift, modFlag.ctrl, modFlag.alt))
                dsm::app::g_state->keyboard(key, modFlag.shift, modFlag.ctrl, modFlag.alt);
        }
        else if (actionFlag.release) {
            if (!dsm::app::common_keyboardUp(key, modFlag.shift, modFlag.ctrl, modFlag.alt))
                dsm::app::g_state->keyboardUp(key, modFlag.shift, modFlag.ctrl, modFlag.alt);
        }
    }
    void mousebutton(GLFWwindow* window, int button_glfw, int action, int mods) {
        if (TwEventMouseButtonGLFW3(window, button_glfw, action, mods))
            return;

        dsm::app::Button button =
            button_glfw == GLFW_MOUSE_BUTTON_LEFT ? dsm::app::LEFT :
            button_glfw == GLFW_MOUSE_BUTTON_RIGHT ? dsm::app::RIGHT :
            button_glfw == GLFW_MOUSE_BUTTON_MIDDLE ? dsm::app::MIDDLE : dsm::app::UNDEFINED;

        auto p = glfw_util::getCursorPos(window);
        mapCursorPos(window, p.x, p.y);
        p.y = dsm::app::g_camera.height - p.y;

        auto modFlag = glfw_util::parseMods(mods);

        auto actionFlag = glfw_util::parseAction(action);
        if (actionFlag.press) {
            if (!dsm::app::common_mouseDown(p.x, p.y, button, modFlag.shift, modFlag.ctrl, modFlag.alt))
                dsm::app::g_state->mouseDown(p.x, p.y, button, modFlag.shift, modFlag.ctrl, modFlag.alt);
        }
        else {
            if (!dsm::app::common_mouseUp(p.x, p.y, button, modFlag.shift, modFlag.ctrl, modFlag.alt))
                dsm::app::g_state->mouseUp(p.x, p.y, button, modFlag.shift, modFlag.ctrl, modFlag.alt);
        }
    }
    void cursorpos(GLFWwindow* window, double dx, double dy) {
        int x = static_cast<int>(dx);
        int y = static_cast<int>(dy);
        mapCursorPos(window, x, y);
        if (TwEventCursorPosGLFW3(window, x, y))
            return;
        y = dsm::app::g_camera.height - y;

        if (!dsm::app::common_mouseMove(x, y))
            dsm::app::g_state->mouseMove(x, y);
    }
    void drop(GLFWwindow* window, int count, const char** names) {
        if (count != 1)
            return;
        if (dsm::app::g_config.autoSave.unsaved && !tinyfd_util::messageBox(APPNAME, "Current data is not saved yet. Continue?", "yesno", "question", 0))
            return;
        string fname = names[0];
        auto fname_ext = fname.substr(fname.size() - 4, 4);
        boost::algorithm::to_lower(fname_ext);
        if (fname_ext == ".obj" || fname_ext == ".off")
            dsm::app::importTriMesh(fname);
        else if (fname_ext == ".dat")
            dsm::app::loadBinary(fname);
        glfwPostEmptyEvent();
    }
}


int main(int argc, char* argv[]) {
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfw_util::InitConfig config;
    config.window.title = "Dual Sheet Meshing";
    config.callback.framebuffersize = glfw_callback::framebuffersize;
    config.callback.key             = glfw_callback::key            ;
    config.callback.character       = [](GLFWwindow* window, unsigned int codepoint) { TwEventCharModsGLFW3(window, codepoint, 0); };
    config.callback.mousebutton     = glfw_callback::mousebutton    ;
    config.callback.cursorpos       = glfw_callback::cursorpos      ;
    config.callback.drop            = glfw_callback::drop           ;
    dsm::app::g_loopControl = glfw_util::init(config);

    // GLEW
    glewInit();
    glGetError();
    dsm::app::g_glew_initialized = true;

    // AntTweakBar
    // set fontscaling for anttweakbar to conform with high dpi displays (IMPORTANT: TwDefine must be called before TwInit!)
    tw_util::set_bar_fontscaling(getDevicePixelRatio(dsm::app::g_loopControl.window));
    // since the fontscaling is set globally only once, moving the window to another display with different pixel ratio
    // could break the bar functionality - TO CHECK - possible overcome: re-init the bar after a display change
    TwInit(TW_OPENGL, NULL);
    std::atexit([](){TwTerminate();});

    tbb::task_scheduler_init init(tbb::task_scheduler_init::automatic);

    dsm::app::init();
    if (argc == 2) {
        string fname = argv[1];
        auto fname_ext = fname.substr(fname.size() - 4, 4);
        boost::algorithm::to_lower(fname_ext);
        if (fname_ext == ".obj" || fname_ext == ".off")
            dsm::app::importTriMesh(fname);
        else if (fname_ext == ".dat")
            dsm::app::loadBinary(fname);
    }

    dsm::app::g_loopControl.idle_func[0] = display;
    dsm::app::g_loopControl.idle_func[1] = autoSave;
    glfw_util::start_loop(dsm::app::g_loopControl);
    return 0;
}
