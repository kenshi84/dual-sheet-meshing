#include "drawhelper.h"
using namespace std;
using namespace Eigen;
using namespace kt84;
using namespace kt84::graphics_util;

void dsm::app::drawhelper::drawSphere(const Vector3d& point, double radius) {
    static kt84::DisplayList dl;
    glPushMatrix();
    glTranslated(point);
    glScaled(Vector3d::Constant(radius));
    dl.render([&] () {
        glutSolidSphere(1, 24, 6);
    });
    glPopMatrix();
}

void dsm::app::drawhelper::drawArrow(const Vector3d& point, const Vector3d& dir, double bar_radius, double bar_height, double tip_radius, double tip_height) {
    static kt84::DisplayList dl_cylinder;
    static kt84::DisplayList dl_cone;
    if (dir.isZero())
        return;
    glPushMatrix();
    glTranslated(point);
    // align dir to z-axis
    const Vector3d n = dir.normalized();
    if (n == Vector3d::UnitZ()) {
    } else if (n == -Vector3d::UnitZ()) {
        glRotated(180, 1, 0, 0);
    } else {
        const double angle = std::acos(n.z()) * 180 / util::pi();
        glRotated(-angle, n.cross(Vector3d::UnitZ()));
    }
    // draw bar
    glPushMatrix();
    glScaled(bar_radius, bar_radius, bar_height);
    dl_cylinder.render([&] () {
        glutSolidCylinder(1, 1, 24, 1);
    });
    glPopMatrix();
    // draw tip
    glTranslated(0, 0, bar_height);
    glScaled(tip_radius, tip_radius, tip_height);
    dl_cone.render([&] () {
        glutSolidCone(1, 1, 24, 1);
    });
    glPopMatrix();
}

void dsm::app::drawhelper::drawCylinder(const Eigen::Vector3d& point_from, const Eigen::Vector3d& point_to, double radius) {
    static kt84::DisplayList dl;
    if (point_from == point_to)
        return;
    glPushMatrix();
    glTranslated(point_from);
    // align dir to z-axis
    const Vector3d d = point_to - point_from;
    const double height = d.norm();
    const Vector3d n = d / height;
    if (n == Vector3d::UnitZ()) {
    } else if (n == -Vector3d::UnitZ()) {
        glRotated(180, 1, 0, 0);
    } else {
        const double angle = std::acos(n.z()) * 180 / util::pi();
        glRotated(-angle, n.cross(Vector3d::UnitZ()));
    }
    glScaled(radius, radius, height);
    dl.render([&] () {
        glutSolidCylinder(1, 1, 24, 1);
    });
    glPopMatrix();
}
