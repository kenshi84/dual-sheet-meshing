#include "State_EditHexLayout.h"
#include "State_EditHexMesh.h"
#include "../misc.h"
using namespace std;
using namespace Eigen;
using namespace kt84;
using namespace kt84::graphics_util;

dsm::app::State_EditHexMesh dsm::app::g_state_EditHexMesh;

void dsm::app::State_EditHexMesh::init() {
    m_bar = TwNewBar(name());
    tw_util::set_bar_size(m_bar, g_camera.width/6, g_camera.height*9/10);
    tw_util::set_bar_position(m_bar, 20, 20);
    tw_util::set_bar_valueswidth(m_bar, g_camera.width/12);
    tw_util::set_bar_text_light(m_bar);
    tw_util::set_bar_color(m_bar, 50, 100, 150);
    tw_util::set_bar_visible(m_bar, false);

    g_renderOption.addToBar(m_bar);
    TwAddSeparator(m_bar, nullptr, nullptr);

    tw_util::AddButton(m_bar, "load_binary",
        [&](){
            if (g_config.autoSave.unsaved && !tinyfd_util::messageBox(APPNAME " - Load", "Current change is not saved yet. \nAre you sure to discard it anyway?", "yesno", "warning"))
                return;
            auto fname = tinyfd_util::openFileDialog(APPNAME " - Load", "", {"*.dat"}, "Binary files", false);
            if (!fname)
                return;
            if (!loadBinary(*fname)) {
                tinyfd_util::messageBox(APPNAME " - Load", "Error occurred!", "ok", "error", 1);
                return;
            }
            // TODO: display update
            g_config.autoSave.unsaved = false;
        }, "label='Load' key=CTRL+o");

    tw_util::AddButton(m_bar, "save_binary",
        [&](){
            auto fname = tinyfd_util::saveFileDialog(APPNAME " - Save", "", {"*.dat"}, "Binary files");
            if (!fname)
                return;
            if (!saveBinary(*fname)) {
                tinyfd_util::messageBox(APPNAME " - Save", "Error occurred!", "ok", "error", 1);
                return;
            }
            g_config.autoSave.unsaved = false;
        }, "label='Save'");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    tw_util::AddButton(m_bar, "undo",
        [&](){
            MementoData mementoData;
            mementoData.set(g_hexMesh);
            if (m_memento.undo(mementoData)) {
                g_config.autoSave.unsaved = true;
                mementoData.get(g_hexMesh);
                m_dispList.invalidate();
            }
        }, "label=Undo key=CTRL+z");
    tw_util::AddButton(m_bar, "redo",
        [&](){
            MementoData mementoData;
            mementoData.set(g_hexMesh);
            if (m_memento.redo(mementoData)) {
                g_config.autoSave.unsaved = true;
                mementoData.get(g_hexMesh);
                m_dispList.invalidate();
            }
        }, "label=Undo key=CTRL+y");
    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    tw_util::AddButton(m_bar, "export_hexmesh",
        [&](){
            auto fname = tinyfd_util::saveFileDialog(APPNAME " - Export", g_filename_dat.substr(0, g_filename_dat.size() - 4) + ".vtk", {"*.vtk"}, "VTK files");
            if (!fname)
                return;
            misc::exportVTK(g_hexMesh, *fname);
        }, "label='Export'");
    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    tw_util::AddButton(m_bar, "back",
        [&](){
            changeState(g_state_EditHexLayout);
            saveBinary();
        }, "label='Back'");
}

void dsm::app::State_EditHexMesh::enter() {
    m_memento.init(g_config.undo_buffer_size);
}

void dsm::app::State_EditHexMesh::render_ssao() {
    // hex cells
    for (auto c : g_hexMesh.cells()) {
        auto& cdata = g_hexMesh.data(c);
        const Vec3d n = vector_cast<3,Vec3d>(g_renderOption.get_dir3f("hexMesh.planeNormal"));
        if ((g_hexMesh.barycenter(c) | n) > g_renderOption.get_float("hexMesh.planeDist"))
            continue;
        g_ssao.shader_pass1.set_uniform_3("u_color", g_hexLayout.data(cdata.layout_cell).color);
        glPushMatrix();
        const Vector3d cog = vector_cast<3, Vector3d>(g_hexMesh.barycenter(c));
        glTranslated(cog);
        glScaled(Vector3d::Constant(g_renderOption.get_float("hexMesh.shrink")));
        cdata.dispList[0].render([&]() {
            glBegin(GL_QUADS);
            for (auto f : g_hexMesh.cell_faces(c)) {
                array<Vector3d, 4> position;
                int i = 0;
                for (auto v : g_hexMesh.face_vertices(f))
                    position[i++] = vector_cast<3, Vector3d>(g_hexMesh.vertex(v));
                Vector3d normal = Vector3d::Zero();
                for (int i = 0; i < 4; ++i)
                    normal += (position[(i + 1) % 4] - position[i]).cross((position[i] - position[(i + 3) % 4]));
                glNormal3d(normal.normalized());
                for (int i = 0; i < 4; ++i)
                    glVertex3d(position[i] - cog);
            }
            glEnd();
        });
        glPopMatrix();
    }
}

void dsm::app::State_EditHexMesh::render() {
    // draw silhouettes
    if (g_renderOption.get_bool("show_silhouette")) {
        g_shader.silhouette.enable();
        g_shader.silhouette.set_uniform_3<Vector3f>("u_eye", g_camera.get_eye().cast<float>());
        g_shader.silhouette.set_uniform_3<Vector3f>("u_color", Vector3f(0,0,0));
        g_dispList.triMesh_silhouette.render([&] () {
            glLineWidth(1);
            glBegin(GL_LINES);
            glColor3f(1, 0, 1);
            for (uint32_t f0 = 0; f0 < g_triMesh.F.cols(); ++f0) {
                for (uint32_t i = 0; i < 3; ++i) {
                    uint32_t v0 = g_triMesh.F(i, f0);
                    uint32_t v1 = g_triMesh.F((i+1)%3, f0);
                    if (v0 < v1) {
                        uint32_t f1 = g_triMesh.E2E[3 * f0 + i] / 3;
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_vertex_0", g_triMesh.V.col(v0));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_vertex_1", g_triMesh.V.col(v1));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_normal_0", g_triMesh.Nf.col(f0));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_normal_1", g_triMesh.Nf.col(f1));
                        glVertex3f(Vector3f(g_triMesh.V.col(v0)));
                        glVertex3f(Vector3f(g_triMesh.V.col(v1)));
                    }
                }
            }
            glEnd();
        });
        g_shader.silhouette.disable();
    }
    // hex cells
    for (auto c : g_hexMesh.cells()) {
        auto& cdata = g_hexMesh.data(c);
        const Vec3d n = vector_cast<3,Vec3d>(g_renderOption.get_dir3f("hexMesh.planeNormal"));
        if ((g_hexMesh.barycenter(c) | n) > g_renderOption.get_float("hexMesh.planeDist"))
            continue;
        glPushMatrix();
        const Vector3d cog = vector_cast<3, Vector3d>(g_hexMesh.barycenter(c));
        glTranslated(cog);
        glScaled(Vector3d::Constant(g_renderOption.get_float("hexMesh.shrink")));
        cdata.dispList[1].render([&]() {
            glLineWidth(1);
            glBegin(GL_LINES);
            glColor3f(0, 0, 0);
            for (auto e : g_hexMesh.cell_edges(c)) {
                for (auto v : g_hexMesh.edge_vertices(e))
                    glVertex3d(vector_cast<3, Vector3d>(g_hexMesh.vertex(v)) - cog);
            }
            glEnd();
        });
        glPopMatrix();
    }
}

void dsm::app::State_EditHexMesh::mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {

}

void dsm::app::State_EditHexMesh::mouseUp  (int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {

}

void dsm::app::State_EditHexMesh::mouseMove(int mouse_x, int mouse_y) {

}

void dsm::app::State_EditHexMesh::keyboard  (int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {

}

