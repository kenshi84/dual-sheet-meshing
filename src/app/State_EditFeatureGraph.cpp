#include "State_EditFeatureGraph.h"
#include "State_EditSheet.h"
#include "drawhelper.h"
using namespace std;
using namespace Eigen;
using namespace kt84;
using namespace kt84::graphics_util;

dsm::app::State_EditFeatureGraph dsm::app::g_state_EditFeatureGraph;

void dsm::app::State_EditFeatureGraph::init() {
    m_bar = TwNewBar(name());
    tw_util::set_bar_size(m_bar, g_camera.width/6, g_camera.height*9/10);
    tw_util::set_bar_position(m_bar, 20, 20);
    tw_util::set_bar_valueswidth(m_bar, g_camera.width/12);
    tw_util::set_bar_text_light(m_bar);
    tw_util::set_bar_color(m_bar, 50, 100, 150);
    tw_util::set_bar_visible(m_bar, false);

    g_renderOption.addToBar(m_bar);
    TwAddSeparator(m_bar, nullptr, nullptr);

    tw_util::AddButton(m_bar, "load_binary",
        [&](){
            if (g_config.autoSave.unsaved && !tinyfd_util::messageBox(APPNAME " - Load", "Current change is not saved yet. \nAre you sure to discard it anyway?", "yesno", "warning"))
                return;
            auto fname = tinyfd_util::openFileDialog(APPNAME " - Load", "", {"*.dat"}, "Binary files", false);
            if (!fname)
                return;
            if (!loadBinary(*fname)) {
                tinyfd_util::messageBox(APPNAME " - Load", "Error occurred!", "ok", "error", 1);
                return;
            }
            // TODO: display update
            g_config.autoSave.unsaved = false;
        }, "label='Load'");

    tw_util::AddButton(m_bar, "save_binary",
        [&](){
            auto fname = tinyfd_util::saveFileDialog(APPNAME " - Save", "", {"*.dat"}, "Binary files");
            if (!fname)
                return;
            if (!saveBinary(*fname)) {
                tinyfd_util::messageBox(APPNAME " - Save", "Error occurred!", "ok", "error", 1);
                return;
            }
            g_config.autoSave.unsaved = false;
        }, "label='Save'");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    tw_util::AddButton(m_bar, "undo",
        [&](){
            MementoData mementoData{ g_triMesh.vertex_constraint, g_triMesh.edge_constraint };
            if (m_memento.undo(mementoData)) {
                g_config.autoSave.unsaved = true;
                g_triMesh.vertex_constraint = mementoData.vertex_constraint;
                g_triMesh.edge_constraint = mementoData.edge_constraint;
                // TODO: update display list
            }
        }, "label=Undo key=CTRL+z");
    tw_util::AddButton(m_bar, "redo",
        [&](){
            MementoData mementoData{ g_triMesh.vertex_constraint, g_triMesh.edge_constraint };
            if (m_memento.redo(mementoData)) {
                g_config.autoSave.unsaved = true;
                g_triMesh.vertex_constraint = mementoData.vertex_constraint;
                g_triMesh.edge_constraint = mementoData.edge_constraint;
                // TODO: update display list
            }
        }, "label=Undo key=CTRL+y");
    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    static float angle_threshold = 40;
    TwAddVarRW(m_bar, "angle_threshold", TW_TYPE_FLOAT, &angle_threshold, "min=0 max=180 step=0.1");
    tw_util::AddButton(m_bar, "init_threshold",
        [&]{
            m_memento.store({ g_triMesh.vertex_constraint, g_triMesh.edge_constraint });
            g_config.autoSave.unsaved = true;
            g_triMesh.init_featuregraph_dihedral_angle(angle_threshold);
        });
    tw_util::AddButton(m_bar, "clear",
        [&]{
            m_memento.store({ g_triMesh.vertex_constraint, g_triMesh.edge_constraint });
            g_config.autoSave.unsaved = true;
            g_triMesh.vertex_constraint = vector<int>(g_triMesh.V.cols(),0);
            g_triMesh.edge_constraint = vector<int>(g_triMesh.edges.size(),0);
        });
    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    tw_util::AddButton(m_bar, "done",
        [&](){
            changeState(g_state_EditSheet);
        }, "label='Done'");
}

void dsm::app::State_EditFeatureGraph::enter() {
    m_memento.init(g_config.undo_buffer_size);
    g_serializedBlob.triMesh.clear();
    g_renderOption.get_bool("show_surface_edges") = true;
}

void dsm::app::State_EditFeatureGraph::render_ssao() {
    // draw model surface
    if (g_renderOption.get_bool("show_surface")) {
        glEnable(GL_CULL_FACE);
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f("model"));
        g_dispList.triMesh_surface.render([&] () {
            glBegin(GL_TRIANGLES);
            for (uint32_t f = 0; f < g_triMesh.F.cols(); ++f) {
                for (uint32_t i = 0; i < 3; ++i) {
                    uint32_t v = g_triMesh.F(i, f);
                    if (g_triMesh.creases.count(v))
                        glNormal3f(Vector3f(g_triMesh.Nf.col(f)));
                    else
                        glNormal3f(Vector3f(g_triMesh.N.col(v)));
                    glVertex3f(Vector3f(g_triMesh.V.col(v)));
                }
            }
            glEnd();
        });
    }
    // draw feature graph
    if (g_renderOption.get_bool("show.featuregraph")) {
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f("featuregraph.node"));
        for (int v = 0; v < g_triMesh.V.cols(); ++v) {
            if (g_triMesh.vertex_constraint[v])
                drawhelper::drawSphere(g_triMesh.V.col(v).cast<double>(), g_renderOption.get_float("featuregraph.node.r"));
        }
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f("featuregraph.arc"));
        for (int e = 0; e < g_triMesh.edges.size(); ++e) {
            if (g_triMesh.edge_constraint[e]) {
                const array<Vector3d,2> p = {
                    g_triMesh.V.col(g_triMesh.edges[e][0]).cast<double>(),
                    g_triMesh.V.col(g_triMesh.edges[e][1]).cast<double>()
                };
                drawhelper::drawCylinder(p[0], p[1], g_renderOption.get_float("featuregraph.arc.r"));
            }
        }
    }
}

void dsm::app::State_EditFeatureGraph::render() {
    // draw silhouettes
    if (g_renderOption.get_bool("show_silhouette")) {
        g_shader.silhouette.enable();
        g_shader.silhouette.set_uniform_3<Vector3f>("u_eye", g_camera.get_eye().cast<float>());
        g_shader.silhouette.set_uniform_3<Vector3f>("u_color", Vector3f(0,0,0));
        g_dispList.triMesh_silhouette.render([&] () {
            glLineWidth(1);
            glBegin(GL_LINES);
            for (uint32_t f0 = 0; f0 < g_triMesh.F.cols(); ++f0) {
                for (uint32_t i = 0; i < 3; ++i) {
                    uint32_t v0 = g_triMesh.F(i, f0);
                    uint32_t v1 = g_triMesh.F((i+1)%3, f0);
                    if (v0 < v1) {
                        uint32_t f1 = g_triMesh.E2E[3 * f0 + i] / 3;
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_vertex_0", g_triMesh.V.col(v0));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_vertex_1", g_triMesh.V.col(v1));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_normal_0", g_triMesh.Nf.col(f0));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_normal_1", g_triMesh.Nf.col(f1));
                        glVertex3f(Vector3f(g_triMesh.V.col(v0)));
                        glVertex3f(Vector3f(g_triMesh.V.col(v1)));
                    }
                }
            }
            glEnd();
        });
        g_shader.silhouette.disable();
    }
    // draw creases
    g_dispList.triMesh_crease.render([&] () {
        glLineWidth(1);
        glBegin(GL_LINES);
        glColor3f(0, 0, 0);
        for (uint32_t f = 0; f < g_triMesh.F.cols(); ++f) {
            for (uint32_t i = 0; i < 3; ++i) {
                const uint32_t e = 3 * f  + i;
                if (g_triMesh.crease_edges.count(e)) {
                    glVertex3f(Vector3f(g_triMesh.V.col(g_triMesh.F(i, f))));
                    glVertex3f(Vector3f(g_triMesh.V.col(g_triMesh.F((i+1)%3, f))));
                }
            }
        }
        glEnd();
    });
    // draw surface edges
    if (g_renderOption.get_bool("show_surface_edges")) {
        // g_dispList.triMesh_surface_edges.render([&](){
            glLineWidth(1);
            glColor3f(g_renderOption.get_color3f("surface_edges"));
            glBegin(GL_LINES);
            for (const array<uint32_t,2>& e : g_triMesh.edges) {
                glVertex3f(Vector3f(g_triMesh.V.col(e[0])));
                glVertex3f(Vector3f(g_triMesh.V.col(e[1])));
            }
            glEnd();
        // });
    }
}

void dsm::app::State_EditFeatureGraph::mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
    if (!g_eventData.mouse_on_triMesh.is_valid()) return;
    auto count_constrained_edges = [](uint32_t v) {
        int cnt = 0;
        const vector<uint32_t> outgoing_halfedges = g_triMesh.outgoing_halfedges(v);
        for (uint32_t he : outgoing_halfedges) {
            if (g_triMesh.edge_constraint[g_triMesh.halfedge_to_edge[he]])
                ++cnt;
        }
        return cnt;
    };
    if (g_eventData.key_pressed['A']) {
        const uint32_t v = g_eventData.mouse_on_triMesh.closestVertex(g_triMesh);
        const int cnt = count_constrained_edges(v);
        if (cnt != 0 && cnt != 2)
            return;
        if (g_triMesh.vertex_constraint[v]) {
            m_memento.store({ g_triMesh.vertex_constraint, g_triMesh.edge_constraint });
            g_config.autoSave.unsaved = true;
            g_triMesh.vertex_constraint[v] = 0;
        } else {
            m_memento.store({ g_triMesh.vertex_constraint, g_triMesh.edge_constraint });
            g_config.autoSave.unsaved = true;
            g_triMesh.vertex_constraint[v] = 1;
        }
        return;
    }
    if (g_eventData.key_pressed['S']) {
        const uint32_t f = g_eventData.mouse_on_triMesh.f;
        const Vector3f& bc = g_eventData.mouse_on_triMesh.bc;
        const uint32_t opposite_v = bc[0]<bc[1] && bc[0]<bc[2] ? g_triMesh.F(0,f) : bc[1]<bc[2] ? g_triMesh.F(1,f) : g_triMesh.F(2,f);
        uint32_t e = ::INVALID;
        uint32_t he = ::INVALID;
        for (int i = 0; i < 3; ++i) {
            he = 3*f + i;
            if (g_triMesh.from_vertex(he) != opposite_v && g_triMesh.to_vertex(he) != opposite_v) {
                e = g_triMesh.halfedge_to_edge[he];
                break;
            }
        }
        assert(e != ::INVALID);
        m_memento.store({ g_triMesh.vertex_constraint, g_triMesh.edge_constraint });
        g_config.autoSave.unsaved = true;
        const bool was_constrained = g_triMesh.edge_constraint[e];
        g_triMesh.edge_constraint[e] = !g_triMesh.edge_constraint[e];
        g_triMesh.make_vertex_constrained_if_necessary();
        // automatically remove vertex constraint as appropriate
        for (int i = 0; i < 2; ++i) {
            const uint32_t v = i == 0 ? g_triMesh.from_vertex(he) : g_triMesh.to_vertex(he);
            const int cnt = count_constrained_edges(v);
            if ((was_constrained && cnt==0) || (!was_constrained && cnt==2)) {
                g_triMesh.vertex_constraint[v] = 0;
            }
        }
        return;
    }
}
