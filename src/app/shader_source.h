#pragma once

namespace dsm {
namespace app {
namespace shader_source {

namespace silhouette {
    extern const char * const FRAG;
    extern const char * const VERT;
}

namespace ssao {
    namespace pass1 {
        extern const char * const VERT;
        extern const char * const FRAG;
    }
    namespace pass2 {
        extern const char * const VERT;
        extern const char * const FRAG;
    }
    namespace pass3 {
        extern const char * const VERT;
        extern const char * const FRAG;
    }
}

}}}
