#include "../app.h"
#include "shader_source.h"
#include <kt84/util.hh>
#include <kt84/graphics/graphics_util.hh>
using namespace std;
using namespace Eigen;
using namespace kt84::graphics_util;

const int dsm::app::SSAO::KERNEL_SIZE_MAX = 256;

// https://learnopengl.com/code_viewer_gh.php?code=src/5.advanced_lighting/9.ssao/ssao.cpp
void dsm::app::SSAO::init(int width, int height) {
    // init and allocate buffers
    texture_position.init();
    texture_position.bind();
    texture_position.set_wrap(GL_CLAMP_TO_EDGE);
    texture_position.set_filter(GL_NEAREST);
    texture_normal.init();
    texture_normal.bind();
    texture_normal.set_wrap(GL_CLAMP_TO_EDGE);
    texture_normal.set_filter(GL_NEAREST);
    texture_color.init();
    texture_color.bind();
    texture_color.set_wrap(GL_CLAMP_TO_EDGE);
    texture_color.set_filter(GL_NEAREST);
    texture_noise.init();
    texture_noise.bind();
    texture_noise.set_wrap(GL_CLAMP_TO_EDGE);
    texture_noise.set_filter(GL_NEAREST);
    texture_occlusion.init();
    texture_occlusion.bind();
    texture_occlusion.set_wrap(GL_CLAMP_TO_EDGE);
    texture_occlusion.set_filter(GL_NEAREST);
    rbo_depth.init();
    allocate(width, height);
    // framebuffer
    fbo_pass1.init();
    fbo_pass1.bind();
    fbo_pass1.attach_texture_color(0, texture_position);
    fbo_pass1.attach_texture_color(1, texture_normal);
    fbo_pass1.attach_texture_color(2, texture_color);
    fbo_pass1.set_draw_buffers_color(3);
    fbo_pass1.attach_renderbuffer_depth(rbo_depth);
    DEBUG_PRINT(fbo_pass1.is_complete());
    fbo_pass2.init();
    fbo_pass2.bind();
    fbo_pass2.attach_texture_color(0, texture_occlusion);
    fbo_pass2.set_draw_buffers_color(1);
    DEBUG_PRINT(fbo_pass2.is_complete());
    kt84::FramebufferObject::unbind();

    // shader
    shader_pass1.init_compile_attach_link(shader_source::ssao::pass1::FRAG, shader_source::ssao::pass1::VERT);
    shader_pass2.init_compile_attach_link(shader_source::ssao::pass2::FRAG, shader_source::ssao::pass2::VERT);
    shader_pass2.enable();
    shader_pass2.set_uniform_1<int>("u_position", 0);
    shader_pass2.set_uniform_1<int>("u_normal", 1);
    shader_pass2.set_uniform_1<int>("u_noise", 2);
    for (int i = 0; i < KERNEL_SIZE_MAX; ++i) {
        float scale = float(i) / KERNEL_SIZE_MAX;
        Vector3f sample = (0.1 + 0.9 * scale * scale) * kt84::util::random_float(0, 1) * Vector3f::Random().normalized();
        if (sample.z() < 0)
            sample.z() *= -1;
        shader_pass2.set_uniform_3((boost::format("u_samples[%d]") % i).str(), sample);
    }
    shader_pass3.init_compile_attach_link(shader_source::ssao::pass3::FRAG, shader_source::ssao::pass3::VERT);
    shader_pass3.enable();
    shader_pass3.set_uniform_1<int>("u_position", 0);
    shader_pass3.set_uniform_1<int>("u_normal", 1);
    shader_pass3.set_uniform_1<int>("u_color", 2);
    shader_pass3.set_uniform_1<int>("u_occlusion", 3);
    kt84::ProgramObject::disable();
}

void dsm::app::SSAO::allocate(int width, int height) {
    if (!g_glew_initialized) return;
    texture_position.bind(); texture_position.allocate(width, height, GL_RGB, GL_RGB16F);
    texture_normal.bind(); texture_normal.allocate(width, height, GL_RGB, GL_RGB16F);
    texture_color.bind(); texture_color.allocate(width, height, GL_RGB, GL_RGB);
    {
        texture_noise.bind();
        texture_noise.allocate(width, height, GL_RGB, GL_RGB16F);
        vector<Vector3f> noise_buf(width * height);
        for (auto& x : noise_buf) {
            float theta = kt84::util::random_float(0, 2) * kt84::util::pi();
            x << cos(theta), sin(theta), 0;
        }
        texture_noise.copy_cpu2gpu(GL_FLOAT, &noise_buf[0][0]);
    }
    texture_occlusion.bind(); texture_occlusion.allocate(width, height, GL_RGB, GL_RED);
    rbo_depth.bind(); rbo_depth.allocate(width, height);
    kt84::TextureObject::unbind();
    kt84::RenderbufferObject::unbind();
}

void dsm::app::SSAO::render_begin() {
    // pass 1: draw into geometry buffer
    fbo_pass1.bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    shader_pass1.enable();
}

void dsm::app::SSAO::render_end() {
    // pass 2: compute occlusion factor from geometry buffer
    fbo_pass2.bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    shader_pass2.enable();
    shader_pass2.set_uniform_1("u_kernelSize", dsm::app::g_renderOption.get_int("ssao.kernelSize"));
    shader_pass2.set_uniform_1("u_sampleRadius", dsm::app::g_renderOption.get_float("ssao.sampleRadius"));
    shader_pass2.set_uniform_1("u_bias", dsm::app::g_renderOption.get_float("ssao.bias"));
    shader_pass2.set_uniform_matrix_4("u_projection", kt84::graphics_util::glGetXf<Matrix4f>(kt84::graphics_util::ParamName::PROJECTION_MATRIX));

    // reset modelview/projection matrices
    glMatrixMode(GL_PROJECTION);    glPushMatrix();     glLoadIdentity();
    glMatrixMode(GL_MODELVIEW );    glPushMatrix();     glLoadIdentity();

    // draw quad
    glActiveTexture(GL_TEXTURE0);   texture_position.bind();
    glActiveTexture(GL_TEXTURE1);   texture_normal.bind();
    glActiveTexture(GL_TEXTURE2);   texture_noise.bind();
    drawQuad();

    // pass 3: draw final image to screen
    kt84::FramebufferObject::unbind();
    kt84::ProgramObject::disable();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    // background color ramp
    glDepthMask(GL_FALSE);
    glBegin(GL_QUADS);
    glColor3f(g_renderOption.get_color3f("bgcolor_bottom"));    glVertex2d(-1, -1);    glVertex2d( 1, -1);
    glColor3f(g_renderOption.get_color3f("bgcolor_top"));    glVertex2d( 1,  1);    glVertex2d(-1,  1);
    glEnd();
    glDepthMask(GL_TRUE);

    shader_pass3.enable();
    shader_pass3.set_uniform_3("u_lightDir", dsm::app::g_renderOption.get_dir3f("phong.lightDir"));
    shader_pass3.set_uniform_1("u_k_ambient", dsm::app::g_renderOption.get_float("phong.k_ambient"));
    shader_pass3.set_uniform_1("u_k_diffuse", dsm::app::g_renderOption.get_float("phong.k_diffuse"));
    shader_pass3.set_uniform_1("u_k_specular", dsm::app::g_renderOption.get_float("phong.k_specular"));
    shader_pass3.set_uniform_1("u_shininess", dsm::app::g_renderOption.get_float("phong.shininess"));

    shader_pass3.set_uniform_1("u_screenWidth", dsm::app::g_camera.width);
    shader_pass3.set_uniform_1("u_screenHeight", dsm::app::g_camera.height);
    shader_pass3.set_uniform_1("u_blurRadius", dsm::app::g_renderOption.get_int("ssao.blurRadius"));

    // draw quad
    glActiveTexture(GL_TEXTURE0);   texture_position.bind();
    glActiveTexture(GL_TEXTURE1);   texture_normal.bind();
    glActiveTexture(GL_TEXTURE2);   texture_color.bind();
    glActiveTexture(GL_TEXTURE3);   texture_occlusion.bind();
    drawQuad();

    // restore modelview/projection matrices
    glMatrixMode(GL_PROJECTION);    glPopMatrix();
    glMatrixMode(GL_MODELVIEW );    glPopMatrix();

    // cleanup
    glActiveTexture(GL_TEXTURE0);   kt84::TextureObject::unbind();
    glActiveTexture(GL_TEXTURE1);   kt84::TextureObject::unbind();
    glActiveTexture(GL_TEXTURE2);   kt84::TextureObject::unbind();
    glActiveTexture(GL_TEXTURE3);   kt84::TextureObject::unbind();
    kt84::ProgramObject::disable();

    fbo_pass1.blit(kt84::FramebufferObject(), g_camera.width, g_camera.height);
}

void dsm::app::SSAO::drawQuad() {
    dispList_quad.render([&](){
        glBegin(GL_QUADS);
        glTexCoord2d(0, 0);   glVertex2d(-1, -1);
        glTexCoord2d(1, 0);   glVertex2d( 1, -1);
        glTexCoord2d(1, 1);   glVertex2d( 1,  1);
        glTexCoord2d(0, 1);   glVertex2d(-1,  1);
        glEnd();
    });
}
