#pragma once
#include "../app.h"

namespace dsm { namespace app {

struct State_EditHexLayout : public State {
    void init();
    const char* name() const override { return "EditHexLayout"; }
    void enter() override;
    void render_ssao() override;
    void render() override;
    void mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) override;
    void mouseUp  (int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) override;
    void mouseMove(int mouse_x, int mouse_y) override;
    void keyboard  (int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) override;

    struct Selected {
        int sheet_id = -1;      // for i-th sheet, 2*i represents the front side, 2*i+1 represents the back side
        void init() { *this = {}; }
    } m_selected;

    struct MementoData {
        std::vector<std::array<uint32_t,2>> sheetNumSubdiv;
        int selected_sheet_id;
        void set(const std::vector<Sheet>& sheets, int selected_sheet_id_) {
            sheetNumSubdiv.resize(sheets.size());
            for (size_t i = 0; i < sheets.size(); ++i)
                sheetNumSubdiv[i] = sheets[i].numSubdiv;
            selected_sheet_id = selected_sheet_id_;
        }
        void get(std::vector<Sheet>& sheets, int& selected_sheet_id_) const {
            for (size_t i = 0; i < sheets.size(); ++i)
                sheets[i].numSubdiv = sheetNumSubdiv[i];
            selected_sheet_id_ = selected_sheet_id;
        }
    };
    kt84::Memento<MementoData> m_memento;

    std::array<kt84::DisplayList,2> m_dispList;
};

extern State_EditHexLayout g_state_EditHexLayout;

} }
