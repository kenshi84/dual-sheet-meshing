#pragma once

#include <kt84/graphics/FramebufferObject.hh>
#include <kt84/graphics/TextureObjectT.hh>
#include <kt84/graphics/RenderbufferObject.hh>
#include <kt84/graphics/ProgramObject.hh>
#include <kt84/graphics/DisplayList.hh>

namespace dsm { namespace app {

struct SSAO {
    kt84::FramebufferObject fbo_pass1;
    kt84::FramebufferObject fbo_pass2;
    kt84::TextureObject texture_position;
    kt84::TextureObject texture_normal;
    kt84::TextureObject texture_color;
    kt84::TextureObject texture_noise;
    kt84::TextureObject texture_occlusion;
    kt84::RenderbufferObject rbo_depth;

    kt84::ProgramObject shader_pass1;
    kt84::ProgramObject shader_pass2;
    kt84::ProgramObject shader_pass3;

    kt84::DisplayList dispList_quad;

    static const int KERNEL_SIZE_MAX;

    void init(int width, int height);
    void allocate(int width, int height);
    void render_begin();
    void render_end();
private:
    void drawQuad();
};

} }
