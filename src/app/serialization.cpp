#include "../app.h"
#include "../helper.h"
#include "../serialization.h"
#include "State_EditHexLayout.h"
#include "State_EditHexMesh.h"
#include "State_EditSheet.h"
#include "State_EditFeatureGraph.h"
using namespace std;
using namespace Eigen;
using namespace kt84;

bool dsm::app::SerializedBlob::save(const string& filename) {
    ScopedTimerPrinter timer{string{"Saving to "} + filename};
    const size_t step =
        (g_state == &g_state_EditSheet || g_state == &g_state_EditFeatureGraph) ? 1 :
        g_state == &g_state_EditHexLayout ? 2 :
        g_state == &g_state_EditHexMesh ? 3 :
        0;
    assert(step != 0);
    vector<string> blobs(1);
    serializeToBlob(step, blobs[0]);

    // triMesh
    if (triMesh.empty() || g_state == &g_state_EditFeatureGraph) {
        serializeToBlob(g_triMesh, triMesh);
    }
    blobs.push_back(triMesh);

    // tetMesh
    if (tetMesh.empty()) {
        serializeToBlob(g_tetMesh, tetMesh);
    }
    blobs.push_back(tetMesh);

    // sheets
    if (sheets.empty() ||  step == 1) {
        serializeToBlob(g_sheets, sheets);
    }
    blobs.push_back(sheets);

    if (step >= 2) {
        // hexLayout
        if (hexLayout.empty()) {
            serializeToBlob(g_hexLayout, hexLayout);
        }
        blobs.push_back(hexLayout);

        // refinedTetMesh
        if (refinedTetMesh.empty()) {
            serializeToBlob(g_refinedTetMesh, refinedTetMesh);
        }
        blobs.push_back(refinedTetMesh);

        // sheetNumSubdiv
        if (step == 2) {
            vector<array<uint32_t,2>> sheetNumSubdiv(g_sheets.size());
            for (size_t i = 0; i < g_sheets.size(); ++i)
                sheetNumSubdiv[i] = g_sheets[i].numSubdiv;
            blobs.push_back({});
            serializeToBlob(sheetNumSubdiv, blobs.back());
        }
    }
    if (step == 3) {
        // hexMesh
        if (hexMesh.empty()) {
            serializeToBlob(g_hexMesh, hexMesh);
        }
        blobs.push_back(hexMesh);

        // hexMeshVertices
        vector<Vector3d> hexMeshVertices(g_hexMesh.n_vertices());
        for (VHandle hexmesh_v : g_hexMesh.vertices())
            hexMeshVertices[hexmesh_v] = vector_cast<3,Vector3d>(g_hexMesh.vertex(hexmesh_v));
        blobs.push_back({});
        serializeToBlob(hexMeshVertices, blobs.back());
    }
    return helper::write_blobs_to_file(filename, blobs);
}

bool dsm::app::SerializedBlob::load(const string& filename) {
    vector<string> blobs;
    if (!helper::read_blobs_from_file(filename, blobs))
        return false;

    if (blobs.size() < 4) {
        cerr << "Expected no less than 4 blobs, found: " << blobs.size() << endl;
        return false;
    }

    int index = 0;
    size_t step;
    deserializeFromBlob(step, blobs[index++]);
    if (step < 1 || 3 < step) {
        cerr << "Unexpected step: " << step << endl;
        return false;
    }

    ScopedTimerPrinter timer{string{"Loading from "} + filename};
    *this = {};

    // triMeesh
    triMesh = blobs[index++];
    deserializeFromBlob(g_triMesh, triMesh);
    g_triMesh.preprocess(g_triMesh.creaseAngle);

    // tetMesh
    tetMesh = blobs[index++];
    deserializeFromBlob(g_tetMesh, tetMesh);
    misc::buildBVH(g_tetMesh, 10);

    // sheets
    sheets = blobs[index++];
    deserializeFromBlob(g_sheets, sheets);
    for (Sheet& sheet : g_sheets) {
        sheet.update(g_tetMesh);
    }

    if (step >= 2) {
        // hexLayout
        hexLayout = blobs[index++];
        deserializeFromBlob(g_hexLayout, hexLayout);

        // refinedTetMesh
        refinedTetMesh = blobs[index++];
        deserializeFromBlob(g_refinedTetMesh, refinedTetMesh);

        // sheetNumSubdiv
        if (step == 2) {
            vector<array<uint32_t,2>> sheetNumSubdiv(g_sheets.size());
            deserializeFromBlob(sheetNumSubdiv, blobs[index++]);
            for (size_t i = 0; i < g_sheets.size(); ++i) {
                g_sheets[i].numSubdiv = sheetNumSubdiv[i];
            }
        }
    }
    if (step == 3) {
        // hexMesh
        hexMesh = blobs[index++];
        deserializeFromBlob(g_hexMesh, hexMesh);

        // hexMeshVertices
        vector<Vector3d> hexMeshVertices;
        deserializeFromBlob(hexMeshVertices, blobs[index++]);
        for (VHandle hexmesh_v : g_hexMesh.vertices()) {
            g_hexMesh.set_vertex(hexmesh_v, vector_cast<3,Vec3d>(hexMeshVertices[hexmesh_v]));
        }
    }

    if (step == 1) {
        changeState(g_state_EditSheet);
    } else if (step == 2) {
        changeState(g_state_EditHexLayout);
    } else {
        changeState(g_state_EditHexMesh);
    }

    return true;
}
