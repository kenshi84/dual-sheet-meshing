#include "State_EditSheet.h"
#include "State_EditFeatureGraph.h"
#include "State_EditHexLayout.h"
#include "../misc.h"
#include "../core.h"
#include "drawhelper.h"
#include "instant-meshes/field.h"
#include <kt84/vector_cast.hh>
using namespace std;
using namespace Eigen;
using namespace kt84;
using namespace kt84::graphics_util;

dsm::app::State_EditSheet dsm::app::g_state_EditSheet;

void dsm::app::State_EditSheet::init() {
    m_bar = TwNewBar(name());
    tw_util::set_bar_size(m_bar, g_camera.width/6, g_camera.height*9/10);
    tw_util::set_bar_position(m_bar, 20, 20);
    tw_util::set_bar_valueswidth(m_bar, g_camera.width/12);
    tw_util::set_bar_text_light(m_bar);
    tw_util::set_bar_color(m_bar, 50, 100, 150);
    tw_util::set_bar_visible(m_bar, false);

    g_renderOption.addToBar(m_bar);
    TwAddSeparator(m_bar, nullptr, nullptr);

    tw_util::AddButton(m_bar, "load_binary",
        [&](){
            if (g_config.autoSave.unsaved && !tinyfd_util::messageBox(APPNAME " - Load", "Current change is not saved yet. \nAre you sure to discard it anyway?", "yesno", "warning"))
                return;
            auto fname = tinyfd_util::openFileDialog(APPNAME " - Load", "", {"*.dat"}, "Binary files", false);
            if (!fname)
                return;
            if (!loadBinary(*fname)) {
                tinyfd_util::messageBox(APPNAME " - Load", "Error occurred!", "ok", "error", 1);
                return;
            }
            // TODO: display update
        }, "label='Load' key=CTRL+o");

    tw_util::AddButton(m_bar, "save_binary",
        [&](){
            if (g_triMesh.is_empty())
                return;
            auto fname = tinyfd_util::saveFileDialog(APPNAME " - Save", g_filename_dat, {"*.dat"}, "Binary files");
            if (!fname)
                return;
            if (!boost::ends_with(*fname, ".dat")) {
                tinyfd_util::messageBox(APPNAME " - Save", "Please use *.dat as the file extension.", "ok", "error", 1);
                return;
            }
            if (!saveBinary(*fname)) {
                tinyfd_util::messageBox(APPNAME " - Save", "Error occurred!", "ok", "error", 1);
                return;
            }
        }, "label='Save'");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------

    tw_util::AddButton(m_bar, "import_trimesh",
        [&](){
            if (g_config.autoSave.unsaved && !tinyfd_util::messageBox(APPNAME " - Import triangle mesh", "Unsaved changes will be discarded. Continue?", "yesno", "warning"))
                return;
            auto fname = tinyfd_util::openFileDialog(APPNAME " - Import triangle mesh", "", {"*.obj", "*.off"}, "triangle mesh files", false);
            if (!fname)
                return;
            importTriMesh(*fname);
            // TODO: display update
        }, "label='Import TriMesh'");

    // group 'Details' -----------------------------------------------------------
    TwAddVarRW(m_bar, "tetgen1_q_ratio", TW_TYPE_DOUBLE, &g_config.tetgen_switches[0].q_ratio, "group=Details min=1.1125 step=0.0001");
    TwAddVarRW(m_bar, "tetgen1_q_angle", TW_TYPE_DOUBLE, &g_config.tetgen_switches[0].q_angle, "group=Details min=0 max=20 step=0.1");
    TwAddVarRW(m_bar, "tetgen1_a", TW_TYPE_DOUBLE, &g_config.tetgen_switches[0].a_fixed_value, "group=Details min=0.000001 step=0.000001");
    tw_util::AddButton(m_bar, "tetrahedralize",
        [&](){
            g_triMesh.tetrahedralize(g_tetMesh, g_config.tetgen_switches[0]);
            g_serializedBlob = {};
            for (auto& sheet : g_sheets)
                sheet.update(g_tetMesh);
            saveBinary();
        }, "group=Details label=Tetrahedralize");

    tw_util::set_var_opened(m_bar, "Details", false);

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------

    tw_util::AddButton(m_bar, "undo",
        [&](){
            MementoData mementoData{g_sheets, m_selected.sheet_id() };
            if (m_memento.undo(mementoData)) {
                g_config.autoSave.unsaved = true;
                g_sheets = mementoData.sheets;
                Sheet* p = nullptr;
                for (auto& sheet : g_sheets) {
                    if (sheet.id == mementoData.selected_sheet_id) {
                        p = &sheet;
                        break;
                    }
                }
                m_selected.init(p);
                for (auto& sheet : g_sheets)
                    sheet.dispList.invalidate();
            }
        }, "label=Undo key=CTRL+z");

    tw_util::AddButton(m_bar, "redo",
        [&](){
            MementoData mementoData{g_sheets, m_selected.sheet_id() };
            if (m_memento.redo(mementoData)) {
                g_config.autoSave.unsaved = true;
                g_sheets = mementoData.sheets;
                Sheet* p = nullptr;
                for (auto& sheet : g_sheets) {
                    if (sheet.id == mementoData.selected_sheet_id) {
                        p = &sheet;
                        break;
                    }
                }
                m_selected.init(p);
                for (auto& sheet : g_sheets)
                    sheet.dispList.invalidate();
            }
        }, "label=Redo key=CTRL+y");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------

    tw_util::AddVarCB_default(m_bar, "show_surface", g_renderOption.get_bool("show_surface"), [&](){ m_selected.init(m_selected.sheet); }, "label='Show Surface' key=SPACE");
    TwAddVarRW(m_bar, "use_ortho", TW_TYPE_BOOLCPP, &g_renderOption.get_bool("use_ortho"), "label='Ortho Proj'");
    TwAddVarRW(m_bar, "hide_other_sheets", TW_TYPE_BOOLCPP, &g_renderOption.get_bool("hide_other_sheets"), "label='Hide Other Sheets'");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------

    tw_util::AddVarCB<int>(m_bar, "sheet_id", [&](const int& value){
        if (value >= 0 && value < g_sheets.size()) {
            m_selected.init(&g_sheets[value]);
        }
    }, [&](int& value) {
        if (m_selected.sheet)
            value = std::distance(&g_sheets[0], m_selected.sheet);
        else
            value = -1;
    }, "label='Sheet ID'");

    tw_util::AddVarCB<std::string>(m_bar, "sheet_type", {/* read-only */}, [&](std::string& value) {
        if (m_selected.sheet)
            value = m_selected.sheet->typeName();
        else
            value = "--";
    }, "label='Sheet Type'");

    tw_util::AddButton(m_bar, "delete_sheet",
        [&](){
            if (!m_selected.sheet)
                return;
            m_memento.store({g_sheets, m_selected.sheet_id()});
            g_config.autoSave.unsaved = true;
            for (auto i = g_sheets.begin(); ; ++i) {
                if (&*i == m_selected.sheet) {
                    g_sheets.erase(i);
                    // TODO: update display
                    break;
                }
            }
            m_selected.init();
            // TODO: update sheet geometry & display list;
        }, "label='Delete Sheet'");

    tw_util::AddButton(m_bar, "delete_constraint",
        [&](){
            if (!m_selected.freeformPoint && !m_selected.freeformSurfacePoint && !m_selected.sketch2dPoint)
                return;
            m_memento.store({g_sheets, m_selected.sheet_id()});
            g_config.autoSave.unsaved = true;
            if (m_selected.freeformPoint) {
                for (auto i = m_selected.sheet->freeform.points.begin(); ; ++i) {
                    if (&*i == m_selected.freeformPoint) {
                        m_selected.sheet->freeform.points.erase(i);
                        break;
                    }
                }
            } else if (m_selected.freeformSurfacePoint) {
                for (auto i = m_selected.sheet->freeform.surfacePoints.begin(); ; ++i) {
                    if (&*i == m_selected.freeformSurfacePoint) {
                        m_selected.sheet->freeform.surfacePoints.erase(i);
                        break;
                    }
                }
            } else {
                for (auto i = m_selected.sheet->sketch2d.points.begin(); ; ++i) {
                    if (&*i == m_selected.sketch2dPoint) {
                        m_selected.sheet->sketch2d.points.erase(i);
                        break;
                    }
                }
            }
            bool should_delete = false;
            if (m_selected.sketch2dPoint) {
                should_delete = m_selected.sheet->sketch2d.points.empty();
            } else {
                should_delete = m_selected.sheet->freeform.is_empty();
            }
            if (should_delete) {
                for (auto i = g_sheets.begin(); ; ++i) {
                    if (&*i == m_selected.sheet) {
                        g_sheets.erase(i);
                        m_selected.init();
                        return;
                    }
                }
            }
            m_selected.sheet->update(g_tetMesh);
            m_selected.init(m_selected.sheet);
        }, "label='Delete Constraint' key=CTRL+d");

    tw_util::AddVarCB<bool>(m_bar, "rotate_canvas", [&](const bool& value){
        if (m_selected.sheet_type() == Sheet::Type::Sketch2D || m_selected.sheet_type() == Sheet::Type::Cylinder) {
            m_canvasRotating = value;
            if (m_canvasRotating) {
                m_memento.store({g_sheets, m_selected.sheet_id()});
                g_config.autoSave.unsaved = true;
                m_selected.init(m_selected.sheet);
                g_camera.eye = m_selected.sheet->canvas.translation + 2 * m_selected.sheet->canvas.rotation.col(2);
                g_camera.up = m_selected.sheet->canvas.rotation.col(1);
                g_camera.center = m_selected.sheet->canvas.translation - 0.5 * m_selected.sheet->canvas.rotation.col(2);
            }
        }
    }, [&](bool& value) {
        if (m_selected.sheet_type() == Sheet::Type::Sketch2D || m_selected.sheet_type() == Sheet::Type::Cylinder)
            value = m_canvasRotating;
        else
            value = false;
    }, "label='Rotate Canvas'");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------

    tw_util::AddButton(m_bar, "edit_feature_graph",
        [&](){
            if (g_triMesh.is_empty())
                return;
            changeState(g_state_EditFeatureGraph);
        }, "label='Edit Feature Graph'");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------

    TwAddVarRW(m_bar, "tetgen2_q_ratio", TW_TYPE_DOUBLE, &g_config.tetgen_switches[1].q_ratio, "min=1.1125 max=3 step=0.0001");
    TwAddVarRW(m_bar, "tetgen2_q_angle", TW_TYPE_DOUBLE, &g_config.tetgen_switches[1].q_angle, "min=0 max=20 step=0.1");
    TwAddVarRW(m_bar, "tetgen2_a", TW_TYPE_DOUBLE, &g_config.tetgen_switches[1].a_fixed_value, "min=0.000001 step=0.000001");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    static bool init_numSubdiv = true;
    static double targetEdgeLength = 0.1;
    TwAddVarRW(m_bar, "init_numSubdiv", TW_TYPE_BOOLCPP, &init_numSubdiv, nullptr);
    TwAddVarRW(m_bar, "targetEdgeLength", TW_TYPE_DOUBLE, &targetEdgeLength, "min=0.001 step=0.001");
    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    static int rand_seed = -1;
    TwAddVarRW(m_bar, "rand_seed", TW_TYPE_INT32, &rand_seed, "min=-1");
    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------

    tw_util::AddButton(m_bar, "next",
        [&](){
            if (g_triMesh.is_empty())
                return;
            try {
                core::computeHexLayoutTopology_step1_dualize(g_hexLayout, g_tetMesh, g_sheets, rand_seed);
            } catch (const error::DualizationFailed& e) {
                cout << "Dualization failed!" << endl;
                cout << "Reason: " << (
                    e.type == error::DualizationFailed_Type::TangentialIntersection ? "Two sheets intersect tangentially." :
                    e.type == error::DualizationFailed_Type::MoreThanThreeSheetsIntersect ? "More than three sheets intersect at almost the same location." :
                    e.type == error::DualizationFailed_Type::MultiplePrimalCellsWithSameCode ? "Found multiple hex cells sharing the same code." :
                    e.type == error::DualizationFailed_Type::IsolatedDuallCell ? "Found isolated duall cell." :
                    e.type == error::DualizationFailed_Type::UnsupportedSingularity ? "Unsupported singularity type." :
                    "Unknown") << endl;
                cout << "Involved sheets:";
                for (int i : e.sheetId)
                    cout << " " << i;
                cout << endl;
                cout << "Relevant locations:" << endl;
                for (const Vector3d& p : e.locations)
                    cout << p.transpose() << endl;
                return;
            }
            if (init_numSubdiv) {
                core::computeHexLayoutTopology_step1a_init_numSubdiv(g_hexLayout, g_tetMesh, g_sheets, targetEdgeLength);
            }
            try {
                core::computeHexLayoutTopology_step2_check_featureGraph(g_hexLayout, g_triMesh, g_sheets);
            } catch (const error::FeatureGraphInconsistent& e) {
                cout << "The induced hex layout is inconsistent with the FeatureGraph!" << endl;
                cout << "Reason: " << (
                    e.type == error::FeatureGraphInconsistent_Type::MultipleNodesInDualCell ? "Multiple nodes exist in the same dual cell." :
                    e.type == error::FeatureGraphInconsistent_Type::MultipleArcsOnDualFace ? "Multiple arcs cross the same dual face." :
                    e.type == error::FeatureGraphInconsistent_Type::NonStarLikeConfigInDuallCell ? "Found a dual cell not having star-like configuration." :
                    e.type == error::FeatureGraphInconsistent_Type::ArcWithNoIntersection ? "An arc is not intersected by any sheets." :
                    "Unknown") << endl;
                if (e.layout_vertex.is_valid())
                    cout << "Relevant layout vertex: " << e.layout_vertex << endl;
                if (e.layout_edge.is_valid())
                    cout << "Relevant layout edge: " << e.layout_edge << endl;
                if (e.featuregraph_arc != -1)
                    cout << "Relevant arc: " << e.featuregraph_arc << endl;
                return;
            }
            saveBinary();
            core::computeHexLayoutGeometry_step1_tetrahedralize(g_hexLayout, g_refinedTetMesh, g_triMesh, g_tetMesh, g_sheets, g_config.tetgen_switches[1]);
            core::computeHexLayoutGeometry_step2_parameterize_dualFace(g_hexLayout, g_refinedTetMesh);
            changeState(g_state_EditHexLayout);
            g_serializedBlob.hexLayout.clear();
            g_serializedBlob.refinedTetMesh.clear();
            saveBinary();
        }, "label='Next'");
}

void dsm::app::State_EditSheet::enter() {
    m_memento.init(g_config.undo_buffer_size);
    m_selected.init();
    g_renderOption.get_bool("show_surface_edges") = false;
    g_renderOption.get_bool("show.featuregraph") = true;
}

void dsm::app::State_EditSheet::render_ssao() {
    // draw model surface
    if (g_renderOption.get_bool("show_surface")) {
        glEnable(GL_CULL_FACE);
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f("model"));
        g_dispList.triMesh_surface.render([&] () {
            glBegin(GL_TRIANGLES);
            for (uint32_t f = 0; f < g_triMesh.F.cols(); ++f) {
                for (uint32_t i = 0; i < 3; ++i) {
                    uint32_t v = g_triMesh.F(i, f);
                    if (g_triMesh.creases.count(v))
                        glNormal3f(Vector3f(g_triMesh.Nf.col(f)));
                    else
                        glNormal3f(Vector3f(g_triMesh.N.col(v)));
                    glVertex3f(Vector3f(g_triMesh.V.col(v)));
                }
            }
            glEnd();
        });
    }
    // draw feature graph
    if (g_renderOption.get_bool("show.featuregraph")) {
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f("featuregraph.node"));
        for (int v = 0; v < g_triMesh.V.cols(); ++v) {
            if (g_triMesh.vertex_constraint[v])
                drawhelper::drawSphere(g_triMesh.V.col(v).cast<double>(), g_renderOption.get_float("featuregraph.node.r"));
        }
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f("featuregraph.arc"));
        for (int e = 0; e < g_triMesh.edges.size(); ++e) {
            if (g_triMesh.edge_constraint[e]) {
                const array<Vector3d,2> p = {
                    g_triMesh.V.col(g_triMesh.edges[e][0]).cast<double>(),
                    g_triMesh.V.col(g_triMesh.edges[e][1]).cast<double>()
                };
                drawhelper::drawCylinder(p[0], p[1], g_renderOption.get_float("featuregraph.arc.r"));
            }
        }
    }
    // draw sheets
    for (Sheet& sheet : g_sheets) {
        // draw sheet surface
        if (g_renderOption.get_bool("hide_other_sheets") && m_selected.sheet != &sheet)
            continue;
        if (!g_renderOption.get_bool("show_surface")) {
            glDisable(GL_CULL_FACE);
            g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f(m_selected.sheet == &sheet ? "sheet_selected" : "sheet"));
            sheet.dispList.render([&]() {
                glBegin(GL_TRIANGLES);
                for (auto& tet : sheet.isosurface.perTetData) {
                    glNormal3d(tet.gradient.normalized());
                    if (tet.intersections.size() >= 3) {
                        glVertex3f(tet.intersections[0].getPosition(g_tetMesh));
                        glVertex3f(tet.intersections[1].getPosition(g_tetMesh));
                        glVertex3f(tet.intersections[2].getPosition(g_tetMesh));
                    }
                    if (tet.intersections.size() == 4) {
                        glVertex3f(tet.intersections[0].getPosition(g_tetMesh));
                        glVertex3f(tet.intersections[2].getPosition(g_tetMesh));
                        glVertex3f(tet.intersections[3].getPosition(g_tetMesh));
                    }
                }
                glEnd();
            });
        }
        // draw sheet boundary edges
        glEnable(GL_CULL_FACE);
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f(m_selected.sheet == &sheet ? "sheet_selected" : "sheet"));
        if (!sheet.isosurface.perFaceData.empty()) {
            for (auto f : g_tetMesh.faces()) {
                if (!g_tetMesh.is_boundary(f))
                    continue;
                auto& intersections = sheet.isosurface.perFaceData[f].intersections;
                if (intersections[0].is_valid()) {
                    drawhelper::drawCylinder(intersections[0].getPosition(g_tetMesh).cast<double>(), intersections[1].getPosition(g_tetMesh).cast<double>(), g_renderOption.get_float("sheet.boundary.r"));
                }
            }
        }
    }
    // draw arrows for Freeform constraint points
    glEnable(GL_CULL_FACE);
    if (m_selected.sheet_type() == Sheet::Type::Freeform) {
        for (const auto& p : m_selected.sheet->freeform.points) {
            g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f(m_selected.freeformPoint == &p ? "constraint_selected" : "constraint"));
            drawhelper::drawSphere(p.head(3), 0.02);
            drawhelper::drawArrow(p.head(3), p.tail(3), 0.01, 0.04, 0.02, 0.04);
        }
        for (const auto& p : m_selected.sheet->freeform.surfacePoints) {
            g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f(m_selected.freeformSurfacePoint == &p ? "constraint_selected" : "constraint"));
            const Vector3d p2 = p.first.getPosition(g_triMesh).cast<double>();
            drawhelper::drawSphere(p2, 0.02);
            drawhelper::drawArrow(p2, p.second.tail(3), 0.01, 0.04, 0.02, 0.04);
        }
    }
    // draw arrows for Sketch2D constraint points
    if (m_selected.sheet_type() == Sheet::Type::Sketch2D) {
        for (const auto& pn : m_selected.sheet->sketch2d.points) {
            g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f(m_selected.sketch2dPoint == &pn ? "constraint_selected" : "constraint"));
            Vector3d p = m_selected.sheet->canvas.unproject(pn.head(2));
            Vector3d n = m_selected.sheet->canvas.rotation * Vector3d(pn[2], pn[3], 0);
            drawhelper::drawSphere(p, 0.02);
            drawhelper::drawArrow(p, n, 0.01, 0.04, 0.02, 0.04);
        }
    }
    // draw loop for Cylinder
    if (m_selected.sheet_type() == Sheet::Type::Cylinder) {
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f(m_selected.cylinderRadius ? "constraint_selected" : "constraint"));
        int num_segments = max<int>(2 * util::pi() * m_selected.sheet->cylinder.radius / 0.05, 12);
        vector<Vector3d> p(num_segments);
        for (int i = 0; i < num_segments; ++i) {
            double t = i * 2 * util::pi() / num_segments;
            p[i] = m_selected.sheet->canvas.unproject(m_selected.sheet->cylinder.radius * Vector2d(cos(t), sin(t)));
        }
        for (int i = 0; i < num_segments; ++i) {
            drawhelper::drawCylinder(p[i], p[(i + 1) % num_segments], 0.003);
        }
    }
    // draw canvas edges
    if (m_selected.sheet_type() == Sheet::Type::Cylinder || m_selected.sheet_type() == Sheet::Type::Sketch2D) {
        const array<Vector3d, 4> canvas_corners = m_selected.sheet->canvas.corners(g_renderOption.get_float("canvas.r"));
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f(m_canvasRotating ? "canvas_rotating" : "canvas"));
        for (int i = 0; i < 4; ++i) {
            drawhelper::drawCylinder(canvas_corners[i], canvas_corners[(i + 1) % 4], 0.007);
            drawhelper::drawSphere(canvas_corners[i], 0.01);
        }
    }
}

void dsm::app::State_EditSheet::render() {
    // draw silhouettes
    if (g_renderOption.get_bool("show_silhouette")) {
        g_shader.silhouette.enable();
        g_shader.silhouette.set_uniform_3<Vector3f>("u_eye", g_camera.get_eye().cast<float>());
        g_shader.silhouette.set_uniform_3<Vector3f>("u_color", Vector3f(0,0,0));
        g_dispList.triMesh_silhouette.render([&] () {
            glLineWidth(1);
            glBegin(GL_LINES);
            for (uint32_t f0 = 0; f0 < g_triMesh.F.cols(); ++f0) {
                for (uint32_t i = 0; i < 3; ++i) {
                    uint32_t v0 = g_triMesh.F(i, f0);
                    uint32_t v1 = g_triMesh.F((i+1)%3, f0);
                    if (v0 < v1) {
                        uint32_t f1 = g_triMesh.E2E[3 * f0 + i] / 3;
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_vertex_0", g_triMesh.V.col(v0));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_vertex_1", g_triMesh.V.col(v1));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_normal_0", g_triMesh.Nf.col(f0));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_normal_1", g_triMesh.Nf.col(f1));
                        glVertex3f(Vector3f(g_triMesh.V.col(v0)));
                        glVertex3f(Vector3f(g_triMesh.V.col(v1)));
                    }
                }
            }
            glEnd();
        });
        g_shader.silhouette.disable();
    }
    // draw creases
    if (g_renderOption.get_bool("show_crease")) {
        g_dispList.triMesh_crease.render([&] () {
            glLineWidth(1);
            glBegin(GL_LINES);
            glColor3f(0, 0, 0);
            for (uint32_t f = 0; f < g_triMesh.F.cols(); ++f) {
                for (uint32_t i = 0; i < 3; ++i) {
                    const uint32_t e = 3 * f  + i;
                    if (g_triMesh.crease_edges.count(e)) {
                        glVertex3f(Vector3f(g_triMesh.V.col(g_triMesh.F(i, f))));
                        glVertex3f(Vector3f(g_triMesh.V.col(g_triMesh.F((i+1)%3, f))));
                    }
                }
            }
            glEnd();
        });
    }
    // draw sketch stroke
    glLineWidth(5);
    glDisable(GL_DEPTH_TEST);
    glBegin(GL_LINE_STRIP);
    glColor3f(g_renderOption.get_color3f(m_strokeMode == StrokeMode::Cylinder ? "stroke_cylinder" : "stroke_sketch2d"));
    for (const Vector3d& p : m_stroke) {
        glVertex3d(p);
    }
    glEnd();
    glEnable(GL_DEPTH_TEST);
    // draw canvas plane
    if (m_selected.sheet_type() == Sheet::Type::Cylinder || m_selected.sheet_type() == Sheet::Type::Sketch2D) {
        glDisable(GL_CULL_FACE);
        glBegin(GL_QUADS);
        glColor4f(g_renderOption.get_color3f(m_canvasRotating ? "canvas_rotating" : "canvas"), 0.5f);
        const array<Vector3d, 4> canvas_corners = m_selected.sheet->canvas.corners(g_renderOption.get_float("canvas.r"));
        for (int i = 0; i < 4; ++i) {
            glVertex3d(canvas_corners[i]);
        }
        glEnd();
        glEnable(GL_CULL_FACE);
    }
}

void dsm::app::State_EditSheet::mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
    static const double SELECT_EPSILON = 0.05;
    m_canvasRotating = false;
    if (g_eventData.key_pressed['A'] || g_eventData.key_pressed['S']) {
        m_strokeMode = g_eventData.key_pressed['A'] ? StrokeMode::Cylinder : StrokeMode::Sketch2D;
        mouseMove(mouse_x, mouse_y);
        return;
    }
    if (m_selected.sheet_type() == Sheet::Type::Sketch2D || m_selected.sheet_type() == Sheet::Type::Cylinder) {
        if (g_eventData.mouse_3D) {
            MaxMinAverage dist_to_edge;
            const array<Vector3d, 4> canvas_corners = m_selected.sheet->canvas.corners(g_renderOption.get_float("canvas.r"));
            for (int i = 0; i < 4; ++i) {
                auto d = eigen_util::distance_to_line(canvas_corners[i], canvas_corners[(i + 1) % 4], *g_eventData.mouse_3D, true);
                if (d)
                    dist_to_edge.update(*d);
            }
            if (dist_to_edge.min() < SELECT_EPSILON) {
                m_memento.store({g_sheets, m_selected.sheet_id()});
                g_config.autoSave.unsaved = true;
                m_selected.init(m_selected.sheet);
                m_canvasDragStart = *g_eventData.mouse_3D;
                return;
            }
        }
        const Vector3d p = m_selected.sheet->canvas.intersect(g_eventData.mouse_3Dz0, g_eventData.mouse_3Dz1);
        const Vector2d p2d = m_selected.sheet->canvas.project(p);
        // accept inputs within the canvas area only
        if (AlignedBox2d(Vector2d::Constant(-g_renderOption.get_float("canvas.r")), Vector2d::Constant(g_renderOption.get_float("canvas.r"))).contains(p2d)) {
            if (m_selected.sheet_type() == Sheet::Type::Sketch2D) {
                if (shift_pressed) {
                    // add a new point
                    m_memento.store({g_sheets, m_selected.sheet_id()});
                    g_config.autoSave.unsaved = true;
                    Vector2d n2d = m_selected.sheet->sketch2d.rbf.gradient(Vector3d(p2d[0], p2d[1], 0)).head(2).normalized();
                    m_selected.sheet->sketch2d.points.push_back(pn2d_make(p2d, n2d));
                    m_selected.sketch2dPoint = &m_selected.sheet->sketch2d.points.back();
                    return;
                } else {
                    MaxMinSelector<PointNormal2d*> clicked_point;
                    for (auto& pn2d : m_selected.sheet->sketch2d.points) {
                        clicked_point.update((pn2d.head(2) - p2d).norm(), &pn2d);
                    }
                    if (clicked_point.min_score < SELECT_EPSILON) {
                        m_memento.store({g_sheets, m_selected.sheet_id()});
                        g_config.autoSave.unsaved = true;
                        m_selected.sketch2dPoint = clicked_point.min_value;
                        return;
                    } else {
                        m_selected.sketch2dPoint = nullptr;
                    }
                }
            } else if (abs(p2d.norm() - m_selected.sheet->cylinder.radius) < SELECT_EPSILON) {
                m_memento.store({g_sheets, m_selected.sheet_id()});
                g_config.autoSave.unsaved = true;
                m_selected.cylinderRadius = &m_selected.sheet->cylinder.radius;
                return;
            }
        }
    }
    if (g_renderOption.get_bool("show_surface")) {
        if (!g_eventData.mouse_on_triMesh.is_valid()) {
            // when clicking empty space, clear selection
            m_selected.init();
            return;
        }
        const Vector3d n = g_triMesh.getNormal(g_eventData.mouse_on_triMesh);
        if (shift_pressed) {
            // add a new point constraint on the surface
            m_memento.store({g_sheets, m_selected.sheet_id()});
            g_config.autoSave.unsaved = true;
            // init direction with the 4-RoSy
            uint32_t closest_vertex = g_eventData.mouse_on_triMesh.closestVertex(g_triMesh);
            Vector3f rosy = g_triMesh.Q.col(closest_vertex);
            Vector3d dir;
            if (m_selected.sheet_type() != Sheet::Type::Freeform) {
                // create new freeform sheet
                g_sheets.push_back(Sheet::make(Sheet::Type::Freeform));
                m_selected.init(&g_sheets.back());
                dir = rosy.cast<double>();
            } else {
                MaxMinSelector<CHandle> closest_tet;
                for (auto tet : g_tetMesh.cells()) {
                    Vector3f cog = vector_cast<3, Vector3f>(g_tetMesh.barycenter(tet));
                    closest_tet.update((g_eventData.mouse_on_triMesh.getPosition(g_triMesh) - cog).norm(), tet);
                }
                Vector3f grad = m_selected.sheet->isosurface.perTetData[closest_tet.min_value].gradient.cast<float>();
                MaxMinSelector<Vector3f> closest_rosy;
                for (int i = 0; i < 4; ++i) {
                    rosy = rotate90_by(rosy, g_triMesh.N.col(closest_vertex), i);
                    closest_rosy.update(rosy.dot(grad), rosy);
                }
                dir = closest_rosy.max_value.cast<double>();
            }
            dir = (dir - dir.dot(n) * n).normalized();
            m_selected.sheet->freeform.surfacePoints.push_back({g_eventData.mouse_on_triMesh, pn_make(*g_eventData.mouse_3D, dir)});
            m_selected.freeformSurfacePoint = &m_selected.sheet->freeform.surfacePoints.back();
            m_selected.sheet->update(g_tetMesh);
            return;
        }
        // check if clicked on Freeform surface point
        if (m_selected.sheet_type() == Sheet::Type::Freeform) {
            MaxMinSelector<std::pair<TriMeshPoint, kt84::PointNormal> *> clicked_surfacePoint;
            for (std::pair<TriMeshPoint, kt84::PointNormal>& p : m_selected.sheet->freeform.surfacePoints) {
                clicked_surfacePoint.update((*g_eventData.mouse_3D - p.second.head(3)).norm(), &p);
            }
            if (clicked_surfacePoint.min_score < SELECT_EPSILON) {
                m_memento.store({g_sheets, m_selected.sheet_id()});
                g_config.autoSave.unsaved = true;
                m_selected.freeformSurfacePoint = clicked_surfacePoint.min_value;;
                return;
            }
        }
        // select clicked sheet
        m_selected.init();
        MaxMinSelector<Sheet *> clicked_sheet;
        for (Sheet& sheet : g_sheets) {
            if (!sheet.isosurface.perEdgeData.empty()) {
                for (auto e : g_tetMesh.edges()) {
                    if (g_tetMesh.is_boundary(e)) {
                        const TetMeshPoint& intersection = sheet.isosurface.perEdgeData[e].intersection;
                        if (intersection.is_valid())
                            clicked_sheet.update((*g_eventData.mouse_3D - intersection.getPosition(g_tetMesh).cast<double>()).norm(), &sheet);
                    }
                }
            }
        }
        if (clicked_sheet.min_score < SELECT_EPSILON && m_selected.sheet != clicked_sheet.min_value) {
            // m_selected a different sheet
            m_selected.init(clicked_sheet.min_value);
        }
    } else {
        if (!g_eventData.mouse_3D) {
            // when clicking empty space, clear selection
            m_selected.init();
            return;
        }
        // find clicked sheet
        MaxMinSelector<Sheet *> clicked_sheet;
        for (Sheet& sheet : g_sheets) {
            if (!sheet.isosurface.perEdgeData.empty()) {
                for (auto e : g_tetMesh.edges()) {
                    const TetMeshPoint& intersection = sheet.isosurface.perEdgeData[e].intersection;
                    if (intersection.is_valid())
                        clicked_sheet.update((*g_eventData.mouse_3D - intersection.getPosition(g_tetMesh).cast<double>()).norm(), &sheet);
                }
            }
        }
        if (m_selected.sheet != clicked_sheet.min_value) {
            // m_selected a different sheet
            m_selected.init(clicked_sheet.min_value);
            return;
        }
        if (m_selected.sheet_type() == Sheet::Type::Freeform) {
            if (shift_pressed) {
                // add a new point constraint on the surface
                m_memento.store({g_sheets, m_selected.sheet_id()});
                g_config.autoSave.unsaved = true;
                MaxMinSelector<CHandle> closest_tet;
                for (auto tet : g_tetMesh.cells()) {
                    Vector3d cog = vector_cast<3, Vector3d>(g_tetMesh.barycenter(tet));
                    closest_tet.update((*g_eventData.mouse_3D - cog).norm(), tet);
                }
                Vector3d dir = m_selected.sheet->isosurface.perTetData[closest_tet.min_value].gradient.normalized();
                m_selected.sheet->freeform.points.push_back(pn_make(*g_eventData.mouse_3D, dir));
                m_selected.freeformPoint = &m_selected.sheet->freeform.points.back();
                return;
            }
            // find clicked point
            MaxMinSelector<PointNormal *> clicked_point;
            for (PointNormal& p : m_selected.sheet->freeform.points) {
                clicked_point.update((*g_eventData.mouse_3D - p.head(3)).norm(), &p);
            }
            if (clicked_point.min_score < SELECT_EPSILON) {
                m_memento.store({g_sheets, m_selected.sheet_id()});
                g_config.autoSave.unsaved = true;
                m_selected.freeformPoint = clicked_point.min_value;
            } else {
                m_selected.freeformPoint = nullptr;
            }
        }
    }
}

void dsm::app::State_EditSheet::mouseUp  (int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
    if (m_strokeMode != StrokeMode::NONE) {
        if (m_stroke.size() > 4) {
            // create new sheet
            m_memento.store({g_sheets, m_selected.sheet_id()});
            g_config.autoSave.unsaved = true;
            g_sheets.push_back(Sheet::make(m_strokeMode==StrokeMode::Cylinder ? Sheet::Type::Cylinder : Sheet::Type::Sketch2D));
            m_selected.init(&g_sheets.back());
            m_selected.sheet->initCanvas(m_stroke, g_camera, g_renderOption.get_float("resampleInterval"));
            m_selected.sheet->update(g_tetMesh);
            m_stroke.clear();
        }
        m_strokeMode = StrokeMode::NONE;
    }
    if (m_selected.sheet_type() == Sheet::Type::Cylinder) {
        m_selected.cylinderRadius = nullptr;
    }
    if (m_canvasDragStart) {
        m_canvasDragStart = boost::none;
    }
}

void dsm::app::State_EditSheet::mouseMove(int mouse_x, int mouse_y) {
    if (!g_eventData.button_left_pressed)
        return;
    if (m_strokeMode != StrokeMode::NONE) {
        if (g_eventData.mouse_on_triMesh.is_valid())
            m_stroke.push_back(g_eventData.mouse_on_triMesh.getPosition(g_triMesh).cast<double>());
        return;
    }
    if (m_selected.sketch2dPoint || m_selected.cylinderRadius) {
        const Vector3d p = m_selected.sheet->canvas.intersect(g_eventData.mouse_3Dz0, g_eventData.mouse_3Dz1);
        const Vector2d p2d = m_selected.sheet->canvas.project(p);
        if (m_selected.sheet_type() == Sheet::Type::Sketch2D) {
            if (g_eventData.ctrl_pressed) {
                // roate
                m_selected.sketch2dPoint->tail(2) = (p2d - m_selected.sketch2dPoint->head(2)).normalized();
            } else {
                // translate
                m_selected.sketch2dPoint->head(2) = p2d;
            }
        } else {
            *m_selected.cylinderRadius = p2d.norm();
        }
        m_selected.sheet->update(g_tetMesh);
        return;
    }
    if (m_canvasDragStart) {
        assert(m_selected.sheet_type() == Sheet::Type::Sketch2D || m_selected.sheet_type() == Sheet::Type::Cylinder);
        const Vector3d p = eigen_util::intersection_plane_line<Vector3d>(*m_canvasDragStart, g_eventData.mouse_3Dz0 - g_eventData.mouse_3Dz1, g_eventData.mouse_3Dz0, g_eventData.mouse_3Dz1);
        m_selected.sheet->canvas.translation += p - *m_canvasDragStart;
        *m_canvasDragStart = p;
        m_selected.sheet->update(g_tetMesh);
        return;
    }
    if (m_selected.freeformSurfacePoint) {
        if (g_eventData.ctrl_pressed) {
            // roate normal
            const TriMeshPoint& sp = m_selected.freeformSurfacePoint->first;
            const Vector3d plane_origin = sp.getPosition(g_triMesh).cast<double>();
            const Vector3d plane_normal = g_triMesh.getNormal(sp);
            const Vector3d q = eigen_util::intersection_plane_line(plane_origin, plane_normal, g_camera.eye, g_eventData.mouse_3Dz1);
            m_selected.freeformSurfacePoint->second.tail(3) = (q - plane_origin).normalized();
        } else {
            // translate point
            if (!g_eventData.mouse_on_triMesh.is_valid())
                return;
            const Vector3d n2 = g_triMesh.getNormal(g_eventData.mouse_on_triMesh);
            Vector3d d = m_selected.freeformSurfacePoint->second.tail(3);
            d = (d - d.dot(n2) * n2).normalized();
            *m_selected.freeformSurfacePoint = { g_eventData.mouse_on_triMesh, pn_make(*g_eventData.mouse_3D, d) };
        }
        m_selected.sheet->update(g_tetMesh);
        return;
    }
    if (m_selected.freeformPoint) {
        Vector3d p = m_selected.freeformPoint->head(3);
        Vector3d n = m_selected.freeformPoint->tail(3);
        if (g_eventData.ctrl_pressed) {
            // rotate
            const Vector2d q = graphics_util::project(p).head(2);
            const double dist = (g_eventData.mouse_2D.cast<double>() - q).norm();
            const double angle = std::asin(std::min<double>(dist / 100, 1));
            const Vector3d d = (g_camera.eye - p).normalized();
            const Vector3d axis = d.cross(g_eventData.mouse_3Dz1 - p).normalized();
            n = AngleAxisd(angle, axis) * d;
        } else {
            // translate
            p = eigen_util::intersection_plane_line<Vector3d>(p, g_camera.eye - p, g_camera.eye, g_eventData.mouse_3Dz1);
        }
        *m_selected.freeformPoint << p, n;
        m_selected.sheet->update(g_tetMesh);
        return;
    }
}

void dsm::app::State_EditSheet::keyboard(int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
    if (key=='R'
        && g_renderOption.get_bool("show_surface")
        && m_selected.sheet_type()==Sheet::Type::Freeform
        && m_selected.sheet->freeform.surfacePoints.size()==1
        && m_selected.freeformSurfacePoint)
    {
        // rotate sheet by 90 degrees in a very special case
        const Vector3d n = g_triMesh.getNormal(m_selected.freeformSurfacePoint->first);
        const Vector3d d = m_selected.freeformSurfacePoint->second.tail(3);
        m_selected.freeformSurfacePoint->second.tail(3) = n.cross(d);
        m_selected.sheet->update(g_tetMesh);
    }
}

