#include "State_EditHexLayout.h"
#include "State_EditHexMesh.h"
#include "State_EditSheet.h"
#include "drawhelper.h"
#include "../core.h"
#include "../helper.h"
#include <igl/cotmatrix.h>
#include <igl/doublearea.h>
#include <igl/massmatrix.h>
#include <igl/invert_diag.h>
#include <igl/min_quad_with_fixed.h>
#include <instant-meshes/normal.h>
using namespace std;
using namespace Eigen;
using namespace kt84;
using namespace kt84::graphics_util;

dsm::app::State_EditHexLayout dsm::app::g_state_EditHexLayout;

int dbg_grid_resolution = 4;

namespace dsm {

Vector3d get_gridPoint(const array<int,3>& signature, const Vector3i& gridIndex, int cornerIndex) {
    const ReferencePolyhedron& rp = ReferencePolyhedron::get(signature);
    array<Vector3d,3> midpoint, centroid;
    int e = rp.V2E[cornerIndex];
    for (int i = 0; i < 3; ++i) {
        const int f = e/5;
        const int deg = rp.F[f].size();
        centroid[i].setZero();
        for (int j = 0; j < deg; ++j) {
            centroid[i] += rp.V.row(rp.F[f][j]) / deg;
        }
        midpoint[i] = 0.5 * (rp.V.row(cornerIndex) + rp.V.row(rp.to_vertex(e)));
        e = rp.E2E[rp.dedge_prev(e)];
    }
    assert(e == rp.V2E[cornerIndex]);
    const Vector3d bottomLeftFront  = rp.V.row(cornerIndex);
    const Vector3d bottomRightFront = midpoint[0];
    const Vector3d topLeftFront     = midpoint[1];
    const Vector3d topRightFront    = centroid[0];
    const Vector3d bottomLeftBack   = midpoint[2];
    const Vector3d bottomRightBack  = centroid[2];
    const Vector3d topLeftBack      = centroid[1];
    const Vector3d topRightBack     = {0,0,0};
    const Vector3d t = gridIndex.cast<double>() / (double)dbg_grid_resolution;
    return eigen_util::interpolate_trilinear(bottomLeftFront, bottomRightFront, topLeftFront, topRightFront, bottomLeftBack, bottomRightBack, topLeftBack, topRightBack, t);
}

Vector2d get_gridPoint2D(int ngon, const Vector2i& index, int rotation_index) {
    const double theta = 2*util::pi()/ngon;
    Rotation2D<double> R(theta);
    const Vector2d p00(tan(util::pi()/2 - theta/2), -1);
    const Vector2d p10(p00[0], 0);
    const Vector2d p01 = R.inverse() * p10;
    const Vector2d t = index.cast<double>() / (double)dbg_grid_resolution;
    Vector2d p = (1-t[0])*(1-t[1]) * p00 + t[0]*(1-t[1]) * p10 + (1-t[0])*t[1] * p01;
    for (int i = 0; i < rotation_index; ++i)
        p = R*p;
    return p;
}

}

void dsm::app::State_EditHexLayout::init() {
    m_bar = TwNewBar(name());
    tw_util::set_bar_size(m_bar, g_camera.width/6, g_camera.height*9/10);
    tw_util::set_bar_position(m_bar, 20, 20);
    tw_util::set_bar_valueswidth(m_bar, g_camera.width/12);
    tw_util::set_bar_text_light(m_bar);
    tw_util::set_bar_color(m_bar, 50, 100, 150);
    tw_util::set_bar_visible(m_bar, false);

    g_renderOption.addToBar(m_bar);
    TwAddSeparator(m_bar, nullptr, nullptr);

    tw_util::AddButton(m_bar, "load_binary",
        [&](){
            if (g_config.autoSave.unsaved && !tinyfd_util::messageBox(APPNAME " - Load", "Current change is not saved yet. \nAre you sure to discard it anyway?", "yesno", "warning"))
                return;
            auto fname = tinyfd_util::openFileDialog(APPNAME " - Load", "", {"*.dat"}, "Binary files", false);
            if (!fname)
                return;
            if (!loadBinary(*fname)) {
                tinyfd_util::messageBox(APPNAME " - Load", "Error occurred!", "ok", "error", 1);
                return;
            }
            // TODO: display update
        }, "label='Load' key=CTRL+o");

    tw_util::AddButton(m_bar, "save_binary",
        [&](){
            if (g_triMesh.is_empty())
                return;
            auto fname = tinyfd_util::saveFileDialog(APPNAME " - Save", g_filename_dat, {"*.dat"}, "Binary files");
            if (!fname)
                return;
            if (!boost::ends_with(*fname, ".dat")) {
                tinyfd_util::messageBox(APPNAME " - Save", "Please use *.dat as the file extension.", "ok", "error", 1);
                return;
            }
            if (!saveBinary(*fname)) {
                tinyfd_util::messageBox(APPNAME " - Save", "Error occurred!", "ok", "error", 1);
                return;
            }
        }, "label='Save'");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    tw_util::AddButton(m_bar, "undo",
        [&](){
            MementoData mementoData;
            mementoData.set(g_sheets, m_selected.sheet_id);
            if (m_memento.undo(mementoData)) {
                g_config.autoSave.unsaved = true;
                mementoData.get(g_sheets, m_selected.sheet_id);
                m_dispList[0].invalidate();
                m_dispList[1].invalidate();
            }
        }, "label=Undo key=CTRL+z");
    tw_util::AddButton(m_bar, "redo",
        [&](){
            MementoData mementoData;
            mementoData.set(g_sheets, m_selected.sheet_id);
            if (m_memento.redo(mementoData)) {
                g_config.autoSave.unsaved = true;
                mementoData.get(g_sheets, m_selected.sheet_id);
                m_dispList[0].invalidate();
                m_dispList[1].invalidate();
            }
        }, "label=Undo key=CTRL+y");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    tw_util::AddVarCB_default(m_bar, "show_surface", g_renderOption.get_bool("show_surface"), []{}, "label='Show Surface' key=SPACE");

    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    tw_util::AddVarCB<int>(m_bar, "num_subdiv", [&](const int& value){
        if (m_selected.sheet_id == -1)
            return;
        MementoData mementoData;
        mementoData.set(g_sheets, m_selected.sheet_id);
        m_memento.store(mementoData);
        g_config.autoSave.unsaved = true;
        g_sheets[m_selected.sheet_id / 2].numSubdiv[m_selected.sheet_id % 2] = value;
        m_dispList[0].invalidate();
        m_dispList[1].invalidate();
    }, [&](int& value) {
        if (m_selected.sheet_id == -1)
            value = -1;
        else
            value = g_sheets[m_selected.sheet_id / 2].numSubdiv[m_selected.sheet_id % 2];
    }, "label='Num subdiv' min=0 keyincr=c keydecr=x");
    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    static double targetEdgeLength = 0.1;
    TwAddVarRW(m_bar, "targetEdgeLength", TW_TYPE_DOUBLE, &targetEdgeLength, "min=0.001 step=0.001");
    tw_util::AddButton(m_bar, "init",
        [&](){
            MementoData mementoData;
            mementoData.set(g_sheets, m_selected.sheet_id);
            m_memento.store(mementoData);
            g_config.autoSave.unsaved = true;
            core::computeHexLayoutTopology_step1a_init_numSubdiv(g_hexLayout, g_tetMesh, g_sheets, targetEdgeLength);
            m_dispList[0].invalidate();
            m_dispList[1].invalidate();
        });
    //-----------------------------------------------------------
    TwAddSeparator(m_bar, nullptr, nullptr);
    //-----------------------------------------------------------
    tw_util::AddButton(m_bar, "back",
        [&](){
            changeState(g_state_EditSheet);
            saveBinary();
        }, "label='Back'");
    tw_util::AddButton(m_bar, "next",
        [&](){
            saveBinary();
            if (g_hexLayout.data(VHandle{0}).param3d.empty()) {
                core::computeHexLayoutGeometry_step3_parameterize_dualCell(g_hexLayout, g_refinedTetMesh);
                g_serializedBlob.hexLayout.clear();
            }
            core::computeHexMesh(g_hexMesh, g_hexLayout, g_refinedTetMesh, g_tetMesh, g_sheets);
            changeState(g_state_EditHexMesh);
            g_serializedBlob.sheets.clear();
            g_serializedBlob.hexMesh.clear();
            saveBinary();
        }, "label='Next'");
}

void dsm::app::State_EditHexLayout::enter() {
    m_memento.init(g_config.undo_buffer_size);
    m_selected.init();
    m_dispList[0].invalidate();
    m_dispList[1].invalidate();
    g_renderOption.get_bool("show.featuregraph") = false;
}

void dsm::app::State_EditHexLayout::render_ssao() {
    // draw feature graph
    if (g_renderOption.get_bool("show.featuregraph")) {
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f("featuregraph.node"));
        for (int v = 0; v < g_triMesh.V.cols(); ++v) {
            if (g_triMesh.vertex_constraint[v])
                drawhelper::drawSphere(g_triMesh.V.col(v).cast<double>(), g_renderOption.get_float("featuregraph.node.r"));
        }
        g_ssao.shader_pass1.set_uniform_3("u_color", g_renderOption.get_color3f("featuregraph.arc"));
        for (int e = 0; e < g_triMesh.edges.size(); ++e) {
            if (g_triMesh.edge_constraint[e]) {
                const array<Vector3d,2> p = {
                    g_triMesh.V.col(g_triMesh.edges[e][0]).cast<double>(),
                    g_triMesh.V.col(g_triMesh.edges[e][1]).cast<double>()
                };
                drawhelper::drawCylinder(p[0], p[1], g_renderOption.get_float("featuregraph.arc.r"));
            }
        }
    }
    // draw dual faces with grid lines
    g_ssao.shader_pass1.set_uniform_1i("u_showGrid", 1);
    if (g_renderOption.get_bool("show_surface")) {
        m_dispList[0].render([&]{
            for (VHandle layout_v : g_hexLayout.boundary_vertices()) {
                const int ngon = g_hexLayout.boundary_vertex_edges(layout_v).size();
                g_ssao.shader_pass1.set_uniform_1i("u_ngon", ngon);
                int i = 0;
                const TetMeshPoint testPoint = g_hexLayout.data(layout_v).testPoint;
                for (HEHandle layout_he : g_hexLayout.boundary_vertex_incoming_halfedges(layout_v)) {
                    const CHandle layout_c = g_hexLayout.cell_handle(g_hexLayout.opposite_halfface_handle(g_hexLayout.boundary_halfedge_halfface(layout_he)));
                    const Sheet& sheet = g_sheets[g_hexLayout.data(g_hexLayout.edge_handle(layout_he)).sheetId];
                    g_ssao.shader_pass1.set_uniform_3f((boost::format("u_cornerColor[%d]") % i).str(), g_hexLayout.data(layout_c).color);
                    const bool backSide = sheet.funcLinear(g_tetMesh, testPoint) < 0;
                    g_ssao.shader_pass1.set_uniform_1i((boost::format("u_numSubdiv[%d]") % i).str(), sheet.numSubdiv[backSide]);
                    ++i;
                }
                glBegin(GL_TRIANGLES);
                for (HFHandle reftet_hf : g_hexLayout.data(layout_v).reftet_boundary_halffaces) {
                    array<Vector3d,3> xyz;
                    array<Vector2d,3> uv;
                    int i = 0;
                    for (VHandle reftet_v : g_refinedTetMesh.halfface_vertices(reftet_hf)) {
                        xyz[i] = vector_cast<3,Vector3d>(g_refinedTetMesh.vertex(reftet_v));
                        uv[i] = g_hexLayout.data(layout_v).boundary_param2d.find(reftet_v)->second;
                        ++i;
                    }
                    glNormal3d((xyz[1]-xyz[0]).cross(xyz[2]-xyz[0]).normalized());
                    for (i = 0; i < 3; ++i) {
                        glTexCoord2d(uv[i]);
                        glVertex3d(xyz[i]);
                    }
                }
                glEnd();
            }
        });
    }
    m_dispList[1].render([&]{
        for (EHandle layout_e : g_hexLayout.edges()) {
            const HEHandle layout_he = g_hexLayout.halfedge_handle(layout_e,0);
            const int ngon = g_hexLayout.edge_cells(layout_e).size() + (g_hexLayout.is_boundary(layout_e) ? 2 : 0);
            g_ssao.shader_pass1.set_uniform_1i("u_ngon", ngon);
            int i = 0;
            if (g_hexLayout.is_boundary(layout_e)) {
                g_ssao.shader_pass1.set_uniform_1i((boost::format("u_numSubdiv[%d]") % i).str(), 0);    // irrelevant as this section is skipped
                i = 1;
            }
            const TetMeshPoint testPoint = g_hexLayout.data(layout_e).testPoint;
            for (HFHandle layout_hf : g_hexLayout.halfedge_halffaces(layout_he)) {
                const CHandle layout_c = g_hexLayout.cell_handle(g_hexLayout.opposite_halfface_handle(layout_hf));
                if (layout_c.is_valid()) {
                    g_ssao.shader_pass1.set_uniform_3f((boost::format("u_cornerColor[%d]") % i).str(), g_hexLayout.data(layout_c).color);
                }
                const vector<HEHandle> layout_he2 = g_hexLayout.halfface_halfedges(layout_hf);
                int j = 0;
                while (layout_he2[j] != layout_he) ++j;
                const Sheet& sheet = g_sheets[g_hexLayout.data(g_hexLayout.edge_handle(layout_he2[(j+1)%4])).sheetId];
                const bool backSide = sheet.funcLinear(g_tetMesh, testPoint) < 0;
                g_ssao.shader_pass1.set_uniform_1i((boost::format("u_numSubdiv[%d]") % i).str(), sheet.numSubdiv[backSide]);
                ++i;
            }
            assert(i == ngon);
            glBegin(GL_TRIANGLES);
            for (HFHandle reftet_hf : g_hexLayout.data(layout_he).reftet_halffaces) {
                array<Vector3d,3> xyz;
                array<Vector2d,3> uv;
                int i = 0;
                for (VHandle reftet_v : g_refinedTetMesh.halfface_vertices(reftet_hf)) {
                    xyz[i] = vector_cast<3,Vector3d>(g_refinedTetMesh.vertex(reftet_v));
                    uv[i] = g_hexLayout.data(layout_e).param2d.find(reftet_v)->second;
                    ++i;
                }
                glNormal3d((xyz[1]-xyz[0]).cross(xyz[2]-xyz[0]).normalized());
                for (i = 0; i < 3; ++i) {
                    glTexCoord2d(uv[i]);
                    glVertex3d(xyz[i]);
                }
            }
            glEnd();
        }
    });
    g_ssao.shader_pass1.set_uniform_1i("u_showGrid", 0);
    // draw sheet boundary loops
    for (size_t i = 0; i < g_sheets.size(); ++i) {
        const Sheet& sheet = g_sheets[i];
        if (!sheet.isosurface.perFaceData.empty()) {
            for (int j = 0; j < 2; ++j) {
                g_ssao.shader_pass1.set_uniform_3f("u_color", g_renderOption.get_color3f(m_selected.sheet_id == 2*i+j ? "sheet_selected" : "sheet"));
                for (auto f : g_tetMesh.faces()) {
                    if (!g_tetMesh.is_boundary(f))
                        continue;
                    auto& intersections = sheet.isosurface.perFaceData[f].intersections;
                    if (intersections[0].is_valid()) {
                        array<Vector3d,2> p;
                        for (int k = 0; k < 2; ++k) {
                            p[k] = intersections[k].getPosition(g_tetMesh);
                            if (m_selected.sheet_id/2 == i) {
                                p[k] += (j==0 ? 1.0 : -1.0) * 0.004 * sheet.gradient(p[k]).normalized();
                            }
                        }
                        drawhelper::drawCylinder(p[0], p[1], g_renderOption.get_float("sheet.boundary.r") * (m_selected.sheet_id != -1 && m_selected.sheet_id/2 == i ? 2 : 1));
                    }
                }
                if (m_selected.sheet_id == -1 || m_selected.sheet_id/2 != i)
                    break;
            }
        }
    }
}

void dsm::app::State_EditHexLayout::render() {
    // draw silhouettes
    if (g_renderOption.get_bool("show_silhouette")) {
        g_shader.silhouette.enable();
        g_shader.silhouette.set_uniform_3<Vector3f>("u_eye", g_camera.get_eye().cast<float>());
        g_shader.silhouette.set_uniform_3<Vector3f>("u_color", Vector3f(0,0,0));
        g_dispList.triMesh_silhouette.render([&] () {
            glLineWidth(1);
            glBegin(GL_LINES);
            for (uint32_t f0 = 0; f0 < g_triMesh.F.cols(); ++f0) {
                for (uint32_t i = 0; i < 3; ++i) {
                    uint32_t v0 = g_triMesh.F(i, f0);
                    uint32_t v1 = g_triMesh.F((i+1)%3, f0);
                    if (v0 < v1) {
                        uint32_t f1 = g_triMesh.E2E[3 * f0 + i] / 3;
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_vertex_0", g_triMesh.V.col(v0));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_vertex_1", g_triMesh.V.col(v1));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_normal_0", g_triMesh.Nf.col(f0));
                        g_shader.silhouette.set_attrib_3<Vector3f>("a_normal_1", g_triMesh.Nf.col(f1));
                        glVertex3f(Vector3f(g_triMesh.V.col(v0)));
                        glVertex3f(Vector3f(g_triMesh.V.col(v1)));
                    }
                }
            }
            glEnd();
        });
        g_shader.silhouette.disable();
    }
    // draw creases
    if (g_renderOption.get_bool("show_crease")) {
        g_dispList.triMesh_crease.render([&] () {
            glLineWidth(1);
            glBegin(GL_LINES);
            glColor3f(0, 0, 0);
            for (uint32_t f = 0; f < g_triMesh.F.cols(); ++f) {
                for (uint32_t i = 0; i < 3; ++i) {
                    const uint32_t e = 3 * f  + i;
                    if (g_triMesh.crease_edges.count(e)) {
                        glVertex3f(Vector3f(g_triMesh.V.col(g_triMesh.F(i, f))));
                        glVertex3f(Vector3f(g_triMesh.V.col(g_triMesh.F((i+1)%3, f))));
                    }
                }
            }
            glEnd();
        });
    }
}

void dsm::app::State_EditHexLayout::mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
    static const double SELECT_EPSILON = 0.05;
    if (g_renderOption.get_bool("show_surface")) {
        if (!g_eventData.mouse_on_triMesh.is_valid()) {
            // when clicking empty space, clear selection
            m_selected.init();
            return;
        }
        // select clicked sheet
        m_selected.init();
        MaxMinSelector<int> clicked_sheet;
        for (int i = 0; i < g_sheets.size(); ++i) {
            const Sheet& sheet = g_sheets[i];
            if (!sheet.isosurface.perEdgeData.empty()) {
                for (auto e : g_tetMesh.edges()) {
                    if (g_tetMesh.is_boundary(e)) {
                        const TetMeshPoint& intersection = sheet.isosurface.perEdgeData[e].intersection;
                        if (intersection.is_valid())
                            clicked_sheet.update((*g_eventData.mouse_3D - intersection.getPosition(g_tetMesh).cast<double>()).norm(), i);
                    }
                }
            }
        }
        if (clicked_sheet.min_score < SELECT_EPSILON) {
            m_selected.sheet_id = 2*clicked_sheet.min_value + (g_sheets[clicked_sheet.min_value].func(*g_eventData.mouse_3D) < 0);
        }
    } else {
        if (!g_eventData.mouse_3D) {
            // when clicking empty space, clear selection
            m_selected.init();
            return;
        }
        // find clicked sheet
        MaxMinSelector<int> clicked_sheet;
        for (int i = 0; i < g_sheets.size(); ++i) {
            const Sheet& sheet = g_sheets[i];
            if (!sheet.isosurface.perEdgeData.empty()) {
                for (auto e : g_tetMesh.edges()) {
                    const TetMeshPoint& intersection = sheet.isosurface.perEdgeData[e].intersection;
                    if (intersection.is_valid())
                        clicked_sheet.update((*g_eventData.mouse_3D - intersection.getPosition(g_tetMesh).cast<double>()).norm(), i);
                }
            }
        }
        m_selected.sheet_id = 2*clicked_sheet.min_value + (g_sheets[clicked_sheet.min_value].gradient(*g_eventData.mouse_3D).dot(g_camera.eye - *g_eventData.mouse_3D) < 0);
    }
}

void dsm::app::State_EditHexLayout::mouseUp(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
}

void dsm::app::State_EditHexLayout::mouseMove(int mouse_x, int mouse_y) {
}

void dsm::app::State_EditHexLayout::keyboard(int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
}
