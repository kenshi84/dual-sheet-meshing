#pragma once
#include "../app.h"

namespace dsm { namespace app {

struct State_EditFeatureGraph : public State {
    void init();

    const char* name() const override { return "EditFeatureGraph"; }
    void enter() override;
    void render_ssao() override;
    void render() override;
    void mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) override;

    struct MementoData {
        std::vector<int> vertex_constraint;
        std::vector<int> edge_constraint;
    };
    kt84::Memento<MementoData> m_memento;
};

extern State_EditFeatureGraph g_state_EditFeatureGraph;

} }
