#pragma once
#include "../app.h"

namespace dsm {
namespace app {
namespace drawhelper {

void drawSphere(const Eigen::Vector3d& point, double radius);
void drawArrow(const Eigen::Vector3d& point, const Eigen::Vector3d& dir, double bar_radius, double bar_height, double tip_radius, double tip_height);
void drawCylinder(const Eigen::Vector3d& point_from, const Eigen::Vector3d& point_to, double radius);

}}}
