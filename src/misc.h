#pragma once
#include "typedefs.h"

namespace dsm { namespace misc {

bool exportVTK(const HexMesh& hexMesh, const std::string& filename);
template <class TetMeshT>
void initTetMesh(TetMeshT& tetMesh, const Eigen::MatrixXd& tetgen_V, const Eigen::MatrixXi& tetgen_C);
template <class MeshT>
void initOpenVolumeMesh(MeshT& mesh, const Eigen::MatrixXd& V, const Eigen::MatrixXi& E, const Eigen::MatrixXi& F, const Eigen::MatrixXi& C);
Isosurface marchingTetra(const TetMesh& mesh, const std::function<double(VHandle)>& func, double isovalue);
template <class TetMeshT>
void buildBVH(TetMeshT& mesh, int numLeafCells);
template <class TetMeshT>
TetMeshPoint searchBVH(const TetMeshT& mesh, const Eigen::Vector3d& pos);

} }
