#pragma once
#include "typedefs.h"

namespace dsm { namespace helper {

template <class Map, class Key>
inline auto queryMap(Map&& map, Key&& key) {
    auto found = map.find(key);
    if (found != map.end())
        return found->second;
    std::cout << "warning: map accessed with nonexistent key!\n";
    return decltype(found->second){};
}
boost::optional<Eigen::Vector4d> compute3SheetsIntersectionWithinTet
    // input: values of 3 functions f, g, h at tet vertices
    // output: barycentric coordinates t such that f.dot(t) = g.dot(t) = h.dot(t) = 0
    (const Eigen::Vector4d& f, const Eigen::Vector4d& g, const Eigen::Vector4d& h);
boost::optional<Eigen::Vector3d> compute2SheetsIntersectionWithinTriangle
    // input: values of 2 functions f, g at triangle vertices
    // output: barycentric coordinates t such that f.dot(t) = g.dot(t) = 0
    (const Eigen::Vector3d& f, const Eigen::Vector3d& g);
boost::optional<std::array<Eigen::Vector4d,2>> compute2SheetsIntersectionWithinTet
    // input: values of 2 functions f & g at tet vertices
    // output: barycentric coordinates t[0] & t[1] such that f.dot(t[i]) = g.dot(t[i]) = 0 where
    //         each t[i] has one component being zero and other components being positive
    (const Eigen::Vector4d& f, const Eigen::Vector4d& g);
Eigen::Vector4d funcValAtTet(const TetMesh& tetMesh, const Sheet& sheet, CHandle tet);
Eigen::Vector3d gradientAtTet(const TetMesh& tetMesh, const Eigen::Vector4d& cornerVal, CHandle tet);
inline Eigen::Vector3d funcGradAtTet(const TetMesh& tetMesh, const Sheet& sheet, CHandle tet) {
    return gradientAtTet(tetMesh, funcValAtTet(tetMesh, sheet, tet), tet);
};
Eigen::Vector3d funcValAtTriangle(const TetMesh& tetMesh, const Sheet& sheet, FHandle tri); // the ordering of corners is according to that of tetMesh.halfface_handle(tri,0)
std::vector<int> parseIntersectingSheets(const ElemCode& code);
ElemCode getVertexCode(const std::vector<Sheet>& sheets, VHandle tet_v);
ElemCode getVertexCode(const std::vector<Sheet>& sheets, const Eigen::Vector3d& point);
ElemCode getEdgeCode(const std::array<ElemCode,2>& v_code);
ElemCode getEdgeCode(const std::vector<Sheet>& sheets, const Eigen::Vector3d& point, int skip_sheet); // assumes the point is on the surface of skip_sheet; 0 is assigned to this index
ElemCode getFaceCode(const std::array<ElemCode,4>& v_code);
inline double acos(double d) { return std::acos(std::min(std::max(d,-1.0),1.0)); }

bool write_blobs_to_file(const std::string& filename, const std::vector<std::string>& blobs);
bool read_blobs_from_file(const std::string& filename, std::vector<std::string>& blobs);

} }
