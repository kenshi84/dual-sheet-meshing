#pragma once
#include "typedefs.h"

namespace dsm { namespace core {

void computeHexLayoutTopology_step1_dualize(HexLayout& layout, const TetMesh& tetMesh, const std::vector<Sheet>& sheets, int rand_seed = -1);
void computeHexLayoutTopology_step1a_init_numSubdiv(const HexLayout& layout, const TetMesh& tetMesh, std::vector<Sheet>& sheets, double targetEdgeLength);
void computeHexLayoutTopology_step2_check_featureGraph(HexLayout& layout, TriMesh& triMesh, const std::vector<Sheet>& sheets);
void computeHexLayoutGeometry_step1_tetrahedralize(HexLayout& layout, RefinedTetMesh& refTetMesh, const TriMesh& triMesh, const TetMesh& tetMesh, const std::vector<Sheet>& sheets, const kt84::tetgen_util::Switches& tetgen_switches);
void computeHexLayoutGeometry_step2_parameterize_dualFace(HexLayout& layout, const RefinedTetMesh& refTetMesh);
void computeHexLayoutGeometry_step3_parameterize_dualCell(HexLayout& layout, const RefinedTetMesh& refTetMesh);
void computeHexMesh(HexMesh& hexMesh, const HexLayout& layout, const RefinedTetMesh& refTetMesh, const TetMesh& tetMesh, const std::vector<Sheet>& sheets);

} }
