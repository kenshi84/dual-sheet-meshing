#include "helper.h"
#include "misc.h"
#include <kt84/MaxMinSelector.hh>
using namespace std;
using namespace Eigen;
using namespace kt84;

boost::optional<Vector4d> dsm::helper::compute3SheetsIntersectionWithinTet (const Vector4d& f, const Vector4d& g, const Vector4d& h) {
    Matrix4d A;
    A << f.transpose(),
         g.transpose(),
         h.transpose(),
         RowVector4d::Constant(1);
    Matrix4d Ainv;
    bool invertible;
    A.computeInverseWithCheck(Ainv, invertible);
    if (!invertible)
        return {};
    Vector4d t = Ainv * Vector4d{0,0,0,1};
    if (t.minCoeff()<0 || 1<t.maxCoeff())
        return {};
    return t;
}
boost::optional<Vector3d> dsm::helper::compute2SheetsIntersectionWithinTriangle(const Vector3d& f, const Vector3d& g) {
    Matrix3d A;
    A << f.transpose(),
         g.transpose(),
         RowVector3d::Constant(1);
    Matrix3d Ainv;
    bool invertible;
    A.computeInverseWithCheck(Ainv, invertible);
    if (!invertible)
        return {};
    Vector3d t = Ainv * Vector3d{0,0,1};
    if (t.minCoeff()<0 || 1<t.maxCoeff())
        return {};
    return t;
}
boost::optional<array<Vector4d,2>> dsm::helper::compute2SheetsIntersectionWithinTet (const Vector4d& f, const Vector4d& g) {
    int cnt=0;
    array<Vector4d,2> result;
    for (int i=0; i<4; ++i) {
        Matrix4d A = Matrix4d::Zero();
        A.topRows(3) <<
            f.transpose(),
            g.transpose(),
            RowVector4d::Constant(1);
        A(3,i) = 1;
        Matrix4d Ainv;
        bool invertible;
        A.computeInverseWithCheck(Ainv, invertible);
        if (!invertible)
            continue;
        Vector4d t = Ainv * Vector4d{0,0,1,0};
        if (0<=t.minCoeff() && t.maxCoeff()<=1) {
            result[cnt] = t;
            ++cnt;
        }
    }
    if (cnt==2)
        return result;
    return boost::none;
}
Vector4d dsm::helper::funcValAtTet(const TetMesh& tetMesh, const Sheet& sheet, CHandle tet) {
    Vector4d result;
    int i=0;
    for (auto v : tetMesh.cell_vertices(tet))
        result[i++] = sheet.isosurface.perVertexData[v].funcVal;
    return result;
}
Vector3d dsm::helper::gradientAtTet(const TetMesh& tetMesh, const Vector4d& cornerVal, CHandle tet) {
    Matrix<double,3,4> x;
    Vector4d y;
    int i=0;
    for (auto v : tetMesh.cell_vertices(tet)) {
        x.col(i) = vector_cast<3,Vector3d>(tetMesh.vertex(v));
        y[i] = cornerVal[i];
        ++i;
    }
    return eigen_util::compute_gradient(x,y);
}
Vector3d dsm::helper::funcValAtTriangle(const TetMesh& tetMesh, const Sheet& sheet, FHandle tri) {
    Vector3d result;
    int i=0;
    for (auto v : tetMesh.face_vertices(tri))
        result[i++] = sheet.isosurface.perVertexData[v].funcVal;
    return result;
}
vector<int> dsm::helper::parseIntersectingSheets(const ElemCode& code) {
    vector<int> result;
    for (int i=0; i<code.str.size(); ++i)
        if (code.str[i]=='0')
            result.push_back(i);
    return result;
}
dsm::ElemCode dsm::helper::getVertexCode(const vector<Sheet>& sheets, VHandle tet_v) {
    ElemCode v_code;
    v_code.str.reserve(sheets.size());
    for (auto& sheet : sheets)
        v_code.str.push_back(sheet.isosurface.perVertexData[tet_v].funcVal>0 ? '+' : '-');
    return v_code;
}
dsm::ElemCode dsm::helper::getVertexCode(const vector<Sheet>& sheets, const Vector3d& point) { 
    ElemCode v_code;
    v_code.str.reserve(sheets.size());
    for (auto& sheet : sheets)
        v_code.str.push_back(sheet.func(point)>0 ? '+' : '-');
    return v_code;
}
dsm::ElemCode dsm::helper::getEdgeCode(const array<ElemCode,2>& v_code) {
    ElemCode e_code = v_code[0];
    int cnt = 0;
    for (int i=0; i<e_code.str.size(); ++i) {
        if (v_code[0].str[i]==v_code[1].str[i])
            continue;
        if (v_code[0].str[i]=='*' || v_code[1].str[i]=='*') {
            e_code.str[i] = '*';
            continue;
        }
        e_code.str[i] = '0';
        ++cnt;
    }
    if (cnt!=1)
        return {};
    return e_code;
}
dsm::ElemCode dsm::helper::getEdgeCode(const vector<Sheet>& sheets, const Vector3d& point, int skip_sheet) {
    ElemCode e_code;
    e_code.str.resize(sheets.size());
    for (int i = 0; i < sheets.size(); ++i)
        e_code.str[i] = i==skip_sheet ? '0' : sheets[i].func(point)>0 ? '+' : '-';
    return e_code;
}
dsm::ElemCode dsm::helper::getFaceCode(const array<ElemCode,4>& v_code) {
    const size_t sz = v_code[0].str.size();
    if (sz!=v_code[1].str.size() ||
        sz!=v_code[2].str.size() ||
        sz!=v_code[3].str.size())
    {
        cout << "[helper::getFaceCode] warning: vertex code sizes don't match\n";
        return {};
    }
    ElemCode f_code;
    int cnt = 0;
    for (size_t i=0; i<sz; ++i) {
        char c = '*';
        for (int j=0; j<4; ++j) {
            if (v_code[j].str[i]=='*')
                continue;
            if (c=='*') {
                c = v_code[j].str[i];
            } else if (c!=v_code[j].str[i]) {
                c = '0';
                break;
            }
        }
        f_code.str.push_back(c);
        if (c=='0')
            ++cnt;
    }
    if (cnt!=2)
        return {};
    return f_code;
}

bool dsm::helper::write_blobs_to_file(const string& filename, const vector<string>& blobs) {
    ofstream ofs(filename.c_str(), ios::binary);
    if (!ofs.is_open()) {
        cerr << "failed to open file: " << filename;
        return false;
    }
    const uint64_t num_blobs = blobs.size();
    ofs.write((const char*)&num_blobs, sizeof(uint64_t));
    for (const string& blob : blobs) {
        const uint64_t sz = blob.size();
        ofs.write((const char*)&sz, sizeof(uint64_t));
        ofs.write(&blob[0], blob.size());
    }
    return true;
}

bool dsm::helper::read_blobs_from_file(const string& filename, vector<string>& blobs) {
    blobs.clear();
    ifstream ifs(filename.c_str(), ios::binary);
    if (!ifs.is_open()) {
        cerr << "failed to open file: " << filename;
        return false;
    }
    uint64_t num_blobs;
    ifs.read((char*)&num_blobs, sizeof(uint64_t));
    if (ifs.gcount() != sizeof(uint64_t)) {
        cerr << "failed to read the number of blobs; read bytes: " << ifs.gcount() << endl;
        return false;
    }
    for (uint64_t i = 0; i < num_blobs; ++i) {
        uint64_t sz;
        ifs.read((char*)&sz, sizeof(uint64_t));
        if (ifs.gcount() != sizeof(uint64_t)) {
            cerr << "failed to read the blob size; read bytes: " << ifs.gcount() << endl;
            blobs.clear();
            return false;
        }
        blobs.resize(blobs.size() + 1);
        blobs.back().resize(sz);
        ifs.read(&blobs.back()[0], sz);
        if (ifs.gcount() != sz) {
            cerr << "failed to read expected number of bytes for a blob: expected=" << sz << ", read=" << ifs.gcount() << endl;
            blobs.clear();
            return false;
        }
    }
    return true;
}
