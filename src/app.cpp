#include "app.h"
#include "app/State_EditFeatureGraph.h"
#include "app/State_EditHexLayout.h"
#include "app/State_EditHexMesh.h"
#include "app/State_EditSheet.h"
#include "app/shader_source.h"
#include "core.h"
#include "helper.h"
#include "misc.h"
using namespace std;
using namespace Eigen;
using namespace kt84;

bool dsm::app::g_glew_initialized = false;
dsm::app::State*                dsm::app::g_state;
kt84::CameraFree                dsm::app::g_camera;
std::string                     dsm::app::g_filename_dat;
kt84::glfw_util::LoopControl    dsm::app::g_loopControl;
dsm::app::Config                dsm::app::g_config;
dsm::app::SerializedBlob        dsm::app::g_serializedBlob;
dsm::app::Shader                dsm::app::g_shader;
dsm::app::DisplayList           dsm::app::g_dispList;
dsm::app::EventData             dsm::app::g_eventData;
dsm::app::RenderOption          dsm::app::g_renderOption;
dsm::app::SSAO                  dsm::app::g_ssao;

dsm::TriMesh                    dsm::app::g_triMesh;
dsm::TetMesh                    dsm::app::g_tetMesh;
std::vector<dsm::Sheet>         dsm::app::g_sheets;
dsm::HexLayout                  dsm::app::g_hexLayout;
dsm::RefinedTetMesh             dsm::app::g_refinedTetMesh;
dsm::HexMesh                    dsm::app::g_hexMesh;

dsm::app::Config::Config() : undo_buffer_size(50), creaseAngle(45) {
    tetgen_switches[0].p = true;
    tetgen_switches[0].q = true;
    tetgen_switches[0].q_ratio = 3.0;
    tetgen_switches[0].a_fixed = true;
    tetgen_switches[0].a_fixed_value = 0.001;
    tetgen_switches[1].p = true;
    tetgen_switches[1].q = true;
    tetgen_switches[1].a_fixed = true;
    tetgen_switches[1].a_fixed_value = 0.1;
    tetgen_switches[1].A = true;
}
dsm::app::Config::AutoSave::AutoSave() : enabled(true), interval(30), unsaved(true), last_time(time(nullptr)) {}

void dsm::app::RenderOption::init() {
    opt_bool["hide_other_sheets"] = false;
    opt_bool["show.featuregraph"] = true;
    opt_bool["show_aabb"] = true;
    opt_bool["show_crease"] = true;
    opt_bool["show_silhouette"] = true;
    opt_bool["show_surface"] = true;
    opt_bool["show_surface_edges"] = false;
    opt_bool["turntable_mode"] = false;
    opt_bool["use_ortho"] = false;
    opt_int["ssao.blurRadius"] = 2;
    opt_int["ssao.kernelSize"] = 64;
    opt_float["canvas.r"] = 0.7;
    opt_float["featuregraph.arc.r"] = 0.003;
    opt_float["featuregraph.node.r"] = 0.006;
    opt_float["hexMesh.planeDist"] = 0;
    opt_float["hexMesh.shrink"] = 0.8;
    opt_float["phong.k_ambient"] = 0.6;
    opt_float["phong.k_diffuse"] = 0.2;
    opt_float["phong.k_specular"] = 0.1;
    opt_float["phong.shininess"] = 2;
    opt_float["resampleInterval"] = 0.24;
    opt_float["sheet.boundary.r"] = 0.003;
    opt_float["ssao.bias"] = 0;
    opt_float["ssao.sampleRadius"] = 1;
    opt_float["turntable.speed"] = 1;
    opt_dir3f["hexMesh.planeNormal"] = { 0.577, 0.577, 0.577 };
    opt_dir3f["phong.lightDir"] = { 0, 0, -1 };
    opt_color3f["bgcolor_bottom"] = { 0.8, 0.8, 0.8 };
    opt_color3f["bgcolor_top"] = { 0.4, 0.4, 0.4 };
    opt_color3f["canvas"] = { 0.8, 0.8, 0.8 };
    opt_color3f["canvas_rotating"] = { 0.9176, 0.8784, 0.3373 };
    opt_color3f["constraint"] = { 0.5333, 0.1647, 0.09412 };
    opt_color3f["constraint_selected"] = { 1, 0.01961, 0.01961 };
    opt_color3f["featuregraph.arc"] = { 0.8863, 0.8118, 0 };
    opt_color3f["featuregraph.node"] = { 0.698, 0.2078, 1 };
    opt_color3f["layout"] = { 0.102, 0.5922, 0.8392 };
    opt_color3f["layout_selected"] = { 1, 0.01961, 0.01961 };
    opt_color3f["model"] = { 0, 0.85, 0.73 };
    opt_color3f["sheet"] = { 0.102, 0.5922, 0.8392 };
    opt_color3f["sheet_selected"] = { 0.9098, 0.3608, 0.2667 };
    opt_color3f["stroke_cylinder"] = { 0.7, 0.4, 0.3 };
    opt_color3f["stroke_sketch2d"] = { 0.7, 0.3, 0.8 };
    opt_color3f["surface_edges"] = { 0.2941, 0.2941, 0.2941 };
}

void dsm::app::RenderOption::addToBar(TwBar* bar) {
    static auto iter_bool = opt_bool.begin();
    tw_util::AddVarCB<int>(bar, "ro_bool_index",
        [&](const int& value){
            iter_bool = opt_bool.begin();
            std::advance(iter_bool, util::clamp<int>(value, 0, opt_bool.size() - 1));
        }, [&](int& value) {
            value = std::distance(opt_bool.begin(), iter_bool);
        }, "group=RenderOption::Bool label=Index");
    tw_util::AddVarCB<std::string>(bar, "ro_bool_name", {/* read-only */},
        [&](std::string& value) {
            value = iter_bool->first;
        }, "group=RenderOption::Bool label=Name");
    tw_util::AddVarCB<bool>(bar, "ro_bool_value",
        [&](const bool& value) {
            iter_bool->second = value;
        }, [&](bool& value) {
            value = iter_bool->second;
        }, "group=RenderOption::Bool label=Value");
    tw_util::set_var_group(bar, "RenderOption::Bool", "RenderOption");
    tw_util::set_var_opened(bar, "RenderOption::Bool", false);

    static auto iter_int = opt_int.begin();
    tw_util::AddVarCB<int>(bar, "ro_int_index",
        [&](const int& value){
            iter_int = opt_int.begin();
            std::advance(iter_int, util::clamp<int>(value, 0, opt_int.size() - 1));
        }, [&](int& value) {
            value = std::distance(opt_int.begin(), iter_int);
        }, "group=RenderOption::Int label=Index");
    tw_util::AddVarCB<std::string>(bar, "ro_int_name", {/* read-only */},
        [&](std::string& value) {
            value = iter_int->first;
        }, "group=RenderOption::Int label=Name");
    tw_util::AddVarCB<int>(bar, "ro_int_value",
        [&](const int& value) {
            iter_int->second = value;
        }, [&](int& value) {
            value = iter_int->second;
        }, "group=RenderOption::Int label=Value");
    tw_util::set_var_group(bar, "RenderOption::Int", "RenderOption");
    tw_util::set_var_opened(bar, "RenderOption::Int", false);

    static auto iter_float = opt_float.begin();
    tw_util::AddVarCB<int>(bar, "ro_float_index",
        [&](const int& value){
            iter_float = opt_float.begin();
            std::advance(iter_float, util::clamp<int>(value, 0, opt_float.size() - 1));
        }, [&](int& value) {
            value = std::distance(opt_float.begin(), iter_float);
        }, "group=RenderOption::Float label=Index");
    tw_util::AddVarCB<std::string>(bar, "ro_float_name", {/* read-only */},
        [&](std::string& value) {
            value = iter_float->first;
        }, "group=RenderOption::Float label=Name");
    tw_util::AddVarCB<float>(bar, "ro_float_value",
        [&](const float& value) {
            iter_float->second = value;
        }, [&](float& value) {
            value = iter_float->second;
        }, "group=RenderOption::Float label=Value step=0.001");
    tw_util::set_var_group(bar, "RenderOption::Float", "RenderOption");
    tw_util::set_var_opened(bar, "RenderOption::Float", false);

    static auto iter_dir3f = opt_dir3f.begin();
    tw_util::AddVarCB<int>(bar, "ro_dir3f_index",
        [&](const int& value){
            iter_dir3f = opt_dir3f.begin();
            std::advance(iter_dir3f, util::clamp<int>(value, 0, opt_dir3f.size() - 1));
        }, [&](int& value) {
            value = std::distance(opt_dir3f.begin(), iter_dir3f);
        }, "group=RenderOption::Dir3f label=Index");
    tw_util::AddVarCB<std::string>(bar, "ro_dir3f_name", {/* read-only */},
        [&](std::string& value) {
            value = iter_dir3f->first;
        }, "group=RenderOption::Dir3f label=Name");
    tw_util::AddVarCB<tw_util::Dir3f>(bar, "ro_dir3f_value",
        [&](const tw_util::Dir3f& value) {
            iter_dir3f->second = (Vector3f&)value;
        }, [&](tw_util::Dir3f& value) {
            (Vector3f&)value = iter_dir3f->second;
        }, "group=RenderOption::Dir3f label=Value opened=1");
    tw_util::set_var_group(bar, "RenderOption::Dir3f", "RenderOption");
    tw_util::set_var_opened(bar, "RenderOption::Dir3f", false);

    static auto iter_color3f = opt_color3f.begin();
    tw_util::AddVarCB<int>(bar, "ro_color3f_index",
        [&](const int& value){
            iter_color3f = opt_color3f.begin();
            std::advance(iter_color3f, util::clamp<int>(value, 0, opt_color3f.size() - 1));
        }, [&](int& value) {
            value = std::distance(opt_color3f.begin(), iter_color3f);
        }, "group=RenderOption::Color3f label=Index");
    tw_util::AddVarCB<std::string>(bar, "ro_color3f_name", {/* read-only */},
        [&](std::string& value) {
            value = iter_color3f->first;
        }, "group=RenderOption::Color3f label=Name");
    tw_util::AddVarCB<tw_util::Color3f>(bar, "ro_color3f_value",
        [&](const tw_util::Color3f& value) {
            iter_color3f->second = (Vector3f&)value;
        }, [&](tw_util::Color3f& value) {
            (Vector3f&)value = iter_color3f->second;
        }, "group=RenderOption::Color3f label=Value colormode=hls opened=1");
    tw_util::set_var_group(bar, "RenderOption::Color3f", "RenderOption");
    tw_util::set_var_opened(bar, "RenderOption::Color3f", false);

    tw_util::AddButton(bar, "ro_print", [&]() { print(); }, "group=RenderOption label=Print");
    tw_util::set_var_opened(bar, "RenderOption", false);
}

void dsm::app::RenderOption::print() {
    cout << "void dsm::app::RenderOption::init() {\n";
    for (auto i : opt_bool)
        cout << "    opt_bool[\"" << i.first << "\"] = " << (i.second ? "true" : "false") << ";\n";
    for (auto i : opt_int)
        cout << "    opt_int[\"" << i.first << "\"] = " << i.second << ";\n";
    for (auto i : opt_float)
        cout << "    opt_float[\"" << i.first << "\"] = " << i.second << ";\n";
    for (auto i : opt_dir3f)
        cout << "    opt_dir3f[\"" << i.first << "\"] = { " << i.second[0] << ", " << i.second[1] << ", " << i.second[2] << " };\n";
    for (auto i : opt_color3f)
        cout << "    opt_color3f[\"" << i.first << "\"] = { " << i.second[0] << ", " << i.second[1] << ", " << i.second[2] << " };\n";
    cout << "}\n";
}

void dsm::app::init() {
    g_renderOption.init();

    TwDefine("GLOBAL fontsize=2");
    g_state_EditFeatureGraph.init();
    g_state_EditHexLayout.init();
    g_state_EditHexMesh.init();
    g_state_EditSheet.init();

    g_state = &g_state_EditSheet;
    changeState(g_state_EditSheet);

    // OpenGL
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(1.0, 1.0);
    // color material (diffuse)
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE); 
    // blend enabled
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // anti-aliasing
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POINT_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);    
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
    // shader
    g_shader.silhouette.init_compile_attach_link(shader_source::silhouette::FRAG, shader_source::silhouette::VERT);

    g_ssao.init(g_camera.width, g_camera.height);

    g_camera.auto_flip_y = false;

    fill(g_eventData.key_pressed, g_eventData.key_pressed + 256, false);
}

void dsm::app::changeState(State& next_state) {
    tw_util::set_bar_visible(g_state->m_bar, false);
    g_state = &next_state;
    g_state->enter();
    tw_util::set_bar_visible(g_state->m_bar, true);
}

bool dsm::app::importTriMesh(const std::string& filename) {
    if (!g_triMesh.load(filename, g_config.creaseAngle)) {
        tinyfd_util::messageBox(APPNAME " - Import triangle mesh", "Error occurred!", "ok", "error", 1);
        return false;
    }

    g_triMesh.tetrahedralize(g_tetMesh, g_config.tetgen_switches[0]);

    g_triMesh.init_featuregraph_dihedral_angle(40);

    g_serializedBlob = {};
    g_sheets = {};
    g_hexLayout = {};
    g_hexMesh = {};
    changeState(g_state_EditSheet);

    const AABB& aabb = g_triMesh.stats.mAABB;
    g_camera.init(aabb.center().cast<double>() + Vector3d(0, 0, aabb.diagonal().norm() * 1.5), aabb.center().cast<double>(), Vector3d::UnitY());
    g_dispList.triMesh_silhouette.invalidate();
    g_dispList.triMesh_crease.invalidate();
    g_dispList.triMesh_surface.invalidate();
    g_dispList.triMesh_surface_edges.invalidate();

    saveBinary(filename.substr(0, filename.size() - 4) + ".dat");
    return true;
}

bool dsm::app::exportHexMesh(const std::string& filename) {
    return true;
}

bool dsm::app::saveBinary(const std::string& filename) {
    assert(g_tetMesh.n_vertices() > 0);

    const std::string filename_temp = filename.empty() ? g_filename_dat : filename;

    if (!g_serializedBlob.save(filename_temp))
        return false;

    g_filename_dat = filename_temp;
    g_config.autoSave.unsaved = false;
    return true;
}

bool dsm::app::loadBinary(const std::string& filename) {
    if (!g_serializedBlob.load(filename))
        return false;

    const AABB& aabb = g_triMesh.stats.mAABB;
    g_camera.init(aabb.center().cast<double>() + Vector3d(0, 0, aabb.diagonal().norm() * 1.5), aabb.center().cast<double>(), Vector3d::UnitY());
    g_dispList.triMesh_silhouette.invalidate();
    g_dispList.triMesh_crease.invalidate();
    g_dispList.triMesh_surface.invalidate();

    g_filename_dat = filename;
    g_config.autoSave.unsaved = false;
    return true;
}

bool dsm::app::common_mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
    g_eventData.button_left_pressed = button == Button::LEFT;
    g_eventData.button_right_pressed = button == Button::RIGHT;
    g_eventData.button_middle_pressed = button == Button::MIDDLE;
    g_eventData.shift_pressed = shift_pressed;
    g_eventData.ctrl_pressed = ctrl_pressed;
    g_eventData.alt_pressed = alt_pressed;
    if (alt_pressed) {
        Camera::DragMode dragMode =
            button == LEFT ? (ctrl_pressed ? Camera::DragMode::PAN : Camera::DragMode::ROTATE) :
            button == RIGHT ? Camera::DragMode::ZOOM : Camera::DragMode::PAN;
        g_camera.mouse_down(mouse_x, mouse_y, dragMode);
        return true;
    }
    return false;
}

bool dsm::app::common_mouseUp  (int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
    g_eventData.button_left_pressed = false;
    g_eventData.button_right_pressed = false;
    g_eventData.button_middle_pressed = false;
    g_eventData.shift_pressed = false;
    g_eventData.ctrl_pressed = false;
    g_eventData.alt_pressed = false;
    if (g_camera.drag_mode != Camera::DragMode::NONE) {
        if (g_camera.drag_mode == Camera::DragMode::ROTATE && shift_pressed)
            g_camera.snap_to_canonical();
        g_camera.mouse_up();
        return true;
    }
    return false;
}

bool dsm::app::common_mouseMove(int mouse_x, int mouse_y) {
    g_eventData.mouse_2D << mouse_x, mouse_y;
    g_eventData.mouse_3Dz1 = graphics_util::unproject(Vector3d(mouse_x, mouse_y, 1));
    g_eventData.mouse_3Dz0 = graphics_util::unproject(Vector3d(mouse_x, mouse_y, 0));
    g_eventData.mouse_on_triMesh = g_triMesh.rayIntersect(::Ray{g_eventData.mouse_3Dz0.cast<float>(), (g_eventData.mouse_3Dz1 - g_eventData.mouse_3Dz0).normalized().cast<float>()});
    float z = graphics_util::read_depth(mouse_x, mouse_y);
    if (z == 1.0f) {
        g_eventData.mouse_3D = boost::none;
    } else {
        g_eventData.mouse_3D = graphics_util::unproject(Vector3d(mouse_x, mouse_y, z));
    }

    if (g_camera.drag_mode != Camera::DragMode::NONE) {
        g_camera.mouse_move(mouse_x, mouse_y);
        if (g_state == &g_state_EditSheet && g_state_EditSheet.m_canvasRotating) {
            Sheet& sheet = *g_state_EditSheet.m_selected.sheet;
            sheet.canvas.rotation.col(1) = g_camera.up;
            sheet.canvas.rotation.col(2) = g_camera.center_to_eye().normalized();
            sheet.canvas.rotation.col(0) = sheet.canvas.rotation.col(1).cross(sheet.canvas.rotation.col(2));
            sheet.canvas.translation = g_camera.eye + 2 * g_camera.eye_to_center().normalized();
            sheet.update(g_tetMesh);
        }
        return true;
    }

    return false;
}

bool dsm::app::common_keyboard  (int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
    g_eventData.key_pressed[key] = true;
    if (ctrl_pressed && key == 'S') {
        if (!g_filename_dat.empty() && g_config.autoSave.unsaved)
            saveBinary();
        return true;
    }
    if (key==GLFW_KEY_ESCAPE) {
        ostringstream oss;
        oss << g_camera.center.transpose() << " ";
        oss << g_camera.eye.transpose() << " ";
        oss << g_camera.up.transpose();
        auto ret = tinyfd_util::inputBox(APPNAME, "Get/set camera parameter", oss.str());
        if (ret) {
            istringstream iss(*ret);
            iss >>
                g_camera.center[0] >> g_camera.center[1] >> g_camera.center[2] >> 
                g_camera.eye[0] >> g_camera.eye[1] >> g_camera.eye[2] >>
                g_camera.up[0] >> g_camera.up[1] >> g_camera.up[2];
        }
        return true;
    }
    if (ctrl_pressed && key == 'T') {
        util::flip_bool(g_renderOption.get_bool("turntable_mode"));
        g_loopControl.poll_events = g_renderOption.get_bool("turntable_mode");
        if (g_loopControl.poll_events) {
            g_loopControl.idle_func[1] = [&]() {
                const Vector3d dx = g_camera.center_to_eye();
                const Vector3d dy = g_camera.up.cross(dx);
                const double theta = g_renderOption.get_float("turntable.speed") * util::pi()/180;
                g_camera.eye = g_camera.center + dx*cos(theta) + dy*sin(theta);
            };
        } else {
            g_loopControl.idle_func[1] = {};
        }
        return true;
    }
    return false;
}

bool dsm::app::common_keyboardUp(int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {
    g_eventData.key_pressed[key] = false;
    return false;
}
