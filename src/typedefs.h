#pragma once
#undef min
#undef max
#include <iostream>
#include <fstream>
#include <functional>
#include <array>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <memory>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/range/numeric.hpp>
#include <boost/functional/hash.hpp>
#include <boost/unordered_map.hpp>
#include <boost/rational.hpp>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <kt84/adjacent_pairs.hh>
#include <kt84/vector_cast.hh>
#include <kt84/vector_concat.hh>
#include <kt84/Average.hh>
#include <kt84/graphics/DisplayList.hh>
#include <kt84/geometry/PolylineT.hh>
#include <kt84/geometry/Polyline_PointNormal.hh>
#include <kt84/geometry/CameraFree.hh>
#include <kt84/openvolumemesh/base/StaticProperty.hh>
#include <kt84/openvolumemesh/base/TetrahedralMesh.hh>
#include <kt84/openvolumemesh/base/TetFaceNormal.hh>
#include <kt84/openvolumemesh/base/TetFaceArea.hh>
#include <kt84/openvolumemesh/base/HexMeshUtility.hh>
#include <kt84/math/HermiteRBF.hh>
#include <kt84/MaxMinSelector.hh>
#include <kt84/Timer.hh>
#include <kt84/KeyValuePair.hh>
#include <kt84/eigen_util.hh>
#include <kt84/tbb_util.hh>
#include <kt84/tinyfd_util.hh>
#include <kt84/container_util.hh>
#include <kt84/hash_util.hh>
#include <kt84/tetgen_util.hh>
#include "instant-meshes/hierarchy.h"
#include "instant-meshes/field.h"
#include "instant-meshes/meshstats.h"
#include "instant-meshes/bvh.h"
#include "instant-meshes/dedge.h"       // declares static const uint32_t ::INVALID

#define DEBUG_PRINT_MSG(msg, x) std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] " << (msg) << ": " << #x << " = " << (x) << std::endl
#define DEBUG_PRINT(x) std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] " << #x << " = " << (x) << std::endl
#define DEBUG_ASSERT_EQ(x, y) if (!((x) == (y))) std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] Equality assertion failed: " << #x << " = " << (x) << ", " << #y << " = " << (y) << std::endl
#define DEBUG_ASSERT(condition, x) if (!(condition)) std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] Assertion failed: " << #x << " = " << (x) << std::endl

namespace Eigen {
using SparseMatrixf = SparseMatrix<float>;
using SparseMatrixd = SparseMatrix<double>;
using Tripletf = Triplet<float>;
using Tripletd = Triplet<double>;
using Vector8i = Matrix<int,8,1>;
}

namespace dsm {

// original graph g derived from triangle mesh with extended k-ring neighbors
using VHandle = OpenVolumeMesh::VertexHandle;
using FHandle = OpenVolumeMesh::FaceHandle;
using EHandle = OpenVolumeMesh::EdgeHandle;
using CHandle = OpenVolumeMesh::CellHandle;
using HEHandle = OpenVolumeMesh::HalfEdgeHandle;
using HFHandle = OpenVolumeMesh::HalfFaceHandle;
using Vec3d = OpenVolumeMesh::Vec3d;

struct TetMeshPoint {
    VHandle vertex;
    EHandle edge;
    FHandle face;
    CHandle cell;
    Eigen::VectorXd bc;     // barycentric coordinates
    TetMeshPoint() {}
    TetMeshPoint(VHandle vertex) : vertex(vertex) {}
    TetMeshPoint(EHandle edge, const Eigen::Vector2d& bc) : edge(edge), bc(bc) {}
    TetMeshPoint(FHandle face, const Eigen::Vector3d& bc) : face(face), bc(bc) {}
    TetMeshPoint(CHandle cell, const Eigen::Vector4d& bc) : cell(cell), bc(bc) {}
    bool operator==(const TetMeshPoint& rhs) const {
        return vertex==rhs.vertex &&
               edge==rhs.edge &&
               face==rhs.face &&
               cell==rhs.cell &&
               bc==rhs.bc;
    }
    bool operator!=(const TetMeshPoint& rhs) const { return !(operator==(rhs)); }
    bool operator<(const TetMeshPoint& rhs) const {
        if (vertex.is_valid() && rhs.vertex.is_valid()) return vertex < rhs.vertex;
        if (edge  .is_valid() && rhs.edge  .is_valid()) return edge   < rhs.edge  ;
        if (face  .is_valid() && rhs.face  .is_valid()) return face   < rhs.face  ;
        if (cell  .is_valid() && rhs.cell  .is_valid()) return cell   < rhs.cell  ;
        return false;
    }
    bool is_valid() const {
        return
            vertex.is_valid() ||
            edge.is_valid() && bc.size()==2 ||
            face.is_valid() && bc.size()==3 ||
            cell.is_valid() && bc.size()==4;
    }
    template <class TMesh>
    Eigen::Vector3d getPosition(const TMesh& mesh) const {
        if (!is_valid())
            throw "invalid!";
        if (vertex.is_valid())
            return kt84::vector_cast<3,Eigen::Vector3d>(mesh.vertex(vertex));
        if (edge.is_valid()) {
            auto v = mesh.edge_vertices(edge);
            auto p0 = mesh.vertex(v[0]),
                 p1 = mesh.vertex(v[1]);
            return kt84::vector_cast<3,Eigen::Vector3d>(bc[0]*p0 + bc[1]*p1);
        }
        Vec3d result = {0,0,0};
        int i = 0;
        if (face.is_valid())
            for (auto v : mesh.face_vertices(face))
                result += bc[i++]*mesh.vertex(v);
        else
            for (auto v : mesh.cell_vertices(cell))
                result += bc[i++]*mesh.vertex(v);
        return kt84::vector_cast<3,Eigen::Vector3d>(result);
    }
    template <class TMesh>
    VHandle closestVertex(const TMesh& mesh) const {
        if (vertex.is_valid()) return vertex;
        if (edge.is_valid()) {
            auto v = mesh.halfedge_vertices(edge);
            return bc[0] > bc[1] ? v[0] : v[1];
        }
        if (face.is_valid()) {
            auto v = mesh.halfface_vertices(face);
            return bc[0] > bc[1] && bc[0] > bc[2] ? v[0] :
                   bc[1] > bc[2] ? v[1] : v[2];
        }
        if (cell.is_valid()) {
            auto v = mesh.cell_vertices(cell);
            return bc[0] > bc[1] && bc[0] > bc[2] && bc[0] > bc[3] ? v[0] :
                   bc[1] > bc[2] && bc[1] > bc[3] ? v[1] :
                   bc[2] > bc[3] ? v[2] : v[3];
        }
        throw "invalid!";
    }
};
inline std::ostream& operator<<(std::ostream& out, const TetMeshPoint& p) {
    if (p.vertex.is_valid()) return out << "vertex.idx=" << p.vertex.idx();
    if (p.edge.is_valid()) return out << "edge.idx=" << p.edge.idx();
    if (p.face.is_valid()) return out << "face.idx=" << p.face.idx();
    if (p.cell.is_valid()) return out << "cell.idx=" << p.cell.idx();
    return out << "invalid";
}

struct BVH {
    struct Node {
        std::array<std::shared_ptr<Node>,2> child;
        Eigen::AlignedBox3d bbox;
        std::vector<CHandle> cells;
    };
    Node root;
};

struct TriMesh;
struct TetMesh;

struct TriMeshPoint {
    uint32_t f;
    Eigen::Vector3f bc;
    TriMeshPoint(uint32_t f = ::INVALID, const Eigen::Vector3f& bc = Eigen::Vector3f::Zero()) : f(f), bc(bc) {}
    bool is_valid() const { return f != ::INVALID && bc != Eigen::Vector3f::Zero(); }
    template <class TriMeshT>
    Eigen::Vector3f getPosition(const TriMeshT& mesh) const {
        if (f == ::INVALID)
            return Eigen::Vector3f::Zero();
        Eigen::Vector3f p = Eigen::Vector3f::Zero();
        for (int i = 0; i < 3; ++i)
            p += bc[i] * mesh.V.col(mesh.F(i,f));
        return p;
    }
    bool is_vertex() const { return is_valid() && (bc == Eigen::Vector3f::UnitX() || bc == Eigen::Vector3f::UnitY() || bc == Eigen::Vector3f::UnitZ()); }
    template <class TriMeshT>
    uint32_t as_vertex(const TriMeshT& mesh) const {
        if (is_vertex())
            return bc == Eigen::Vector3f::UnitX() ? mesh.F(0,f) : bc == Eigen::Vector3f::UnitY() ? mesh.F(1,f) : mesh.F(2,f);
        return ::INVALID;
    }
    template <class TriMeshT>
    uint32_t closestVertex(const TriMeshT& mesh) const {
        if (!is_valid()) return ::INVALID;
        return bc[0]>bc[1] && bc[0]>bc[2] ? mesh.F(0,f) : bc[1]>bc[2] ? mesh.F(1,f) : mesh.F(2,f);
    }
    template <class TriMeshT>
    TriMeshPoint(uint32_t v, const TriMeshT& mesh) {
        assert(v < mesh.V.cols());
        f = mesh.V2E[v] / 3;
        bc.setZero();
        for (int i = 0; i < 3; ++i) {
            if (mesh.F(i,f) == v) {
                bc[i] = 1;
                return;
            }
        }
        assert(false);
    }
    template <class TriMeshT>
    TriMeshPoint(uint32_t e, float t, const TriMeshT& mesh) {
        // point on an edge: (t-t)*from_vertex + t*to_vertex
        assert(e < mesh.E2E.size());
        f = e / 3;
        bc.setZero();
        bc[e%3] = 1-t;
        bc[(e+1)%3] = t;
    }
};

using RoSyConstraint = std::pair<uint32_t, Eigen::Vector3f>;

struct TriMesh {
    // these matrices are column-based
    Eigen::MatrixXf V;      // 3D coordinate of a vertex
    ::MatrixXu F;           // vertex indices of a triangle
    ::VectorXu V2E;         // outgoing halfedge from F(e%3,e/3) to F((e+1)%3,e/3) where e=V2E[v] (assuming F(e%3,e/3)==v)
    ::VectorXu E2E;         // E2E[e] is opposite halfedge
    Eigen::MatrixXf N;      // 3D normal of a vertex
    Eigen::MatrixXf Nf;     // 3D normal of a triangle
    Eigen::VectorXf A;      // dual vertex area
    ::VectorXb boundary, nonManifold;
    std::set<uint32_t> creases;         // set of crease vertices
    std::set<uint32_t> crease_edges;    // set of crease edges (ID of i-th edge of face f is `3*f + i`)
    ::MeshStats stats;
    float creaseAngle;

    // constraints representing Feature Graph
    std::vector<std::array<uint32_t,2>> edges;  // undirected edges, assuming e[0] < e[1]
    std::vector<uint32_t> edge_to_halfedge;     // maps edge to one of two halfedges; the same size as edges
    std::vector<uint32_t> halfedge_to_edge;     // maps halfedge to edge; the same size as E2E
    std::vector<int> edge_constraint;           // constraint flag per edge, initialized with 0; a continuous sequence of constrained edges will receive a common group ID
    std::vector<int> vertex_constraint;         // constraint flag per vertex, initialized with 0
    void init_featuregraph_dihedral_angle(float angle_degree);
    void make_vertex_constrained_if_necessary();     // flag vertices as constrained if incident to N constrained edges with N!=0 and N!=2

    // utility for accessing elements
    uint32_t incident_face(uint32_t halfedge) const { return halfedge / 3; }
    uint32_t opposite_halfedge(uint32_t halfedge) const { return E2E[halfedge]; }
    uint32_t from_vertex(uint32_t halfedge) const { return F(halfedge%3, halfedge/3); }
    uint32_t to_vertex(uint32_t halfedge) const { return F((halfedge+1)%3, halfedge/3); }
    std::vector<uint32_t> outgoing_halfedges(uint32_t vertex) const {
        std::vector<uint32_t> result;
        uint32_t halfedge = V2E[vertex];
        while (true) {
            result.push_back(halfedge);
            halfedge = dedge_next_3(E2E[halfedge]);
            if (halfedge == V2E[vertex])
                break;
        }
        return result;
    }
    float edge_length(uint32_t halfedge) const { return (V.col(from_vertex(halfedge)) - V.col(to_vertex(halfedge))).norm(); }

    // for computing 4-Rosy
    ::MultiResolutionHierarchy mRes;
    ::Optimizer mOptimizer;
    Eigen::MatrixXf Q;      // one of four tangent vectors of 4-Rosy of a vertex

    // intersection
    std::shared_ptr<::BVH> mBVH;

    TriMesh();
    ~TriMesh();
    bool is_empty() const { return V.cols() == 0; }
    bool load(const std::string& filename, float creaseAngle_);
    void preprocess(float creaseAngle_);
    void tetrahedralize(TetMesh& tetMesh, const kt84::tetgen_util::Switches& tetgen_switches);
    void expand_kring();    // OBSOLETE
    void computeRoSy(const std::vector<RoSyConstraint>& constraints = {});
    TriMeshPoint rayIntersect(const ::Ray& ray) const;
    Eigen::Vector3d getNormal(const TriMeshPoint& p) const;
};

struct TetMeshTraits : public kt84::openvolumemesh::StaticProperty_EmptyTraits {
};

using TetMeshBase =
    kt84::openvolumemesh::TetrahedralMesh<
// #ifndef WIN32
    kt84::openvolumemesh::StaticProperty<TetMeshTraits,
// #else
//     kt84::openvolumemesh::DebugInfo<TetMeshTraits,
// #endif
    kt84::openvolumemesh::Utility<
    OpenVolumeMesh::GeometricPolyhedralMeshV3d>>>;

struct TetMesh : public TetMeshBase {
    BVH mBVH;
};

struct Isosurface {
    struct PerVertexData {
        double funcVal;
    };
    struct PerEdgeData {
        TetMeshPoint intersection;
    };
    struct PerFaceData {
        std::array<TetMeshPoint,2> intersections;
    };
    struct PerTetData {
        Eigen::Vector3d gradient;
        std::vector<TetMeshPoint> intersections;        // 3 or 4 intersections
        bool ignored = false;
    };
    std::vector<PerVertexData> perVertexData;
    std::vector<PerEdgeData> perEdgeData;
    std::vector<PerFaceData> perFaceData;
    std::vector<PerTetData> perTetData;
};

struct Sheet {
    enum struct Type {
        UNDEFINED = -1,
        Freeform,
        Cylinder,
        Sketch2D
    } type = Type::UNDEFINED;
    int id = -1;
    std::array<uint32_t,2> numSubdiv = {{4,4}};       // number of subdivision can be specified separately for front/back sides
    static int id_counter;
    static void reset_counter(int value) { id_counter = std::max<int>(value, id_counter); }
    static Sheet make(Type type) { return Sheet{type, ++id_counter}; }
    std::string typeName() const {
        return type == Type::Freeform ? "Freeform" :
               type == Type::Cylinder ? "Cylinder" : "Sketch2D";
    }
    struct Freeform {
        kt84::HermiteRBF<3, 1, kt84::RBFKernel_Cubed, 1> rbf;
        std::vector<kt84::PointNormal> points;
        std::vector<std::pair<TriMeshPoint, kt84::PointNormal>> surfacePoints;
        bool is_empty() const { return points.empty() && surfacePoints.empty(); }
    } freeform;
    struct Cylinder {
        double radius;
    } cylinder;
    struct Sketch2D {
        kt84::HermiteRBF<3, 1, kt84::RBFKernel_Cubed, 1> rbf;
        std::vector<kt84::PointNormal2d> points;
    } sketch2d;
    struct Canvas {
        Eigen::Matrix3d rotation;   // unitary matrix of columns: right, up, normal
        Eigen::Vector3d translation;
        Eigen::Vector2d project(const Eigen::Vector3d& p) const {
            return (rotation.transpose() * (p - translation)).head(2);
        }
        Eigen::Vector3d unproject(const Eigen::Vector2d& p) const {
            return rotation * Eigen::Vector3d(p[0], p[1], 0) + translation;
        }
        Eigen::Vector3d intersect(const Eigen::Vector3d& eye, const Eigen::Vector3d& mouse) const {
            return kt84::eigen_util::intersection_plane_line<Eigen::Vector3d>(translation, rotation.col(2), eye, mouse);
        }
        std::array<Eigen::Vector3d, 4> corners(double r) const {
            return {
                translation - r * rotation.col(0) -  r * rotation.col(1),
                translation + r * rotation.col(0) -  r * rotation.col(1),
                translation + r * rotation.col(0) +  r * rotation.col(1),
                translation - r * rotation.col(0) +  r * rotation.col(1)
            };
        }
    } canvas;
    double func(const Eigen::Vector3d& p) const;                                    // evaluates the underlying function
    double funcLinear(const TetMesh& tetMesh, const TetMeshPoint& p) const;         // linearly interpolates values sampled at tet corners
    Eigen::Vector3d gradient(const Eigen::Vector3d& p) const;
    Isosurface isosurface;
    std::vector<int> ignoredTets;
    kt84::DisplayList dispList;
    void buildFunc();
    void initCanvas(const kt84::Polyline3d& stroke3d, const kt84::CameraFree& camera, double resampleInterval);
    void update(const TetMesh& tetMesh);
};

// OBSOLETE
template <typename T>
struct Grid {
    void resize(const Eigen::Vector3i& numCells) {
        mNumCells = numCells;
        mPoints.clear();
        mPoints.resize((mNumCells[0]+1) * (mNumCells[1]+1) * (mNumCells[2]+1), T{});
    }
    int getIndexUnrolled(const Eigen::Vector3i& index) const { return index[0] + (mNumCells[0]+1)*(index[1] + (mNumCells[1]+1)*index[2]); }
    auto      & operator()(const Eigen::Vector3i& index)       { return mPoints[getIndexUnrolled(index)]; }
    auto const& operator()(const Eigen::Vector3i& index) const { return mPoints[getIndexUnrolled(index)]; }
    std::vector<T> mPoints;
    Eigen::Vector3i mNumCells = {0,0,0};     // number of cells along each axis
};

// OBSOLETE
template <typename T>
struct Grid2D {
    void resize(const Eigen::Vector2i& numCells) {
        mNumCells = numCells;
        mPoints.clear();
        mPoints.resize((mNumCells[0]+1) * (mNumCells[1]+1), T{});
    }
    int getIndexUnrolled(const Eigen::Vector2i& index) const { return index[0] + (mNumCells[0]+1)*index[1]; }
    auto      & operator()(const Eigen::Vector2i& index)       { return mPoints[getIndexUnrolled(index)]; }
    auto const& operator()(const Eigen::Vector2i& index) const { return mPoints[getIndexUnrolled(index)]; }
    std::vector<T> mPoints;
    Eigen::Vector2i mNumCells = {0,0};     // number of cells along each axis
};

struct ElemCode {
    std::string str = "";
    bool operator==(const ElemCode& rhs) const {
        if (str.size() != rhs.str.size()) {
            std::cout << "Warning: element codes of differnet sizes got compared!\n";
            return false;
        }
        for (size_t i=0; i<str.size(); ++i) {
            if (str[i]=='*' || rhs.str[i]=='*' || str[i]==rhs.str[i])
                continue;
            return false;
        }
        return true;
    }
    bool operator!=(const ElemCode& rhs) const { return !(*this==rhs); }
};

} // namespace dsm
namespace std {
template <>
struct hash<dsm::ElemCode> {
    size_t operator()(const dsm::ElemCode&) const { return 0; }
};
}
namespace dsm {

struct HexLayoutTraits : public kt84::openvolumemesh::StaticProperty_EmptyTraits {
    struct VertexData {
        ElemCode code;                              // corresponds to dual cell; all sheets should have either '+' or '-'
        std::unordered_set<CHandle> reftet_cells;            // set of tets in RefinedTetMesh corresponding to this dual cell
        std::unordered_map<VHandle, Eigen::Vector3d> param3d;   // result of 3D parameterization for each RefinedTetMesh vertex covered by reftet_cells
        std::unordered_set<HFHandle> reftet_boundary_halffaces;       // set of halffaces in RefinedTetMesh corresponding to this (oriented) dual face
        std::unordered_map<VHandle, Eigen::Vector2d> boundary_param2d;      // boundary dual face (i.e. intersection of dual cell and boundary surface) must be parameterized to 2D similar to internal dual face
        std::array<int,3> signature = {{0,0,0}};      // as per introduced in Liu et al. 2018 paper; i.e. signature[i] is the count of dual faces of (3+i)-gon forming this dual cell
        std::unordered_map<VHandle, int> dualCellCorner_to_referencePolyhedronCorner;       // mapping from RefinedTetMesh vertex to 3D position of corresponding reference polyhedron corners, obtained via boost::isomorphism
        bool flipped = false;

        // (for boundary vertex only) if this dual cell contains a featuregraph node or is crossed by featuregraph arcs, the origin of the 2d parameter space should land on this reftet vertex
        //      if no featuregraph node exists in this dual cell, the midpoint of the crossing featuregraph arc is treated as constrained
        VHandle reftet_constrained_vertex;
        // we distinguish boundary dual cells containing a featuregraph node (possibly along with some featuregraph arcs) from those only containing featuregraph arcs without a featuregraph node.
        // we call the latter as 'pseudo constrained'; this vertex is not constrained to be exactly on the origin, it only indicates where the 'middle point' lies within the cell
        bool pseudo_constrained = false;

        // (for boundary vertex only) for each outgoing boundary layout halfedge, if the corresponding boundary dual edge is crossed by a featuregraph arc, we store a sequence of reftet halfedges
        //      starting from the above `VertexData::reftet_constrained_vertex` and ending at the corresponding `EdgeData::reftet_constrained_vertex`
        std::vector<std::vector<HEHandle>> reftet_constrained_halfedges_per_boundary_outgoing_halfedge;
        int featuregraph_node = -1;

        TetMeshPoint testPoint;                     // an arbitrary point inside the dual cell for determining which side it is w.r.t. sheets
    };
    struct EdgeData : public kt84::openvolumemesh::EdgeLength_EdgeTraits {
        ElemCode code;                              // corresponds to dual face; one sheet should have '0'
        int sheetId = -1;   // unneeded
        std::unordered_map<VHandle, Eigen::Vector2d> param2d;   // result of 2D parameterization for each RefinedTetMesh vertex covered by reftet_halffaces
        VHandle reftet_constrained_vertex;          // (for boundary edge only) if this edge is crossed by a featuregraph arc, the reftet vertex at the intersection is stored
        int featuregraph_arc = -1;
        TetMeshPoint testPoint;                     // an arbitrary point on the dual face for determining which side it is w.r.t. sheets
    };
    struct FaceData {
        ElemCode code;                              // corresponds to dual edge; two sheets should have '0'
        std::vector<TetMeshPoint> sheetCrossing;
        std::array<int, 2> sheetId = {{-1, -1}};
    };
    struct HalfEdgeData {
        std::unordered_set<HFHandle> reftet_halffaces;       // set of halffaces in RefinedTetMesh corresponding to this (oriented) dual face
        std::vector<HEHandle> reftet_boundary_halfedges;     // a dual face corresponding to a layout _boundary_ edge must be bounded by an array of RefinedTetMesh boundary (oriented) edges 
    };
    struct HalfFaceData {
        std::vector<HEHandle> reftet_halfedges;  // array of halfedges in RefinedTetMesh corresponding to this (oriented) dual edge; possibly starting from/ending at boundary surface
    };
    struct CellData {
        ElemCode code;                           // corresponds to dual vertex; three sheets should have '0'
        TetMeshPoint sheetCrossing;
        std::array<int, 3> sheetId = {{-1, -1, -1}};
        VHandle reftet_vertex;           // vertex in RefinedTetMesh corresponding to this dual vertex
        // code notation: pnn --> positive with sheet 0, negative with sheet 1, negative with sheet 2
        //             @--------+------@
        //           _/      _/      _/|
        //         _/  npp _/  ppp _/  |
        //        /       /       /    |
        //       +-------+-------+ ppp +
        //     _/      _/      _/|   _/|
        //   _/  nnp _/  pnp _/  | _/  |
        //  /       /       /    |/    |
        // @-------+-------@ pnp + ppn @
        // |       |       |   _/|   _/
        // |  nnp  |  pnp  | _/  | _/
        // |       |       |/    |/
        // +-------+-------+ pnn +
        // |       |       |   _/
        // |  nnn  |  pnn  | _/
        // |       |       |/
        // @-------+-------@
        //
        // corner indices:
        //   6----7
        //  /    /|
        // 4----5 |
        // |    | 3
        // |    |/
        // 0----1
        // 
        // edge indices (orthogonal to a sheet, going from negative to positive):
        //     @---3---@
        //    /       /|
        //   5       7 11
        //  /       /  |
        // @---2---@   @
        // |       |  /
        // 8       9 6
        // |       |/
        // @---0---@
        //
        // face indices:
        //          3
        //          |
        //       @----@
        //      / 5  /|
        //     @----@1|
        // 0---| 2  | @
        //     |    |/
        //     @----@
        //        |
        //        4
        // 
        std::array<VHandle, 8> corner;
        std::array<HEHandle, 12> edge;
        std::array<HFHandle, 6> face;
        bool flipped = false;       // true if gradients of three sheets form left-handed frame
        Eigen::Vector3f color;
        kt84::DisplayList dispList;
    };
};

struct HexLayout : public
    kt84::openvolumemesh::EdgeLength<
// #ifndef WIN32
    kt84::openvolumemesh::StaticProperty<HexLayoutTraits,
// #else
//     kt84::openvolumemesh::DebugInfo<HexLayoutTraits,
// #endif
    kt84::openvolumemesh::HexMeshUtility<
    OpenVolumeMesh::GeometricHexahedralMeshV3d>>>
{
    std::unordered_map<ElemCode, VHandle> vertexByCode;
    std::unordered_map<ElemCode, EHandle> edgeByCode;
    std::unordered_map<ElemCode, FHandle> faceByCode;
    std::unordered_map<ElemCode, CHandle> cellByCode;
};

struct RefinedTetMeshTraits : public kt84::openvolumemesh::StaticProperty_EmptyTraits {
    struct VertexData {
        bool constrained = false;
        CHandle layout_cell;
        FHandle layout_boundary_face;    // if this vertex is on a boundary dual vertex (intersection of dual edge and boundary surface), the corresponding layout face is stored
    };
    struct EdgeData {
        bool constrained = false;
        EHandle layout_boundary_edge;    // if this edge is on a boundary dual edge (intersection of dual face and boundary surface), the corresponding layout edge is stored
    };
    struct CellData {
        VHandle layout_vertex;    // obtained using TetGen's regional attribute
    };
};

using RefinedTetMeshBase =
    kt84::openvolumemesh::TetrahedralMesh<
    kt84::openvolumemesh::StaticProperty<RefinedTetMeshTraits,
    kt84::openvolumemesh::Utility<
    OpenVolumeMesh::GeometricPolyhedralMeshV3d>>>;

struct RefinedTetMesh : public RefinedTetMeshBase {
    BVH mBVH;
};

struct ReferencePolyhedron {
    Eigen::MatrixXd V;
    std::vector<Eigen::VectorXi> F;         // a face can have degree of either 3, 4 or 5; a halfedge from F[f][i] to F[f][(i+1)%deg] gets assigned an index 5*f + i
    Eigen::VectorXi E2E;                    // array of 5*#F integers storing opposite halfedge index (-1 for irrelevant indices for face with degree less than 5)
    Eigen::VectorXi V2E;                    // index of an outgoing halfedge
    std::vector<std::pair<int,int>> edges;  // undirected edges; assume e.first < e.second
    int dedge_prev(int e) const {
        const int f = e/5;
        const int i = e%5;
        const int deg = F[f].size();
        return 5*f + (i + deg-1)%deg;
    }
    int dedge_next(int e) const {
        const int f = e/5;
        const int i = e%5;
        const int deg = F[f].size();
        return 5*f + (i + 1)%deg;
    }
    int from_vertex(int e) const { return F[e/5][e%5]; }
    int to_vertex(int e) const {
        const int f = e/5;
        const int i = e%5;
        const int deg = F[f].size();
        return F[f][(i+1)%deg];
    }
    static const ReferencePolyhedron& get(const std::array<int,3>& signature);
    bool is_face_flipped(std::vector<int> face_vertices) const;       // returns true if given sequence of vertex indices is in the reversed order of one of its faces' sequence of vertex indices
};

struct HexMeshTraits : public kt84::openvolumemesh::StaticProperty_EmptyTraits {
    struct VertexData {
        VHandle layout_vertex;
    };
    struct HalfEdgeData {
        HEHandle layout_halfedge;
    };
    struct CellData {
        CHandle layout_cell;
        std::array<kt84::DisplayList, 2> dispList;
    };
};

struct HexMesh : public
// #ifndef WIN32
    kt84::openvolumemesh::StaticProperty<HexMeshTraits,
// #else
//     kt84::openvolumemesh::DebugInfo<HexMeshTraits,
// #endif
    kt84::openvolumemesh::HexMeshUtility<
    OpenVolumeMesh::GeometricHexahedralMeshV3d>>
{};

namespace error {

enum DualizationFailed_Type {
    TangentialIntersection,
    MoreThanThreeSheetsIntersect,
    MultiplePrimalCellsWithSameCode,
    IsolatedDuallCell,
    UnsupportedSingularity
};

struct DualizationFailed : public std::runtime_error {
    DualizationFailed(DualizationFailed_Type type, const std::vector<int>& sheetId, const std::vector<Eigen::Vector3d>& locations)
        : std::runtime_error("Dualization failed")
        , type(type)
        , sheetId(sheetId)
        , locations(locations)
    {}
    DualizationFailed_Type type;
    std::vector<int> sheetId;
    std::vector<Eigen::Vector3d> locations;
};

enum FeatureGraphInconsistent_Type {
    MultipleNodesInDualCell,
    MultipleArcsOnDualFace,
    NonStarLikeConfigInDuallCell,
    ArcWithNoIntersection
};

struct FeatureGraphInconsistent : public std::runtime_error {
    FeatureGraphInconsistent(FeatureGraphInconsistent_Type type, VHandle layout_vertex = VHandle{}, EHandle layout_edge = EHandle{}, int featuregraph_arc = -1)
        : std::runtime_error("Feature graph is inconsistent with layout topology")
        , layout_vertex(layout_vertex)
        , layout_edge(layout_edge)
        , featuregraph_arc(featuregraph_arc)
    {}
    FeatureGraphInconsistent_Type type;
    VHandle layout_vertex;
    EHandle layout_edge;
    int featuregraph_arc;
};

}   // namespace dsm::error

}   // namespace dsm
