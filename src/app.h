#pragma once

#include "typedefs.h"
#include <kt84/graphics/graphics_util.hh>
#include <kt84/tw_util.hh>
#include <kt84/glfw_util.hh>
#include <kt84/glut_clone/geometry.hh>
#include <kt84/tinyfd_util.hh>
#include <kt84/Memento.hh>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include "app/ssao.h"

#define APPNAME "DualSheetMeshing"

namespace dsm { namespace app {

enum Button {
    LEFT = 0,
    RIGHT,
    MIDDLE,
    UNDEFINED
};

struct State {
    TwBar* m_bar = nullptr;
    virtual const char* name() const { return ""; }
    virtual void enter() {}                // called when entering to this state
    virtual void render_ssao() {}
    virtual void render() {}
    virtual void mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {}
    virtual void mouseUp  (int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {}
    virtual void mouseMove(int mouse_x, int mouse_y) {}
    virtual void keyboard  (int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {}
    virtual void keyboardUp(int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed) {}
};

struct Config {
    Config();
    size_t undo_buffer_size;
    float creaseAngle;
    struct AutoSave {
        AutoSave();
        bool        enabled;
        int         interval;
        bool        unsaved;
        std::time_t last_time;
    } autoSave;
    std::array<kt84::tetgen_util::Switches,2> tetgen_switches;      // the second one is for the refinement step
};

struct SerializedBlob {
    std::string triMesh;
    std::string tetMesh;
    std::string sheets;
    std::string hexLayout;
    std::string refinedTetMesh;
    std::string hexMesh;
    bool save(const std::string& filename);
    bool load(const std::string& filename);
};

struct Shader {
    kt84::ProgramObject silhouette;
    kt84::ProgramObject phong;
};

struct DisplayList {
    kt84::DisplayList triMesh_silhouette;
    kt84::DisplayList triMesh_crease;
    kt84::DisplayList triMesh_surface;
    kt84::DisplayList triMesh_surface_edges;
};

struct EventData {
    bool key_pressed[256];
    Eigen::Vector2i mouse_2D;
    Eigen::Vector3d mouse_3Dz0, mouse_3Dz1;             // 3D position obtained by unprojecting (mouse_2D.x, mouse_2D.y, 0) and (mouse_2D.x, mouse_2D.y, 1); the vector (mouse_3Dz1 - mouse_3Dz0) defines the eye ray
    dsm::TriMeshPoint mouse_on_triMesh;
    boost::optional<Eigen::Vector3d> mouse_3D;
    bool button_left_pressed;
    bool button_right_pressed;
    bool button_middle_pressed;
    bool shift_pressed;
    bool ctrl_pressed;
    bool alt_pressed;
};

struct RenderOption {
    std::map<std::string, bool> opt_bool;
    std::map<std::string, int> opt_int;
    std::map<std::string, float> opt_float;
    std::map<std::string, Eigen::Vector3f> opt_dir3f;
    std::map<std::string, Eigen::Vector3f> opt_color3f;
    void init();
    void addToBar(TwBar* bar);
    void print();
    bool& get_bool(const std::string& name) { return opt_bool.find(name)->second; }
    int& get_int(const std::string& name) { return opt_int.find(name)->second; }
    float& get_float(const std::string& name) { return opt_float.find(name)->second; }
    Eigen::Vector3f& get_dir3f(const std::string& name) { return opt_dir3f.find(name)->second; }
    Eigen::Vector3f& get_color3f(const std::string& name) { return opt_color3f.find(name)->second; }
};

// system data
extern bool g_glew_initialized;
extern State* g_state;
extern kt84::CameraFree g_camera;
extern std::string g_filename_dat;
extern kt84::glfw_util::LoopControl g_loopControl;
extern Config g_config;
extern SerializedBlob g_serializedBlob;
extern Shader g_shader;
extern DisplayList g_dispList;
extern EventData g_eventData;
extern RenderOption g_renderOption;
extern SSAO g_ssao;

// algorithm core objects
extern TriMesh g_triMesh;
extern TetMesh g_tetMesh;
extern std::vector<Sheet> g_sheets;
extern HexLayout g_hexLayout;
extern RefinedTetMesh g_refinedTetMesh;
extern HexMesh g_hexMesh;

void init();
void changeState(State& next_state);

bool importTriMesh(const std::string& filename);
bool exportHexMesh(const std::string& filename);
bool saveBinary(const std::string& filename = "");  // when filename is empty, g_filename_dat gets used
bool loadBinary(const std::string& filename);

bool common_mouseDown(int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed);
bool common_mouseUp  (int mouse_x, int mouse_y, Button button, bool shift_pressed, bool ctrl_pressed, bool alt_pressed);
bool common_mouseMove(int mouse_x, int mouse_y);
bool common_keyboard  (int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed);
bool common_keyboardUp(int key, bool shift_pressed, bool ctrl_pressed, bool alt_pressed);

}}
