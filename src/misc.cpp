#include "misc.h"
#include "helper.h"
using namespace std;
using namespace Eigen;
using namespace kt84;


bool dsm::misc::exportVTK(const HexMesh& hexMesh, const std::string& filename) {
    ofstream ofs(filename);
    ofs << "# vtk DataFile Version 3.0" << endl;
    ofs << "Volume mesh" << endl;
    ofs << "ASCII" << endl;
    ofs << "DATASET UNSTRUCTURED_GRID" << endl;
    ofs << "POINTS " << hexMesh.n_vertices() << " double" << endl;
    for (VHandle v : hexMesh.vertices()) {
        const Vec3d p = hexMesh.vertex(v);
        for (int i = 0; i < 3; ++i)
            ofs << (i==0 ? "" : " ") << p[i];
        ofs << endl;
    }
    ofs << "CELLS " << hexMesh.n_cells() << " " << (9*hexMesh.n_cells()) << endl;
    for (CHandle c : hexMesh.cells()) {
        ofs << 8;
        array<VHandle,8> hex_vertices;
        int i = 0;
        for (VHandle v : hexMesh.hex_vertices(c))
            hex_vertices[i++] = v;
        swap(hex_vertices[1], hex_vertices[3]);
        for (i = 0; i < 8; ++i)
            ofs << " " << hex_vertices[i];
        ofs << endl;
    }
    ofs << "CELL_TYPES " << hexMesh.n_cells() << endl;
    for (CHandle c : hexMesh.cells())
        ofs << "12 " << endl;
    return true;
}

template <class TetMeshT>
void dsm::misc::initTetMesh(TetMeshT& tetMesh, const MatrixXd& tetgen_V, const MatrixXi& tetgen_C) {
    MatrixXi E;
    MatrixXi F;
    MatrixXi C;
    E.resize(2, 0);
    F.resize(3, 0);
    C.resize(4, tetgen_C.cols());
    unordered_map<array<int,2>, int> edge_idx;
    unordered_map<array<int,3>, int> face_idx;
    for (int c = 0; c < tetgen_C.cols(); ++c) {
        for (int i = 0; i < 3; ++i) {
            for (int j = i + 1; j < 4; ++j) {
                const int vi = tetgen_C(i, c);
                const int vj = tetgen_C(j, c);
                const auto e_key = vi < vj ? array<int,2>{vi,vj} : array<int,2>{vj,vi};         // positive orientation assumes e[0] < e[1]
                if (edge_idx.find(e_key) == edge_idx.end()) {
                    edge_idx[e_key] = E.cols();
                    E.conservativeResize(2, E.cols()+1);
                    E.col(E.cols()-1) << e_key[0], e_key[1];
                }
            }
        }
        static const array<array<int,3>,4> corner_numbering = {{
            // see "5.2.4 .ele files" in TetGen manual
            {0,1,2},
            {3,1,0},
            {3,2,1},
            {3,0,2}
        }};
        for (int i = 0; i < 4; ++i) {
            array<int,3> face_corners;
            for (int j = 0; j < 3; ++j) {
                face_corners[j] = tetgen_C(corner_numbering[i][j], c);
            }
            auto f_key = face_corners;
            boost::sort(f_key);                                         // positive orientation assumes two edges being positive (i.e. sorted array of vertex indices)
            if (face_idx.find(f_key) == face_idx.end()) {
                face_idx[f_key] = F.cols();
                F.conservativeResize(3, F.cols()+1);
                for (int j = 0; j < 3; ++j) {
                    const auto e_key =  j == 0 ? array<int,2>{ f_key[0], f_key[1] } :
                                        j == 1 ? array<int,2>{ f_key[1], f_key[2] } : array<int,2>{ f_key[0], f_key[2] };
                    uint32_t e = 2 * edge_idx.find(e_key)->second;
                    if (j == 2)         // the last edge should be oriented negative
                        ++e;
                    F(j, F.cols()-1) = e;
                }
            }
            uint32_t f = 2 * face_idx[f_key];
            container_util::bring_front(face_corners, f_key[0]);
            if (!boost::equal(face_corners, f_key))     // if the rotated array starting with the smallest index disagrees with the sorted array, it means orientation is negative
                ++f;
            C(i, c) = f;
        }
    }
    initOpenVolumeMesh(tetMesh, tetgen_V, E, F, C);
}

template <class MeshT>
void dsm::misc::initOpenVolumeMesh(MeshT& mesh, const MatrixXd& V, const MatrixXi& E, const MatrixXi& F, const MatrixXi& C) {
    kt84::Timer timer;
    cout << "building OpenVolumeMesh datastructure ...\n";
    mesh.clear();
    mesh.enable_bottom_up_incidences(false);
    mesh.reserve_vertices(V.cols());
    mesh.reserve_edges(E.cols());
    mesh.reserve_faces(F.cols());
    mesh.reserve_cells(C.cols());
    mesh.staticProperty_setAutoResize(false);
    for (int v = 0; v < V.cols(); ++v) {
        Vector3d p = V.col(v);
        mesh.add_vertex(vector_cast<3, Vec3d>(p));
    }
    for (int e = 0; e < E.cols(); ++e) {
        mesh.add_edge(VHandle{E(0,e)}, VHandle{E(1,e)}, true);
    }
    for (int f = 0; f < F.cols(); ++f) {
        vector<HEHandle> hes;
        for (int i = 0; i < F.rows(); ++i)
            hes.push_back(HEHandle{F(i,f)});
        mesh.add_face(hes);
    }
    for (int c = 0; c < C.cols(); ++c) {
        vector<HFHandle> hfs;
        for (int i = 0; i < C.rows(); ++i)
            hfs.push_back(HFHandle{C(i,c)});
        mesh.add_cell(hfs);
    }
    mesh.enable_bottom_up_incidences(true);
    mesh.utility_storeIncidence();
    mesh.staticProperty_resize();
    cout << "Mesh statistics:\n" 
        << "vertices: " << mesh.n_vertices() << '\n'
        << "cells   : " << mesh.n_cells() << '\n'
        << "faces   : " << mesh.n_faces() << '\n'
        << "edges   : " << mesh.n_edges() << '\n';
    cout << "building OpenVolumeMesh datastructure ... " << timer.milliseconds_str() << endl;
}

dsm::Isosurface dsm::misc::marchingTetra(const TetMesh& mesh, const function<double(VHandle)>& func, double isovalue) {
    Isosurface result;
    // per-vertex data: funcVal
    result.perVertexData.resize(mesh.n_vertices());
    tbb_util::parallel_for(mesh.n_vertices(), [&](int vi) {
        VHandle v{vi};
        result.perVertexData[v].funcVal = func(v);
    });
    // per-edge data: intersection
    result.perEdgeData.resize(mesh.n_edges());
    tbb_util::parallel_for(mesh.n_edges(), [&](int ei) {
        EHandle e{ei};
        auto v = mesh.edge_vertices(e);
        double val[2] = { result.perVertexData[v[0]].funcVal,
                          result.perVertexData[v[1]].funcVal };
        assert(val[0] != isovalue);         // TODO: these assumptions may be too strong, probably we want to nicely handle cases where a tetmesh vertex lies exactly on a sheet
        assert(val[1] != isovalue);
        // (1-t)*val[0] + t*val[1] = isovalue
        double t = (isovalue-val[0]) / (val[1]-val[0]);
        if (0 < t && t < 1)
            result.perEdgeData[e].intersection = {e, {1-t, t}};
    });
    // per-face data: intersections[]
    result.perFaceData.resize(mesh.n_faces());
    tbb_util::parallel_for(mesh.n_faces(), [&](int fi) {
        FHandle f{fi};
        auto& f_intersections = result.perFaceData[f].intersections;
        int i = 0;
        for (auto e : mesh.face_edges(f)) {
            const auto& e_intersection = result.perEdgeData[e].intersection;
            if (e_intersection.is_valid())
                f_intersections[i++] = e_intersection;
        }
        assert(i==0 || i==2);
    });
    // per-tet data: gradient, intersections[]
    result.perTetData.resize(mesh.n_cells());
    tbb_util::parallel_for(mesh.n_cells(), [&](int ci) {
        CHandle c{ci};
        // gradient
        auto v = mesh.cell_vertices(c);
        Vector4d cornerVal;
        for (int i=0; i<4; ++i)
            cornerVal[i] = result.perVertexData[v[i]].funcVal;
        result.perTetData[c].gradient = helper::gradientAtTet(mesh, cornerVal, c);
        // intersections[]
        auto& c_intersections = result.perTetData[c].intersections;
        for (auto e : mesh.cell_edges(c)) {
            const auto& e_intersection = result.perEdgeData[e].intersection;
            if (e_intersection.is_valid())
                c_intersections.push_back(result.perEdgeData[e].intersection);
        }
        if (c_intersections.empty())
            return;
        assert(c_intersections.size()==3 || c_intersections.size()==4);
        // modify vertex ordering using 'cross-product' heuristic
        if (c_intersections.size()==4) {
            array<Vector3d,4> points;
            for (int i=0; i<4; ++i)
                points[i] = c_intersections[i].getPosition(mesh);
            const array<array<int,4>,3> permutations = {{{0,1,2,3}, {0,1,3,2}, {0,2,1,3}}};   // strange syntax... https://stackoverflow.com/questions/24390702
            MaxMinSelector<int> selector;
            for (int i=0; i<3; ++i) {
                auto& permutation = permutations[i];
                Vector3d cp={0,0,0};
                for (int j=0; j<4; ++j) {
                    Vector3d e0 = points[permutation[(j+1)%4]]-points[permutation[ j     ]],
                             e1 = points[permutation[(j+2)%4]]-points[permutation[(j+1)%4]];
                    cp += e0.cross(e1);
                }
                selector.update(cp.norm(), i);
            }
            auto& permutation = permutations[selector.max_value];   // choose the ordering maximizing estimated area
            auto old = c_intersections;
            for (int i=0; i<4; ++i)
                c_intersections[i] = old[permutation[i]];
        }
        // reverse c_intersections if the normal points opposite to gradient
        Vector3d d0 = c_intersections[1].getPosition(mesh) - c_intersections[0].getPosition(mesh),
                 d1 = c_intersections[2].getPosition(mesh) - c_intersections[0].getPosition(mesh);
        if (d0.cross(d1).dot(result.perTetData[c].gradient) < 0)
            boost::reverse(c_intersections);
    });
/*
    // build manifold mesh
    result.V.resize(3, 0);
    result.F.resize(3, 0);
    unordered_map<EHandle,int> vidx_map;
    for (CHandle tet : mesh.cells()) {
        auto& tetData = result.perTetData[tet];
        if (tetData.intersections.size() < 3)
            continue;
        // add vertices/faces to the matrices
        auto get_vidx = [&](const TetMeshPoint& p) -> int {
            const EHandle e = mesh.edge_handle(p.edge);
            if (vidx_map.find(e) == vidx_map.end()) {
                vidx_map[e] = result.V.cols();
                result.V.conservativeResize(3, result.V.cols()+1);
                result.V.col(result.V.cols()-1) = p.getPosition(mesh).cast<float>();
            }
            return vidx_map[e];
        };
        result.F.conservativeResize(3, result.F.cols()+1);
        for (int i = 0; i < 3; ++i) {
            result.F(i, result.F.cols()-1) = get_vidx(tetData.intersections[i]);
        }
        if (tetData.intersections.size() == 4) {
            result.F.conservativeResize(3, result.F.cols()+1);
            for (int i = 0; i < 3; ++i) {
                result.F(i, result.F.cols()-1) = get_vidx(tetData.intersections[array<int,3>{0,2,3}[i]]);
            }
        }
    }
    build_dedge(result.F, result.V, result.V2E, result.E2E, result.boundary, result.nonManifold);
    generate_smooth_normals(result.F, result.V, result.V2E, result.E2E, result.nonManifold, result.N, result.Nf);
*/
    return result;
}

namespace dsm { namespace misc { namespace detail {
template <class TetMeshT>
void buildBVH_sub(const TetMeshT& mesh, BVH::Node& node, int numLeafCells) {
    if (node.cells.size() < numLeafCells)
        return;
    node.child[0] = make_shared<BVH::Node>();
    node.child[1] = make_shared<BVH::Node>();
    Vector3d diagonal = node.bbox.diagonal();
    int splitAxis = diagonal.x()>diagonal.y() && diagonal.x()>diagonal.z() ? 0 : (diagonal.y()>diagonal.z() ? 1 : 2);
    diagonal[splitAxis] *= 0.5;
    node.child[0]->bbox.extend(node.bbox.min());
    node.child[0]->bbox.extend(node.bbox.min()+diagonal);
    node.child[1]->bbox.extend(node.bbox.max());
    node.child[1]->bbox.extend(node.bbox.max()-diagonal);
    for (auto c : node.cells) {
        bool contained[2] = {false, false};
        auto v = mesh.cell_vertices(c);
        Vector3d vpos[4];
        for (int i = 0; i < 4; ++i) {
            vpos[i] = vector_cast<3,Vector3d>(mesh.vertex(v[i]));
            for (int j = 0; j < 2; ++j)
                if (node.child[j]->bbox.contains(vpos[i]))
                    contained[j] = true;
        }
        for (int j = 0; j < 2; ++j) {
            if (contained[j]) {
                node.child[j]->cells.push_back(c);
                for (int i = 0; i < 4; ++i)
                    node.child[j]->bbox.extend(vpos[i]);
            }
        }
    }
    int n = node.cells.size(),
        n0 = node.child[0]->cells.size(),
        n1 = node.child[1]->cells.size();
    if (0<n0 && 0<n1 && n0<n && n1<n) {
        buildBVH_sub(mesh, *node.child[0], numLeafCells);
        buildBVH_sub(mesh, *node.child[1], numLeafCells);
    } else {
        node.child = {};
    }
}
template <class TetMeshT>
TetMeshPoint searchBVH_sub(const TetMeshT& mesh, const BVH::Node& node, const Vector3d& pos) {
    if (node.child[0] && node.child[0]->bbox.contains(pos))
        return searchBVH_sub(mesh, *node.child[0], pos);
    if (node.child[1] && node.child[1]->bbox.contains(pos))
        return searchBVH_sub(mesh, *node.child[1], pos);
    MaxMinSelector<TetMeshPoint> selector;
    for (auto c : node.cells) {
        Vector4d t;
        auto v = mesh.cell_vertices(c);
        Vector3d vpos[4];
        for (int i=0; i<4; ++i)
            vpos[i] = vector_cast<3,Vector3d>(mesh.vertex(v[i]));
        eigen_util::project_to_tetrahedron(vpos[0], vpos[1], vpos[2], vpos[3], pos, t);
        TetMeshPoint p{c,t};
        if (t.minCoeff() >= 0 && t.maxCoeff() <= 1)
            return p;
        Vector3d center = Vector3d::Zero();
        for (int i = 0; i < 4; ++i)
            center += 0.25 * vpos[i];
        selector.update((center - pos).squaredNorm(), p);
    }
    return selector.min_value;
}
} } }

template <class TetMeshT>
void dsm::misc::buildBVH(TetMeshT& mesh, int numLeafCells) {
    kt84::Timer timer;
    cout << "building BVH..." << endl;
    mesh.mBVH = {};
    for (auto v : mesh.vertices())
        mesh.mBVH.root.bbox.extend(vector_cast<3,Vector3d>(mesh.vertex(v)));
    for (auto c : mesh.cells())
        mesh.mBVH.root.cells.push_back(c);
    detail::buildBVH_sub(mesh, mesh.mBVH.root, numLeafCells);
    cout << "building BVH..." << timer.milliseconds_str() << endl;
}
template <class TetMeshT>
dsm::TetMeshPoint dsm::misc::searchBVH(const TetMeshT& mesh, const Vector3d& pos) {
    return detail::searchBVH_sub(mesh, mesh.mBVH.root, pos);
}

template void dsm::misc::initTetMesh<dsm::TetMesh       >(TetMesh       & tetMesh, const MatrixXd& tetgen_V, const MatrixXi& tetgen_C);
template void dsm::misc::initTetMesh<dsm::RefinedTetMesh>(RefinedTetMesh& tetMesh, const MatrixXd& tetgen_V, const MatrixXi& tetgen_C);
template void dsm::misc::initOpenVolumeMesh<dsm::TetMesh       >(dsm::TetMesh       & mesh, const MatrixXd& V, const MatrixXi& E, const MatrixXi& F, const MatrixXi& C);
template void dsm::misc::initOpenVolumeMesh<dsm::RefinedTetMesh>(dsm::RefinedTetMesh& mesh, const MatrixXd& V, const MatrixXi& E, const MatrixXi& F, const MatrixXi& C);
template void dsm::misc::initOpenVolumeMesh<dsm::HexLayout>(dsm::HexLayout& mesh, const MatrixXd& V, const MatrixXi& E, const MatrixXi& F, const MatrixXi& C);
template void dsm::misc::initOpenVolumeMesh<dsm::HexMesh  >(dsm::HexMesh  & mesh, const MatrixXd& V, const MatrixXi& E, const MatrixXi& F, const MatrixXi& C);
template void dsm::misc::buildBVH<dsm::TetMesh>(TetMesh& mesh, int numLeafCells);
template void dsm::misc::buildBVH<dsm::RefinedTetMesh>(RefinedTetMesh& mesh, int numLeafCells);
template dsm::TetMeshPoint dsm::misc::searchBVH<dsm::TetMesh>(const TetMesh& mesh, const Vector3d& pos);
template dsm::TetMeshPoint dsm::misc::searchBVH<dsm::RefinedTetMesh>(const RefinedTetMesh& mesh, const Vector3d& pos);
