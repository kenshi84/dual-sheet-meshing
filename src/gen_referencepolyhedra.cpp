#include "typedefs.h"
using namespace std;
using namespace Eigen;

inline int dedge_prev(int e, int deg) {
    const int f = e/5;
    const int i = e%5;
    return 5*f + (i + deg-1)%deg;
}

inline int dedge_next(int e, int deg) {
    const int f = e/5;
    const int i = e%5;
    return 5*f + (i + 1)%deg;
}

void optimize(MatrixXd& V, const vector<VectorXi>& F) {
    const int nV = V.rows();
    const int nF = F.size();

    // enumerate undirected edges
    MatrixXi E(0,2);
    for (int f = 0; f < nF; ++f) {
        const int deg = F[f].size();
        for (int i = 0; i < deg; ++i) {
            const int v0 = F[f][i];
            const int v1 = F[f][(i+1)%deg];
            if (v0 < v1) {
                E.conservativeResize(E.rows()+1,2);
                E.row(E.rows()-1) << v0, v1;
            }
        }
    }

    // compute vertex valence
    VectorXi valence = VectorXi::Zero(nV);
    for (int e = 0; e < E.rows(); ++e) {
        valence[E(e,0)]++;
        valence[E(e,1)]++;
    }

    while (true) {
        // recenter
        Vector3d centroid = Vector3d::Zero();
        for (int v = 0; v < nV; ++v) {
            centroid += V.row(v) / nV;
        }
        for (int v = 0; v < nV; ++v) {
            V.row(v) -= centroid;
        }

        // check error
        double error = 0;
        for (int e = 0; e < E.rows(); ++e) {
            error += abs((V.row(E(e,1)) - V.row(E(e,0))).norm() - 1);
        }
        if (error < 1.0e-10) {
            return;
        }

        // update vertices
        MatrixXd V2;
        V2.setZero(nV,3);
        for (int e = 0; e < E.rows(); ++e) {
            const int v0 = E(e,0);
            const int v1 = E(e,1);
            Vector3d midpoint = 0.5 * (V.row(v0) + V.row(v1));
            Vector3d n = (V.row(v1) - V.row(v0)).normalized();
            V2.row(v0) += midpoint - 0.5*n;
            V2.row(v1) += midpoint + 0.5*n;
        }
        for (int v = 0; v < nV; ++v) {
            V.row(v) = V2.row(v) / valence[v];
        }
    }
}

pair<MatrixXd, vector<VectorXi>> dualize(const MatrixXd& V, const vector<VectorXi>& F) {
    const int nV = V.rows();
    const int nF = F.size();

    // compute halfedges
    unordered_map<array<int,2>,int> pair_to_halfedgeIdx;
    for (int f = 0; f < nF; ++f) {
        const int deg = F[f].size();
        for (int i = 0; i < deg; ++i) {
            const array<int,2> p = { F[f][i], F[f][(i+1)%deg] };
            assert(pair_to_halfedgeIdx.find(p) == pair_to_halfedgeIdx.end());
            pair_to_halfedgeIdx[p] = 5*f + i;
        }
    }
    VectorXi E2E = VectorXi::Constant(5*nF,-1);
    VectorXi V2E = VectorXi::Constant(nV,-1);
    for (int f = 0; f < nF; ++f) {
        const int deg = F[f].size();
        for (int i = 0; i < deg; ++i) {
            const int halfedgeIdx = 5*f + i;
            E2E[halfedgeIdx] = pair_to_halfedgeIdx.find({F[f][(i+1)%deg], F[f][i]})->second;
            V2E[F[f][i]] = halfedgeIdx;
        }
    }

    // dualize
    MatrixXd dual_V;
    dual_V.setZero(nF,3);
    for (int f = 0; f < nF; ++f) {
        const int deg = F[f].size();
        for (int i = 0; i < deg; ++i)
            dual_V.row(f) += V.row(F[f][i]) / deg;
    }
    vector<VectorXi> dual_F(nV);
    for (int v = 0; v < nV; ++v) {
        dual_F[v].resize(0);
        for (int e = V2E[v]; ; ) {
            const int f = e/5;
            const int deg = F[f].size();
            dual_F[v].conservativeResize(dual_F[v].size()+1);
            dual_F[v][dual_F[v].size()-1] = f;
            e = E2E[dedge_prev(e,deg)];
            if (e == V2E[v])
                break;
        }
    }
    return {dual_V, dual_F};
}

struct PointKey {
    std::map<int, boost::rational<int>> weightedIndices;
    void add(int index, const boost::rational<int>& weight = 1) {
        weightedIndices[index] += weight;
    }
    void add(const PointKey& x, const boost::rational<int>& multiplier = 1) {
        for (auto& p : x.weightedIndices)
            weightedIndices[p.first] += multiplier * p.second;
    }
    void make_unique() {
        for (auto i = weightedIndices.begin(); i != weightedIndices.end(); ) {
            if (i->second == 0)
                i = weightedIndices.erase(i);
            else
                ++i;
        }
    }
    bool operator==(const PointKey& rhs) const { return weightedIndices == rhs.weightedIndices; }
};
namespace std {
template<>
struct hash<PointKey> {
    size_t operator()(const PointKey& x) const {
        size_t h = 0;
        for (const pair<int, boost::rational<int>>& p : x.weightedIndices) {
            kt84::hash_util::hash_combine(h, p.first);
            kt84::hash_util::hash_combine(h, p.second.numerator());
            kt84::hash_util::hash_combine(h, p.second.denominator());
        }
        return h;
    }
};
}

pair<MatrixXd, vector<VectorXi>> subdivide(const MatrixXd& V, const vector<VectorXi>& F, int numSubdiv) {
    MatrixXd subdiv_V(0,3);
    vector<VectorXi> subdiv_F;
    unordered_map<PointKey, int> key_to_index;
    const int numCells = numSubdiv + 1;
    for (int f = 0; f < F.size(); ++f) {
        const int deg = F[f].size();
        PointKey centroid_key;
        Vector3d centroid_pos = Vector3d::Zero();
        for (int i = 0; i < deg; ++i) {
            centroid_key.add(F[f][i], {1,deg});
            centroid_pos += V.row(F[f][i]) / deg;
        }
        for (int i = 0; i < deg; ++i) {
            const int v = F[f][i];
            const int v_next = F[f][(i+1)%deg];
            const int v_prev = F[f][(i+deg-1)%deg];
            const array<PointKey,4> corner_key = {{
                { {{v, 1}} },
                { {{v,boost::rational<int>{1,2}}, {v_next,boost::rational<int>{1,2}}} },
                centroid_key,
                { {{v,boost::rational<int>{1,2}}, {v_prev,boost::rational<int>{1,2}}} }
            }};
            const array<Vector3d,4> corner_pos = {{
                V.row(v),
                0.5 * V.row(v) + 0.5* V.row(v_next),
                centroid_pos,
                0.5 * V.row(v) + 0.5* V.row(v_prev)
            }};
            Vector2i index;
            dsm::Grid2D<PointKey> grid;
            grid.resize(Vector2i::Constant(numCells));
            for (index[0]=0; index[0]<=numCells; ++index[0])
            for (index[1]=0; index[1]<=numCells; ++index[1])
            {
                auto t = kt84::vector_cast<2,array<boost::rational<int>,2>>(index);
                t[0] /= numCells;
                t[1] /= numCells;
                grid(index).add(corner_key[0], (1-t[0])*(1-t[1]));
                grid(index).add(corner_key[1], t[0]*(1-t[1]));
                grid(index).add(corner_key[2], t[0]*t[1]);
                grid(index).add(corner_key[3], (1-t[0])*t[1]);
                grid(index).make_unique();
                if (key_to_index.find(grid(index)) == key_to_index.end()) {
                    key_to_index[grid(index)] = subdiv_V.rows();
                    const Vector2d t2 = {
                        boost::rational_cast<double>(t[0]),
                        boost::rational_cast<double>(t[1])
                    };
                    subdiv_V.conservativeResize(subdiv_V.rows()+1,3);
                    subdiv_V.row(subdiv_V.rows()-1) =
                        (1-t2[0]) * (1-t2[1]) * corner_pos[0] +
                           t2[0]  * (1-t2[1]) * corner_pos[1] +
                           t2[0]  *    t2[1]  * corner_pos[2] +
                        (1-t2[0]) *    t2[1]  * corner_pos[3];
                }
            }
            for (index[0]=0; index[0]<numCells; ++index[0])
            for (index[1]=0; index[1]<numCells; ++index[1])
            {
                subdiv_F.push_back({});
                subdiv_F.back().resize(4);
                subdiv_F.back() <<
                    key_to_index[grid(index)],
                    key_to_index[grid(index + Vector2i(1,0))],
                    key_to_index[grid(index + Vector2i(1,1))],
                    key_to_index[grid(index + Vector2i(0,1))];
            }
        }
    }
    return {subdiv_V, subdiv_F};
}

void write_off(const MatrixXd& V, const vector<VectorXi>& F, const string& filename_base) {
    kt84::tetgen_util::Format_off out_off;
    for (int v = 0; v < V.rows(); ++v) {
        for (int i = 0; i < 3; ++i)
           out_off.vertices.push_back(V(v,i));
    }
    for (int f = 0; f < F.size(); ++f) {
        out_off.faces.push_back({});
        const int deg = F[f].size();
        for (int i = 0; i < deg; ++i)
            out_off.faces.back().push_back(F[f][i]);
    }
    out_off.write(filename_base);
}

void gen_4_0_0(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(4,3);
    V.row(0) <<  1, 1, 1;
    V.row(1) << -1, 0, 0;
    V.row(2) <<  0,-1, 0;
    V.row(3) <<  0, 0,-1;
    F.resize(4);
    F[0].resize(3); F[0] << 0,3,1;
    F[1].resize(3); F[1] << 0,1,2;
    F[2].resize(3); F[2] << 0,2,3;
    F[3].resize(3); F[3] << 3,2,1;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    write_off(dual_V, dual_F, "reference-polyhedron-4-0-0");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-4-0-0-subdiv");
}

void gen_2_3_0(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(5,3);
    V.row(0) << 0,0,1;
    V.row(1) << 0,0,-1;
    V.row(2) << 1,1,0;
    V.row(3) << -1,1,0;
    V.row(4) << 0,-1,0;
    F.resize(6);
    F[0].resize(3); F[0] << 0,4,2;
    F[1].resize(3); F[1] << 0,2,3;
    F[2].resize(3); F[2] << 0,3,4;
    F[3].resize(3); F[3] << 1,2,4;
    F[4].resize(3); F[4] << 1,3,2;
    F[5].resize(3); F[5] << 1,4,3;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    write_off(dual_V, dual_F, "reference-polyhedron-2-3-0");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-2-3-0-subdiv");
}

void gen_2_2_2(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(6,3);
    V.row(0) << -1,-1, 0;
    V.row(1) <<  1,-1, 0;
    V.row(2) << -1, 1, 0;
    V.row(3) <<  1, 1, 0;
    V.row(4) <<  0,-1,-1;
    V.row(5) <<  0,-1, 1;
    F.resize(8);
    F[0].resize(3); F[0] << 1,5,4;
    F[1].resize(3); F[1] << 1,3,5;
    F[2].resize(3); F[2] << 2,5,3;
    F[3].resize(3); F[3] << 0,5,2;
    F[4].resize(3); F[4] << 0,4,5;
    F[5].resize(3); F[5] << 0,2,4;
    F[6].resize(3); F[6] << 2,3,4;
    F[7].resize(3); F[7] << 1,4,3;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    write_off(dual_V, dual_F, "reference-polyhedron-2-2-2");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-2-2-2-subdiv");
}

void gen_0_6_0(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(6,3);
    V.row(0) << 0,0,1;
    V.row(1) << 0,0,-1;
    V.row(2) << 0,1,0;
    V.row(3) << -1,0,0;
    V.row(4) << 0,-1,0;
    V.row(5) << 0,0,1;
    F.resize(8);
    F[0].resize(3); F[0] << 0,5,2;
    F[1].resize(3); F[1] << 0,2,3;
    F[2].resize(3); F[2] << 0,3,4;
    F[3].resize(3); F[3] << 0,4,5;
    F[4].resize(3); F[4] << 1,2,5;
    F[5].resize(3); F[5] << 1,3,2;
    F[6].resize(3); F[6] << 1,4,3;
    F[7].resize(3); F[7] << 1,5,4;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    write_off(dual_V, dual_F, "reference-polyhedron-0-6-0");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-0-6-0-subdiv");
}

void gen_0_5_2(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(7,3);
    V.row(0) << 0,0,1;
    V.row(1) << 0,0,-1;
    V.row(2) << -1,2,0;
    V.row(3) << -2,-1,0;
    V.row(4) << 0,-2,0;
    V.row(5) << 2,-1,0;
    V.row(6) << 1,2,0;
    F.resize(10);
    F[0].resize(3); F[0] << 0,6,2;
    F[1].resize(3); F[1] << 0,2,3;
    F[2].resize(3); F[2] << 0,3,4;
    F[3].resize(3); F[3] << 0,4,5;
    F[4].resize(3); F[4] << 0,5,6;
    F[5].resize(3); F[5] << 1,2,6;
    F[6].resize(3); F[6] << 1,3,2;
    F[7].resize(3); F[7] << 1,4,3;
    F[8].resize(3); F[8] << 1,5,4;
    F[9].resize(3); F[9] << 1,6,5;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    write_off(dual_V, dual_F, "reference-polyhedron-0-5-2");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-0-5-2-subdiv");
}

void gen_1_3_3(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(7,3);
    V.row(0) << 0,1,1;
    V.row(1) << 0,-1,0;
    V.row(2) << 1,0,-1;
    V.row(3) << -1,0,-1;
    V.row(4) << 0,1,-1;
    V.row(5) << -1,0,0;
    V.row(6) << 1,0,0;
    F.resize(10);
    F[0].resize(3); F[0] << 0,4,5;
    F[1].resize(3); F[1] << 0,5,6;
    F[2].resize(3); F[2] << 0,6,4;
    F[3].resize(3); F[3] << 3,5,4;
    F[4].resize(3); F[4] << 1,6,5;
    F[5].resize(3); F[5] << 2,4,6;
    F[6].resize(3); F[6] << 1,5,3;
    F[7].resize(3); F[7] << 1,2,6;
    F[8].resize(3); F[8] << 3,4,2;
    F[9].resize(3); F[9] << 3,2,1;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    write_off(dual_V, dual_F, "reference-polyhedron-1-3-3");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-1-3-3-subdiv");
}

void gen_0_4_4(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(8,3);
    V.row(0) << 1,-1,0;
    V.row(1) << -1,-1,0;
    V.row(2) << 0,1,1;
    V.row(3) << 0,1,-1;
    V.row(4) << 1,1,0;
    V.row(5) << 0,-1,1;
    V.row(6) << -1,1,0;
    V.row(7) << 0,-1,-1;
    F.resize(12);
    F[ 0].resize(3); F[ 0] << 0,4,5;
    F[ 1].resize(3); F[ 1] << 0,7,4;
    F[ 2].resize(3); F[ 2] << 3,4,7;
    F[ 3].resize(3); F[ 3] << 2,4,3;
    F[ 4].resize(3); F[ 4] << 2,5,4;
    F[ 5].resize(3); F[ 5] << 2,6,5;
    F[ 6].resize(3); F[ 6] << 1,5,6;
    F[ 7].resize(3); F[ 7] << 0,5,1;
    F[ 8].resize(3); F[ 8] << 0,1,7;
    F[ 9].resize(3); F[ 9] << 2,3,6;
    F[10].resize(3); F[10] << 1,6,7;
    F[11].resize(3); F[11] << 3,7,6;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    write_off(dual_V, dual_F, "reference-polyhedron-0-4-4");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-0-4-4-subdiv");
}

void gen_2_0_6(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(8,3);
    V.row(0) << 0,-2,0;
    V.row(1) << 0,2,0;
    V.row(2) << 0,-1,-1;
    V.row(3) << 1,-1,0;
    V.row(4) << -1,-1,0;
    V.row(5) << 0,1,1;
    V.row(6) << -1,1,0;
    V.row(7) << 1,1,0;
    F.resize(12);
    F[ 0].resize(3); F[ 0] << 0,3,4;
    F[ 1].resize(3); F[ 1] << 0,4,2;
    F[ 2].resize(3); F[ 2] << 0,2,3;
    F[ 3].resize(3); F[ 3] << 3,5,4;
    F[ 4].resize(3); F[ 4] << 2,4,6;
    F[ 5].resize(3); F[ 5] << 7,3,2;
    F[ 6].resize(3); F[ 6] << 4,5,6;
    F[ 7].resize(3); F[ 7] << 2,6,7;
    F[ 8].resize(3); F[ 8] << 3,7,5;
    F[ 9].resize(3); F[ 9] << 1,6,5;
    F[10].resize(3); F[10] << 1,7,6;
    F[11].resize(3); F[11] << 1,5,7;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    write_off(dual_V, dual_F, "reference-polyhedron-2-0-6");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-2-0-6-subdiv");
}

void gen_0_3_6(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(9,3);
    V.row(0) << -2,0,0;
    V.row(1) << 1,0,-1;
    V.row(2) << 1,0,1;
    V.row(3) << -1,1,-1;
    V.row(4) << -1,-1,-1;
    V.row(5) << -1,-1,1;
    V.row(6) << -1,1,1;
    V.row(7) << 1,1,0;
    V.row(8) << 1,-1,0;
    F.resize(14);
    F[ 0].resize(3); F[ 0] << 0,3,4;
    F[ 1].resize(3); F[ 1] << 0,6,3;
    F[ 2].resize(3); F[ 2] << 3,6,7;
    F[ 3].resize(3); F[ 3] << 1,3,7;
    F[ 4].resize(3); F[ 4] << 1,4,3;
    F[ 5].resize(3); F[ 5] << 1,8,4;
    F[ 6].resize(3); F[ 6] << 4,8,5;
    F[ 7].resize(3); F[ 7] << 0,4,5;
    F[ 8].resize(3); F[ 8] << 0,5,6;
    F[ 9].resize(3); F[ 9] << 1,7,8;
    F[10].resize(3); F[10] << 2,6,5;
    F[11].resize(3); F[11] << 2,7,6;
    F[12].resize(3); F[12] << 2,8,7;
    F[13].resize(3); F[13] << 2,5,8;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    dual_V(2,1) += 0.4;
    dual_V(6,1) -= 0.4;
    write_off(dual_V, dual_F, "reference-polyhedron-0-3-6");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-0-3-6-subdiv");
}

void gen_0_2_8(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(10,3);
    V.row(0) << 0,-2,0;
    V.row(1) << 0,2,0;
    V.row(2) << 0,-1,1;
    V.row(3) << -1,-1,0;
    V.row(4) << 0,-1,-1;
    V.row(5) << 1,-1,0;
    V.row(6) << 1,1,1;
    V.row(7) << -1,1,1;
    V.row(8) << -1,1,-1;
    V.row(9) << 1,1,-1;
    F.resize(16);
    F[ 0].resize(3); F[ 0] << 0,5,2;
    F[ 1].resize(3); F[ 1] << 0,2,3;
    F[ 2].resize(3); F[ 2] << 0,3,4;
    F[ 3].resize(3); F[ 3] << 0,4,5;
    F[ 4].resize(3); F[ 4] << 2,5,6;
    F[ 5].resize(3); F[ 5] << 2,7,3;
    F[ 6].resize(3); F[ 6] << 3,8,4;
    F[ 7].resize(3); F[ 7] << 4,9,5;
    F[ 8].resize(3); F[ 8] << 2,6,7;
    F[ 9].resize(3); F[ 9] << 3,7,8;
    F[10].resize(3); F[10] << 4,8,9;
    F[11].resize(3); F[11] << 5,9,6;
    F[12].resize(3); F[12] << 1,7,6;
    F[13].resize(3); F[13] << 1,8,7;
    F[14].resize(3); F[14] << 1,9,8;
    F[15].resize(3); F[15] << 1,6,9;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    write_off(dual_V, dual_F, "reference-polyhedron-0-2-8");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-0-2-8-subdiv");
}

void gen_0_0_12(int numSubdiv) {
    // primary triangular
    MatrixXd V;
    vector<VectorXi> F;
    V.resize(12,3);
    V.row( 0) << 0,0,2;
    V.row( 1) << 0,0,-2;
    V.row( 2) << -1,1,1;
    V.row( 3) << -1,0,1;
    V.row( 4) << 0,-1,1;
    V.row( 5) << 1,0,1;
    V.row( 6) << 1,1,1;
    V.row( 7) << -1,0,-1;
    V.row( 8) << -1,-1,-1;
    V.row( 9) << 1,-1,-1;
    V.row(10) << 1,0,-1;
    V.row(11) << 0,1,-1;
    F.resize(20);
    F[ 0].resize(3); F[ 0] << 0,6,2;
    F[ 1].resize(3); F[ 1] << 0,2,3;
    F[ 2].resize(3); F[ 2] << 0,3,4;
    F[ 3].resize(3); F[ 3] << 0,4,5;
    F[ 4].resize(3); F[ 4] << 0,5,6;
    F[ 5].resize(3); F[ 5] << 2,6,11;
    F[ 6].resize(3); F[ 6] << 3,2,7;
    F[ 7].resize(3); F[ 7] << 4,3,8;
    F[ 8].resize(3); F[ 8] << 5,4,9;
    F[ 9].resize(3); F[ 9] << 6,5,10;
    F[10].resize(3); F[10] << 2,11,7;
    F[11].resize(3); F[11] << 3,7,8;
    F[12].resize(3); F[12] << 4,8,9;
    F[13].resize(3); F[13] << 5,9,10;
    F[14].resize(3); F[14] << 6,10,11;
    F[15].resize(3); F[15] << 1,7,11;
    F[16].resize(3); F[16] << 1,8,7;
    F[17].resize(3); F[17] << 1,9,8;
    F[18].resize(3); F[18] << 1,10,9;
    F[19].resize(3); F[19] << 1,11,10;
    optimize(V, F);

    // dual polygonal
    MatrixXd dual_V;
    vector<VectorXi> dual_F;
    tie(dual_V, dual_F) = dualize(V, F);
    optimize(dual_V, dual_F);
    write_off(dual_V, dual_F, "reference-polyhedron-0-0-12");

    // subdivide
    MatrixXd subdiv_V;
    vector<VectorXi> subdiv_F;
    tie(subdiv_V, subdiv_F) = subdivide(dual_V, dual_F, numSubdiv);
    write_off(subdiv_V, subdiv_F, "reference-polyhedron-0-0-12-subdiv");
}

int main(int argc, char** argv) {
    int numSubdiv = 7;
    if (argc == 0) {
        try {
            numSubdiv = boost::lexical_cast<int>(argv[0]);
        }
        catch (...) {}
    }
    gen_4_0_0(numSubdiv);
    gen_2_3_0(numSubdiv);
    gen_2_2_2(numSubdiv);
    gen_0_6_0(numSubdiv);
    gen_0_5_2(numSubdiv);
    gen_1_3_3(numSubdiv);
    gen_0_4_4(numSubdiv);
    gen_2_0_6(numSubdiv);
    gen_0_3_6(numSubdiv);
    gen_0_2_8(numSubdiv);
    gen_0_0_12(numSubdiv);
}
