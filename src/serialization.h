#include "typedefs.h"
#include "misc.h"
#include <cereal/cereal.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/array.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/utility.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_set.hpp>
#include <cereal/types/unordered_map.hpp>

namespace cereal {
    //---------------------------------------------------------------------------------------------
    // Eigen::Matrix
    template<class Archive, typename Scalar, int Rows, int Cols, int Options, int MaxRows, int MaxCols>
    void save(Archive& archive, const Eigen::Matrix<Scalar,Rows,Cols,Options,MaxRows,MaxCols>& m) {
        archive(cereal::make_nvp("rows", m.rows()));
        archive(cereal::make_nvp("cols", m.cols()));
        for (int i=0; i<m.rows(); ++i)
        for (int j=0; j<m.cols(); ++j)
            archive(cereal::make_nvp((boost::format("row_%d_col_%d") % i % j).str(), m(i,j)));
    }
    template<class Archive, typename Scalar, int Rows, int Cols, int Options, int MaxRows, int MaxCols>
    void load(Archive& archive, Eigen::Matrix<Scalar,Rows,Cols,Options,MaxRows,MaxCols>& m) {
        Eigen::Index rows, cols;
        archive(cereal::make_nvp("rows", rows));
        archive(cereal::make_nvp("cols", cols));
        m.resize(rows,cols);
        for (int i=0; i<m.rows(); ++i)
        for (int j=0; j<m.cols(); ++j)
            archive(m(i,j));
    }

    //---------------------------------------------------------------------------------------------
    // boost::optional
    template<class Archive, class T>
    void save(Archive& archive, const boost::optional<T>& opt) {
        archive(cereal::make_nvp("is_valid", static_cast<bool>(opt)));
        if (opt)
            archive(cereal::make_nvp("value", *opt));
    }
    template<class Archive, class T>
    void load(Archive& archive, boost::optional<T>& opt) {
        opt = boost::none;
        bool is_valid;
        archive(cereal::make_nvp("is_valid", is_valid));
        if (!is_valid)
            return;
        T value;
        archive(cereal::make_nvp("value", value));
        opt = value;
    }

    //---------------------------------------------------------------------------------------------
    // kt84::Polyline2d
    template<class Archive>
    void serialize(Archive& archive, kt84::Polyline2d& polyline) {
        archive(cereal::make_nvp("is_loop", polyline.is_loop));
        archive(cereal::make_nvp("points", polyline.points));
    }

    //---------------------------------------------------------------------------------------------
    // kt84::Polyline3d
    template<class Archive>
    void serialize(Archive& archive, kt84::Polyline3d& polyline) {
        archive(cereal::make_nvp("is_loop", polyline.is_loop));
        archive(cereal::make_nvp("points", polyline.points));
    }

    //---------------------------------------------------------------------------------------------
    // kt84::Polyline_PointNormal
    template<class Archive>
    void serialize(Archive& archive, kt84::Polyline_PointNormal& polyline) {
        archive(cereal::make_nvp("is_loop", polyline.is_loop));
        archive(cereal::make_nvp("points", polyline.points));
    }

    //---------------------------------------------------------------------------------------------
    // dsm::{V,E,F,HE,HF,C}Handle
    template<class Archive> void serialize(Archive& archive, dsm::VHandle& handle) { archive(cereal::make_nvp("idx", *const_cast<int*>(&handle.idx()))); }
    template<class Archive> void serialize(Archive& archive, dsm::EHandle& handle) { archive(cereal::make_nvp("idx", *const_cast<int*>(&handle.idx()))); }
    template<class Archive> void serialize(Archive& archive, dsm::FHandle& handle) { archive(cereal::make_nvp("idx", *const_cast<int*>(&handle.idx()))); }
    template<class Archive> void serialize(Archive& archive, dsm::HEHandle& handle) { archive(cereal::make_nvp("idx", *const_cast<int*>(&handle.idx()))); }
    template<class Archive> void serialize(Archive& archive, dsm::HFHandle& handle) { archive(cereal::make_nvp("idx", *const_cast<int*>(&handle.idx()))); }
    template<class Archive> void serialize(Archive& archive, dsm::CHandle& handle) { archive(cereal::make_nvp("idx", *const_cast<int*>(&handle.idx()))); }

    //---------------------------------------------------------------------------------------------
    // dsm::TetMeshPoint
    template<class Archive> void serialize(Archive& archive, dsm::TetMeshPoint& p) {
        archive(cereal::make_nvp("vertex", p.vertex));
        archive(cereal::make_nvp("edge", p.edge));
        archive(cereal::make_nvp("face", p.face));
        archive(cereal::make_nvp("cell", p.cell));
        archive(cereal::make_nvp("bc", p.bc));
    }

    //---------------------------------------------------------------------------------------------
    // dsm::TriMeshPoint
    template<class Archive> void serialize(Archive& archive, dsm::TriMeshPoint& p) {
        archive(cereal::make_nvp("f", p.f));
        archive(cereal::make_nvp("bc", p.bc));
    }

    //---------------------------------------------------------------------------------------------
    // dsm::Sheet::Freeform
    template<class Archive>
    void serialize(Archive& archive, dsm::Sheet::Freeform& freeform) {
        archive(cereal::make_nvp("points", freeform.points));
        archive(cereal::make_nvp("surfacePoints", freeform.surfacePoints));
    }

    //---------------------------------------------------------------------------------------------
    // dsm::Sheet::Cylinder
    template<class Archive>
    void serialize(Archive& archive, dsm::Sheet::Cylinder& cylinder) {
        archive(cereal::make_nvp("radius", cylinder.radius));
    }

    //---------------------------------------------------------------------------------------------
    // dsm::Sheet::Sketch2D
    template<class Archive>
    void serialize(Archive& archive, dsm::Sheet::Sketch2D& sketch2d) {
        archive(cereal::make_nvp("points", sketch2d.points));
    }

    //---------------------------------------------------------------------------------------------
    // dsm::Sheet::Canvas
    template<class Archive>
    void serialize(Archive& archive, dsm::Sheet::Canvas& canvas) {
        archive(cereal::make_nvp("rotation", canvas.rotation));
        archive(cereal::make_nvp("translation", canvas.translation));
    }

    //---------------------------------------------------------------------------------------------
    // dsm::Sheet
    template<class Archive>
    void save(Archive& archive, const dsm::Sheet& sheet) {
        std::string type = sheet.type==dsm::Sheet::Type::Freeform ? "freeform" :
                           sheet.type==dsm::Sheet::Type::Cylinder ? "cylinder" : "sketch2d";
        archive(cereal::make_nvp("type", type));
        archive(cereal::make_nvp("id", sheet.id));
        archive(cereal::make_nvp("numSubdiv", sheet.numSubdiv));
        if (type=="freeform")
            archive(cereal::make_nvp("freeform", sheet.freeform));
        else if (type=="cylinder")
            archive(cereal::make_nvp("cylinder", sheet.cylinder));
        else
            archive(cereal::make_nvp("sketch2d", sheet.sketch2d));
        if (type=="cylinder" || type=="sketch2d")
            archive(cereal::make_nvp("canvas", sheet.canvas));
        archive(cereal::make_nvp("ignoredTets", sheet.ignoredTets));
    }
    template<class Archive>
    void load(Archive& archive, dsm::Sheet& sheet) {
        std::string type;
        archive(cereal::make_nvp("type", type));
        archive(cereal::make_nvp("id", sheet.id));
        dsm::Sheet::reset_counter(sheet.id);
        archive(cereal::make_nvp("numSubdiv", sheet.numSubdiv));
        if (type=="freeform") {
            sheet.type = dsm::Sheet::Type::Freeform;
            archive(cereal::make_nvp("freeform", sheet.freeform));
        } else if (type=="cylinder") {
            sheet.type = dsm::Sheet::Type::Cylinder;
            archive(cereal::make_nvp("cylinder", sheet.cylinder));
        } else if (type=="sketch2d") {
            sheet.type = dsm::Sheet::Type::Sketch2D;
            archive(cereal::make_nvp("sketch2d", sheet.sketch2d));
        } else {
            std::cerr << "Warning: unrecognized sheet type: " << type << std::endl;
        }
        if (type=="cylinder" || type=="sketch2d")
            archive(cereal::make_nvp("canvas", sheet.canvas));
        archive(cereal::make_nvp("ignoredTets", sheet.ignoredTets));
    }

    //---------------------------------------------------------------------------------------------
    // dsm::TriMesh
    template<class Archive>
    void serialize(Archive& archive, dsm::TriMesh& triMesh) {
        archive(cereal::make_nvp("V", triMesh.V));
        archive(cereal::make_nvp("F", triMesh.F));
        archive(cereal::make_nvp("Q", triMesh.Q));
        archive(cereal::make_nvp("creaseAngle", triMesh.creaseAngle));
        archive(cereal::make_nvp("vertex_constraint", triMesh.vertex_constraint));
        archive(cereal::make_nvp("edge_constraint", triMesh.edge_constraint));
    }

    //---------------------------------------------------------------------------------------------
    // generic code for OVM meshes:
    template<bool is_tetmesh, class Archive, class Mesh>
    void save(Archive& archive, const Mesh& mesh) {
        Eigen::MatrixXd V(3, mesh.n_vertices());
        Eigen::MatrixXi E(2, mesh.n_edges());
        Eigen::MatrixXi F(is_tetmesh ? 3 : 4, mesh.n_faces());
        Eigen::MatrixXi C(is_tetmesh ? 4 : 6, mesh.n_cells());
        for (auto v : mesh.vertices()) {
            V.col(v) = kt84::vector_cast<3, Eigen::Vector3d>(mesh.vertex(v));
        }
        for (auto e : mesh.edges()) {
            auto v = mesh.edge_vertices(e);
            E.col(e) << v[0], v[1];
        }
        for (auto f : mesh.faces()) {
            int i = 0;
            dsm::HFHandle hf = mesh.halfface_handle(f, 0);
            for (auto he : mesh.halfface_halfedges(hf))
                F(i++, f) = he;
        }
        for (auto c : mesh.cells()) {
            int i = 0;
            for (auto hf : mesh.cell_halffaces(c))
                C(i++, c) = hf;
        }
        archive(cereal::make_nvp("V", V));
        archive(cereal::make_nvp("E", E));
        archive(cereal::make_nvp("F", F));
        archive(cereal::make_nvp("C", C));
    }
    template<bool is_tetmesh, class Archive, class Mesh>
    void load(Archive& archive, Mesh& mesh) {
        Eigen::MatrixXd V;
        Eigen::MatrixXi E;
        Eigen::MatrixXi F;
        Eigen::MatrixXi C;
        archive(cereal::make_nvp("V", V));
        archive(cereal::make_nvp("E", E));
        archive(cereal::make_nvp("F", F));
        archive(cereal::make_nvp("C", C));
        dsm::misc::initOpenVolumeMesh(mesh, V, E, F, C);
    }

    //---------------------------------------------------------------------------------------------
    // dsm::TetMesh
    template<class Archive>
    void save(Archive& archive, const dsm::TetMesh& tetMesh) {
        save<true>(archive, tetMesh);
    }
    template<class Archive>
    void load(Archive& archive, dsm::TetMesh& tetMesh) {
        load<true>(archive, tetMesh);
        // dsm::misc::buildBVH(tetMesh, 10);
    }

    //---------------------------------------------------------------------------------------------
    // dsm::ElemCode
    template<class Archive>
    void serialize(Archive& archive, dsm::ElemCode& code) {
        archive(cereal::make_nvp("str", code.str));
    };

    //---------------------------------------------------------------------------------------------
    // dsm::HexLayout
    template<class Archive>
    void serialize(Archive& archive, dsm::HexLayoutTraits::VertexData& vdata) {
        archive(cereal::make_nvp("code", vdata.code));
        archive(cereal::make_nvp("reftet_cells", vdata.reftet_cells));
        archive(cereal::make_nvp("param3d", vdata.param3d));
        archive(cereal::make_nvp("reftet_boundary_halffaces", vdata.reftet_boundary_halffaces));
        archive(cereal::make_nvp("boundary_param2d", vdata.boundary_param2d));
        archive(cereal::make_nvp("signature", vdata.signature));
        archive(cereal::make_nvp("dualCellCorner_to_referencePolyhedronCorner", vdata.dualCellCorner_to_referencePolyhedronCorner));
        archive(cereal::make_nvp("reftet_constrained_vertex", vdata.reftet_constrained_vertex));
        archive(cereal::make_nvp("reftet_constrained_halfedges_per_boundary_outgoing_halfedge", vdata.reftet_constrained_halfedges_per_boundary_outgoing_halfedge));
        archive(cereal::make_nvp("flipped", vdata.flipped));
        archive(cereal::make_nvp("testPoint", vdata.testPoint));
    }
    template<class Archive>
    void serialize(Archive& archive, dsm::HexLayoutTraits::EdgeData& edata) {
        archive(cereal::make_nvp("code", edata.code));
        archive(cereal::make_nvp("sheetId", edata.sheetId));
        archive(cereal::make_nvp("param2d", edata.param2d));
        archive(cereal::make_nvp("reftet_constrained_vertex", edata.reftet_constrained_vertex));
        archive(cereal::make_nvp("testPoint", edata.testPoint));
    }
    template<class Archive>
    void serialize(Archive& archive, dsm::HexLayoutTraits::FaceData& fdata) {
        archive(cereal::make_nvp("code", fdata.code));
        archive(cereal::make_nvp("sheetCrossing", fdata.sheetCrossing));
        archive(cereal::make_nvp("sheetId", fdata.sheetId));
    }
    template<class Archive>
    void serialize(Archive& archive, dsm::HexLayoutTraits::HalfEdgeData& hedata) {
        archive(cereal::make_nvp("reftet_halffaces", hedata.reftet_halffaces));
        archive(cereal::make_nvp("reftet_boundary_halfedges", hedata.reftet_boundary_halfedges));
    }
    template<class Archive>
    void serialize(Archive& archive, dsm::HexLayoutTraits::HalfFaceData& hfdata) {
        archive(cereal::make_nvp("reftet_halfedges", hfdata.reftet_halfedges));
    }
    template<class Archive, class T>
    void serialize(Archive& archive, dsm::Grid<T>& grid) {
        archive(cereal::make_nvp("mNumCells", grid.mNumCells));
        archive(cereal::make_nvp("mPoints", grid.mPoints));
    }
    template<class Archive>
    void serialize(Archive& archive, dsm::HexLayoutTraits::CellData& cdata) {
        archive(cereal::make_nvp("code", cdata.code));
        archive(cereal::make_nvp("sheetCrossing", cdata.sheetCrossing));
        archive(cereal::make_nvp("sheetId", cdata.sheetId));
        archive(cereal::make_nvp("reftet_vertex", cdata.reftet_vertex));
        archive(cereal::make_nvp("corner", cdata.corner));
        archive(cereal::make_nvp("edge", cdata.edge));
        archive(cereal::make_nvp("face", cdata.face));
        archive(cereal::make_nvp("flipped", cdata.flipped));
        archive(cereal::make_nvp("color", cdata.color));
    }
    template<class Archive>
    void save(Archive& archive, const dsm::HexLayout& hexLayout) {
        save<false>(archive, hexLayout);
        std::vector<dsm::HexLayoutTraits::VertexData> vdata(hexLayout.n_vertices());
        std::vector<dsm::HexLayoutTraits::EdgeData> edata(hexLayout.n_edges());
        std::vector<dsm::HexLayoutTraits::FaceData> fdata(hexLayout.n_faces());
        std::vector<dsm::HexLayoutTraits::HalfEdgeData> hedata(hexLayout.n_halfedges());
        std::vector<dsm::HexLayoutTraits::HalfFaceData> hfdata(hexLayout.n_halffaces());
        std::vector<dsm::HexLayoutTraits::CellData> cdata(hexLayout.n_cells());
        for (auto v : hexLayout.vertices()) vdata[v] = hexLayout.data(v);
        for (auto e : hexLayout.edges()) edata[e] = hexLayout.data(e);
        for (auto f : hexLayout.faces()) fdata[f] = hexLayout.data(f);
        for (auto he : hexLayout.halfedges()) hedata[he] = hexLayout.data(he);
        for (auto hf : hexLayout.halffaces()) hfdata[hf] = hexLayout.data(hf);
        for (auto c : hexLayout.cells()) cdata[c] = hexLayout.data(c);
        archive(cereal::make_nvp("vdata", vdata));
        archive(cereal::make_nvp("edata", edata));
        archive(cereal::make_nvp("fdata", fdata));
        archive(cereal::make_nvp("hedata", hedata));
        archive(cereal::make_nvp("hfdata", hfdata));
        archive(cereal::make_nvp("cdata", cdata));
        archive(cereal::make_nvp("vertexByCode", hexLayout.vertexByCode));
        archive(cereal::make_nvp("edgeByCode", hexLayout.edgeByCode));
        archive(cereal::make_nvp("faceByCode", hexLayout.faceByCode));
        archive(cereal::make_nvp("cellByCode", hexLayout.cellByCode));
    }
    template<class Archive>
    void load(Archive& archive, dsm::HexLayout& hexLayout) {
        load<false>(archive, hexLayout);
        std::vector<dsm::HexLayoutTraits::VertexData> vdata;
        std::vector<dsm::HexLayoutTraits::EdgeData> edata;
        std::vector<dsm::HexLayoutTraits::FaceData> fdata;
        std::vector<dsm::HexLayoutTraits::HalfEdgeData> hedata;
        std::vector<dsm::HexLayoutTraits::HalfFaceData> hfdata;
        std::vector<dsm::HexLayoutTraits::CellData> cdata;
        archive(cereal::make_nvp("vdata", vdata));
        archive(cereal::make_nvp("edata", edata));
        archive(cereal::make_nvp("fdata", fdata));
        archive(cereal::make_nvp("hedata", hedata));
        archive(cereal::make_nvp("hfdata", hfdata));
        archive(cereal::make_nvp("cdata", cdata));
        for (auto v : hexLayout.vertices()) hexLayout.data(v) = vdata[v];
        for (auto e : hexLayout.edges()) hexLayout.data(e) = edata[e];
        for (auto f : hexLayout.faces()) hexLayout.data(f) = fdata[f];
        for (auto he : hexLayout.halfedges()) hexLayout.data(he) = hedata[he];
        for (auto hf : hexLayout.halffaces()) hexLayout.data(hf) = hfdata[hf];
        for (auto c : hexLayout.cells()) hexLayout.data(c) = cdata[c];
        archive(cereal::make_nvp("vertexByCode", hexLayout.vertexByCode));
        archive(cereal::make_nvp("edgeByCode", hexLayout.edgeByCode));
        archive(cereal::make_nvp("faceByCode", hexLayout.faceByCode));
        archive(cereal::make_nvp("cellByCode", hexLayout.cellByCode));
    }

    //---------------------------------------------------------------------------------------------
    // dsm::RefinedTetMesh
    template<class Archive>
    void serialize(Archive& archive, dsm::RefinedTetMeshTraits::VertexData& vdata) {
        archive(cereal::make_nvp("constrained", vdata.constrained));
        archive(cereal::make_nvp("layout_cell", vdata.layout_cell));
        archive(cereal::make_nvp("layout_boundary_face", vdata.layout_boundary_face));
    }
    template<class Archive>
    void serialize(Archive& archive, dsm::RefinedTetMeshTraits::EdgeData& edata) {
        archive(cereal::make_nvp("constrained", edata.constrained));
        archive(cereal::make_nvp("layout_boundary_edge", edata.layout_boundary_edge));
    }
    template<class Archive>
    void serialize(Archive& archive, dsm::RefinedTetMeshTraits::CellData& cdata) {
        archive(cereal::make_nvp("layout_vertex", cdata.layout_vertex));
    }
    template<class Archive>
    void save(Archive& archive, const dsm::RefinedTetMesh& refinedTetMesh) {
        save<true>(archive, refinedTetMesh);
        std::vector<dsm::RefinedTetMeshTraits::VertexData> vdata(refinedTetMesh.n_vertices());
        std::vector<dsm::RefinedTetMeshTraits::EdgeData> edata(refinedTetMesh.n_edges());
        std::vector<dsm::RefinedTetMeshTraits::CellData> cdata(refinedTetMesh.n_cells());
        for (auto v : refinedTetMesh.vertices()) vdata[v] = refinedTetMesh.data(v);
        for (auto e : refinedTetMesh.edges()) edata[e] = refinedTetMesh.data(e);
        for (auto c : refinedTetMesh.cells()) cdata[c] = refinedTetMesh.data(c);
        archive(cereal::make_nvp("vdata", vdata));
        archive(cereal::make_nvp("edata", edata));
        archive(cereal::make_nvp("cdata", cdata));
    }
    template<class Archive>
    void load(Archive& archive, dsm::RefinedTetMesh& refinedTetMesh) {
        load<true>(archive, refinedTetMesh);
        std::vector<dsm::RefinedTetMeshTraits::VertexData> vdata;
        std::vector<dsm::RefinedTetMeshTraits::EdgeData> edata;
        std::vector<dsm::RefinedTetMeshTraits::CellData> cdata;
        archive(cereal::make_nvp("vdata", vdata));
        archive(cereal::make_nvp("edata", edata));
        archive(cereal::make_nvp("cdata", cdata));
        for (auto v : refinedTetMesh.vertices()) refinedTetMesh.data(v) = vdata[v];
        for (auto e : refinedTetMesh.edges()) refinedTetMesh.data(e) = edata[e];
        for (auto c : refinedTetMesh.cells()) refinedTetMesh.data(c) = cdata[c];
        // dsm::misc::buildBVH(refinedTetMesh, 10);
    }

    //---------------------------------------------------------------------------------------------
    // dsm::HexMesh
    template<class Archive>
    void serialize(Archive& archive, dsm::HexMeshTraits::VertexData& vdata) {
        archive(cereal::make_nvp("layout_vertex", vdata.layout_vertex));
    }
    template<class Archive>
    void serialize(Archive& archive, dsm::HexMeshTraits::HalfEdgeData& hedata) {
        archive(cereal::make_nvp("layout_halfedge", hedata.layout_halfedge));
    }
    template<class Archive>
    void serialize(Archive& archive, dsm::HexMeshTraits::CellData& cdata) {
        archive(cereal::make_nvp("layout_cell", cdata.layout_cell));
    }
    template<class Archive>
    void save(Archive& archive, const dsm::HexMesh& hexMesh) {
        save<false>(archive, hexMesh);
        std::vector<dsm::HexMeshTraits::VertexData> vdata(hexMesh.n_vertices());
        std::vector<dsm::HexMeshTraits::HalfEdgeData> hedata(hexMesh.n_halfedges());
        std::vector<dsm::HexMeshTraits::CellData> cdata(hexMesh.n_cells());
        for (auto v : hexMesh.vertices()) vdata[v] = hexMesh.data(v);
        for (auto he : hexMesh.halfedges()) hedata[he] = hexMesh.data(he);
        for (auto c : hexMesh.cells()) cdata[c] = hexMesh.data(c);
        archive(cereal::make_nvp("vdata", vdata));
        archive(cereal::make_nvp("hedata", hedata));
        archive(cereal::make_nvp("cdata", cdata));
    }
    template<class Archive>
    void load(Archive& archive, dsm::HexMesh& hexMesh) {
        load<false>(archive, hexMesh);
        std::vector<dsm::HexMeshTraits::VertexData> vdata;
        std::vector<dsm::HexMeshTraits::HalfEdgeData> hedata;
        std::vector<dsm::HexMeshTraits::CellData> cdata;
        archive(cereal::make_nvp("vdata", vdata));
        archive(cereal::make_nvp("hedata", hedata));
        archive(cereal::make_nvp("cdata", cdata));
        for (auto v : hexMesh.vertices()) hexMesh.data(v) = vdata[v];
        for (auto he : hexMesh.halfedges()) hexMesh.data(he) = hedata[he];
        for (auto c : hexMesh.cells()) hexMesh.data(c) = cdata[c];
    }

}

namespace dsm {

template <class T>
inline void serializeToBlob(const T& obj, std::string& blob) {
    std::ostringstream oss;
    cereal::PortableBinaryOutputArchive oarchive(oss);
    oarchive(obj);
    blob = oss.str();
}

template <class T>
inline void deserializeFromBlob(T& obj, const std::string& blob) {
    std::istringstream iss(blob);
    cereal::PortableBinaryInputArchive iarchive(iss);
    iarchive(obj);
}

}
