#include "typedefs.h"
#include "misc.h"
#include "instant-meshes/adjacency.h"
#include "instant-meshes/normal.h"
#include "instant-meshes/dedge.h"
#include <igl/read_triangle_mesh.h>
using namespace std;
using namespace Eigen;
using namespace kt84;

dsm::TriMesh::TriMesh()
    : mOptimizer(mRes, false)
{
    mOptimizer.setRoSy(4);
    mOptimizer.setPoSy(4);
    mOptimizer.setExtrinsic(true);
}

dsm::TriMesh::~TriMesh() {
    mOptimizer.shutdown();
    mRes.free();
}

bool dsm::TriMesh::load(const string& filename, float creaseAngle) {
    MatrixXd Vd;
    MatrixXi Fi;
    if (!igl::read_triangle_mesh(filename, Vd, Fi))
        return false;
    V = Vd.cast<float>().transpose();
    F = Fi.cast<uint32_t>().transpose();
    cout << "triMesh statistics:\n"
         << "vertices: " << V.cols() << '\n'
         << "faces: " << F.cols() << '\n';
    // recenter and normalize
    AlignedBox3f bbox;
    for (int i = 0; i < V.cols(); ++i)
        bbox.extend(V.col(i));
    for (int i = 0; i < V.cols(); ++i)
        V.col(i) -= bbox.center();
    V /= bbox.diagonal().maxCoeff();

    preprocess(creaseAngle);
    return true;
}

void dsm::TriMesh::preprocess(float creaseAngle_) {
    ScopedTimerPrinter timer("TriMesh::preprocess");
    creaseAngle = creaseAngle_;

    stats = compute_mesh_stats(F, V);

    /* Compute a directed edge data structure */
    build_dedge(F, V, V2E, E2E, boundary, nonManifold);

    cout << "TriMesh stats:" << endl <<
      "  vertices: " << V.cols() << endl <<
      "  faces: " << F.cols() << endl <<
      "  edges: " << (E2E.size() / 2) << endl <<
      "  AABB min:\n" << stats.mAABB.min << endl <<
      "  AABB max:\n" << stats.mAABB.max << endl <<
      "  avg edge length: " << stats.mAverageEdgeLength << endl <<
      "  max edge length: " << stats.mMaximumEdgeLength << endl <<
      "  surface area: " << stats.mSurfaceArea << endl;

    /* Compute adjacency matrix */
    AdjacencyMatrix adj = generate_adjacency_matrix_uniform(F, V2E, E2E, nonManifold);

    /* Compute vertex/crease normals */
    generate_crease_normals(F, V, V2E, E2E, boundary, nonManifold, creaseAngle, N, Nf, creases);

    /* Compute crease edges */
    const double dpThreshold = std::cos(creaseAngle * M_PI / 180);
    crease_edges.clear();
    for (uint32_t f0 = 0; f0 < F.cols(); ++f0) {
        for (uint32_t i = 0; i < 3; ++i) {
            const uint32_t e0 = 3 * f0  + i,
                           e1 = E2E[e0],
                           f1 = e1 / 3;
            if (e0 < e1) {
                if (Nf.col(f0).dot(Nf.col(f1)) < dpThreshold)
                    crease_edges.insert(e0);
            }
        }
    }

    /* Compute dual vertex areas */
    compute_dual_vertex_areas(F, V, V2E, E2E, nonManifold, A);

    /* Build multi-resolution hierarrchy */
    mRes.free();
    auto F_tmp = F;
    auto V_tmp = V;
    auto A_tmp = A;
    auto N_tmp = N;
    auto E2E_tmp = E2E;
    mRes.setF(std::move(F_tmp));
    mRes.setV(std::move(V_tmp));
    mRes.setA(std::move(A_tmp));
    mRes.setN(std::move(N_tmp));
    mRes.setE2E(std::move(E2E_tmp));
    mRes.setAdj(std::move(adj));
    mRes.build();
    mRes.resetSolution();
    computeRoSy({});

    mBVH = std::make_shared<::BVH>(&F, &V, &N, stats.mAABB);
    mBVH->build();

    // compute undirected edges
    halfedge_to_edge.resize(E2E.size());
    for (int f = 0; f < F.cols(); ++f) {
        for (int i = 0; i < 3; ++i) {
            const uint32_t v0 = F(i,f);
            const uint32_t v1 = F((i+1)%3,f);
            if (v0 < v1) {
                const uint32_t halfedge_id = 3*f + i;
                edges.push_back({v0,v1});
                edge_to_halfedge.push_back(halfedge_id);
                halfedge_to_edge[halfedge_id] = edges.size() - 1;
                halfedge_to_edge[E2E[halfedge_id]] = edges.size() - 1;
            }
        }
    }
}

void dsm::TriMesh::tetrahedralize(TetMesh& tetMesh, const tetgen_util::Switches& tetgen_switches) {
    // prepare TetGen input
    tetgen_util::Format_off in_off;
    in_off.vertices.resize(V.cols() * 3);
    for (int v = 0; v < V.cols(); ++v)
        for (int i = 0; i < 3; ++i)
            in_off.vertices[i + 3*v] = V(i, v);
    in_off.faces.resize(F.cols(), {0, 0, 0});
    for (int f = 0; f < F.cols(); ++f)
        for (int i = 0; i < 3; ++i)
            in_off.faces[f][i] = F(i, f);

    auto timer = make_ScopedTimerPrinter("calling TetGen");
    cout << "TetGen switches: " << tetgen_switches.to_string() << endl;
    auto out = tetgen_util::tetrahedralize(in_off, tetgen_switches);
    timer.reset();

    MatrixXd tetgen_V;
    tetgen_V.resize(3, out.node.pointlist.size()/3);
    int v = 0;
    for (auto p=out.node.pointlist.begin(); p!=out.node.pointlist.end(); p+=3, ++v)
        tetgen_V.col(v) << *p, *(p+1), *(p+2);
    MatrixXi tetgen_C;
    tetgen_C.resize(4, out.ele.tetrahedronlist.size()/4);
    int c = 0;
    for (auto p=out.ele.tetrahedronlist.begin(); p!=out.ele.tetrahedronlist.end(); p+=4, ++c)
        tetgen_C.col(c) << *p, *(p+1), *(p+2), *(p+3);

    misc::initTetMesh(tetMesh, tetgen_V, tetgen_C);
    misc::buildBVH(tetMesh, 10);
}

void dsm::TriMesh::computeRoSy(const vector<RoSyConstraint>& constraints) {
    ScopedTimerPrinter timer("TriMesh::computeRoSy");
    mRes.clearConstraints();
    for (const auto& p : constraints) {
        size_t v = p.first;
        Vector3f tlocal = p.second;
        tlocal -= tlocal.dot(mRes.N().col(v)) * mRes.N().col(v);
        tlocal.normalize();
        mRes.CQ().col(v) = tlocal;
        mRes.CQw()[v] = 1.0f;
    }
    mRes.propagateConstraints(mOptimizer.rosy(), mOptimizer.posy());
    mOptimizer.optimizeOrientations(-1);
    mOptimizer.notify();
    mOptimizer.wait();
    Q = mRes.Q();
}

dsm::TriMeshPoint dsm::TriMesh::rayIntersect(const ::Ray& ray) const {
    if (!mBVH) return {};
    uint32_t idx;
    float t;
    Vector2f uv;
    if (mBVH->rayIntersect(ray, idx, t, &uv)) {
        TriMeshPoint p;
        p.f = idx;
        p.bc << 1 - uv.sum(), uv[0], uv[1];
        return p;
    }
    return {};
}

Vector3d dsm::TriMesh::getNormal(const TriMeshPoint& p) const {
    Vector3d n = Vector3d::Zero();
    bool near_crease = false;
    for (int i = 0; i < 3; ++i) {
        if (creases.count(F(i, p.f))) {
            near_crease = true;
            break;
        }
    }
    if (near_crease) {
        n = Nf.col(p.f).cast<double>();
    } else {
        for (int i = 0; i < 3; ++i)
            n += p.bc[i] * N.col(F(i, p.f)).cast<double>();
        n.normalize();
    }
    return n;
}

void dsm::TriMesh::init_featuregraph_dihedral_angle(float angle_degree) {
    vertex_constraint = vector<int>(V.cols(),0);
    edge_constraint = vector<int>(edges.size(),0);
    for (uint32_t e = 0; e < edges.size(); ++e) {
        const uint32_t he = edge_to_halfedge[e];
        const Vector3f n0 = Nf.col(incident_face(he));
        const Vector3f n1 = Nf.col(incident_face(opposite_halfedge(he)));
        edge_constraint[e] = n0.dot(n1) < cos(angle_degree*util::pi()/180);
    }
    make_vertex_constrained_if_necessary();
    // also constrain vertex incident to two constrained edges if those edges form an acute angle
    for (int v = 0; v < V.cols(); ++v) {
        if (vertex_constraint[v])
            continue;
        const vector<uint32_t> outgoing_halfedges = this->outgoing_halfedges(v);
        vector<Vector3f> d;
        for (uint32_t he : outgoing_halfedges) {
            if (edge_constraint[halfedge_to_edge[he]]) 
                d.push_back((V.col(to_vertex(he)) - V.col(v)).normalized());
        }
        if (d.size() == 2 && d[0].dot(-d[1]) < cos(2*angle_degree*util::pi()/180))
            vertex_constraint[v] = 1;
    }
}

void dsm::TriMesh::make_vertex_constrained_if_necessary() {
    for (int v = 0; v < V.cols(); ++v) {
        const vector<uint32_t> outgoing_halfedges = this->outgoing_halfedges(v);
        int cnt = 0;
        for (uint32_t he : outgoing_halfedges) {
            if (edge_constraint[halfedge_to_edge[he]])
                ++cnt;
        }
        if (cnt != 0 && cnt != 2)
            vertex_constraint[v] = 1;
    }
}
